//
//  Constants.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 10/04/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import Firebase

let notAvailable = "N/A"

struct Constants {
    struct refs {
        static let databaseRoot = Database.database().reference()
        static let databaseChats = databaseRoot.child("chat")
    }
}
