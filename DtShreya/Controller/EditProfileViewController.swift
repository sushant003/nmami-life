//
//  EditProfileViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 25/09/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class EditProfileViewController: BaseViewController {
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var ageTF: UITextField!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var headerView: CustomView!
    var gender = ""
    var datePickerController : DatePickerViewController! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTF.attributedPlaceholder = NSAttributedString(string: "Name",
                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        ageTF.attributedPlaceholder = NSAttributedString(string: "Date Of Birth",
                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        datePickerController = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as? DatePickerViewController
//        maleButton.setImage(#imageLiteral(resourceName: "form_male").withRenderingMode(.alwaysTemplate), for: .normal)
//        maleButton.tintColor = AppConstants.Colors.greenColor
        setProfileData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
    }
    
    func setProfileData() {
        nameTF.text = ((data as! [String:Any])["data"] as! ProfileModel).name
        ageTF.text = ((data as! [String:Any])["data"] as! ProfileModel).age
        
        gender = ((data as! [String:Any])["data"] as! ProfileModel).gender
        if gender == "Male" {
            femaleButton.setTitleColor(UIColor.lightGray, for: .normal)
            maleButton.setTitleColor(AppConstants.Colors.greenColor, for: .normal)
            maleButton.setImage(#imageLiteral(resourceName: "form_male").withRenderingMode(.alwaysTemplate), for: .normal)
            maleButton.imageView?.tintColor = AppConstants.Colors.greenColor
            femaleButton.titleLabel?.textColor = UIColor.lightGray
            femaleButton.imageView?.tintColor = UIColor.darkGray
        }
        else {
            maleButton.setTitleColor(UIColor.lightGray, for: .normal)
            femaleButton.setTitleColor(AppConstants.Colors.greenColor, for: .normal)
            femaleButton.setImage(#imageLiteral(resourceName: "form_female").withRenderingMode(.alwaysTemplate), for: .normal)
            femaleButton.imageView?.tintColor = AppConstants.Colors.greenColor
            maleButton.titleLabel?.textColor = UIColor.lightGray
            maleButton.imageView?.tintColor = UIColor.darkGray
        }
    }
    
    //MARK: IBAction
    
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickMale(_ sender: Any) {
        self.view.endEditing(true)
        gender = "Male"
        
        femaleButton.setTitleColor(UIColor.lightGray, for: .normal)
        
        maleButton.setTitleColor(AppConstants.Colors.greenColor, for: .normal)
        maleButton.setImage(#imageLiteral(resourceName: "form_male").withRenderingMode(.alwaysTemplate), for: .normal)
        maleButton.imageView?.tintColor = AppConstants.Colors.greenColor
        femaleButton.titleLabel?.textColor = UIColor.lightGray
        femaleButton.imageView?.tintColor = UIColor.darkGray
    }
    
    @IBAction func onClickFemale(_ sender: Any) {
        self.view.endEditing(true)
        gender = "Female"
        maleButton.setTitleColor(UIColor.lightGray, for: .normal)
        femaleButton.setTitleColor(AppConstants.Colors.greenColor, for: .normal)
        femaleButton.setImage(#imageLiteral(resourceName: "form_female").withRenderingMode(.alwaysTemplate), for: .normal)
        femaleButton.imageView?.tintColor = AppConstants.Colors.greenColor
        maleButton.titleLabel?.textColor = UIColor.lightGray
        maleButton.imageView?.tintColor = UIColor.darkGray
    }
    
    @IBAction func onClickDOB(_ sender: Any) {
        self.view.endEditing(true)
        self.datePickerController.data = ["pickerType":"date"]
        self.datePickerController.datePickerDelegate = self
        self.addChildViewController(self.datePickerController)
        self.view.addSubview(self.datePickerController.view)
        self.datePickerController.didMove(toParentViewController: self)
    }
    
    
    @IBAction func onClickSave(_ sender: Any) {
        self.view.endEditing(true)
        saveBasicInfo()
    }
    
    //MARK: Webservice methods
    func saveBasicInfo() {
        if nameTF.text == "" || ageTF.text == ""  {
             AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.All_Data_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            return
        }
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.saveProfileCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.getProfleDataServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.name:nameTF.text!,AppConstants.ServiceKeys.dob:ageTF.text!,AppConstants.ServiceKeys.gender:gender], url: AppConstants.Dashboard.saveProfileDataServiceSuffix, notificationKey: AppConstants.Dashboard.getProfleDataServiceNotify, controllerName: self,requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func saveProfileCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                navigationController?.popViewController(animated: true)
                ((data as! [String:Any])["controller"] as! throwData).sendPopedData(data: "")
//                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: message!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: message!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}

//MARK: Datepicker Delegate
extension EditProfileViewController : DatePickerListener {
    func setDateFromPicker(date: Any) {
        datePickerController.view.removeFromSuperview()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        ageTF.text = dateFormatter.string(from: date as! Date)
    }
    
    func dismissDatePicker() {
        datePickerController.view.removeFromSuperview()
    }
}
