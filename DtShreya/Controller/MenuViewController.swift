//
//  MenuViewController.swift
//  GDI
//
//  Created by Sushant Jugran on 03/07/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit

class MenuViewController: BaseViewController {

    @IBOutlet weak var userImage: CustomImage!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var headerlabel: UILabel!
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var emailLabel: UILabel!
    
    var menuArray : [[String:UIImage]] = [[AppConstants.Menu.dietDiary:#imageLiteral(resourceName: "menu_diet_diary")],[AppConstants.Menu.dieseaseDiary:#imageLiteral(resourceName: "menu_disease_diary")],[AppConstants.Menu.dietStats:#imageLiteral(resourceName: "menu_diet_stats")],[AppConstants.Menu.healthTips:#imageLiteral(resourceName: "menu_health_read")],[AppConstants.Menu.feedback:#imageLiteral(resourceName: "menu_feedback")],[AppConstants.Menu.refer_earn:#imageLiteral(resourceName: "menu_refer&earn")],[AppConstants.Menu.setting:#imageLiteral(resourceName: "menu_setting")],[AppConstants.Menu.recepies:#imageLiteral(resourceName: "menu_health_read")],[AppConstants.Menu.logout:#imageLiteral(resourceName: "menu_logout")]]
    //[AppConstants.Menu.payment:#imageLiteral(resourceName: "menu_payment")],
    var nonActiveMenuArray : [Int:[[String:UIImage]]] = [0:[[AppConstants.Menu.facebook:#imageLiteral(resourceName: "facebook")],[AppConstants.Menu.twitter:#imageLiteral(resourceName: "facebook")],[AppConstants.Menu.instagram:#imageLiteral(resourceName: "facebook")],[AppConstants.Menu.youtube:#imageLiteral(resourceName: "facebook")],[AppConstants.Menu.logout:#imageLiteral(resourceName: "menu_logout")]]]
    var selectedIndex : [Int:Int] = [0:0]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        userName.text = AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.userDefaultConstants.userName) as? String
        emailLabel.text = AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.userDefaultConstants.email) as? String
        
        AppHelper.getInstance().loadImageIntoImageView(imageUrl: (AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.image) as? String)!, imageView: userImage, placeHolder: "user_placeholder")
    }
    
    func isValidAccount() -> Bool {
        if (AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.dieterStatus) as! String) == "1" {
            return true
        }
        return true
    }
    
    //MARK: IBAction
    
    @IBAction func onClickImage(_ sender: Any) {
        AppHelper.getInstance().pushView(identifier: "CompleteProfileViewController", controllerName: self)
//        AppHelper.getInstance().pushControllerWithData(id: "ImageViewController", controllerName: self, data: (AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.image) as? String)!)
    }
}

extension MenuViewController : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
       
        return 1
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if isValidAccount() {
//            if section == 0 {
//                return 0
//            }
//            else {
//                return 40
//            }
//        }
//        return 40
//    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        if isValidAccount() {
//            if section == 0 {
//                return nil
//            }
//            else {
//                return headerView
//            }
//        }
//        headerlabel.text = "Connect to Us"
//        return headerView
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isValidAccount() {
            return menuArray.count
        }
        return (nonActiveMenuArray[section]?.count)!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.selectionStyle = .none
        (cell.viewWithTag(2) as! UILabel).text = isValidAccount() ? menuArray[indexPath.row].first?.key : nonActiveMenuArray[indexPath.section]?[indexPath.row].first?.key
        if selectedIndex.first?.key == indexPath.section  &&  selectedIndex.first?.value == indexPath.row{
            (cell.viewWithTag(2) as! UILabel).textColor = AppConstants.Colors.blueColor
        }
        else {
            (cell.viewWithTag(2) as! UILabel).textColor = UIColor.black
        }
        (cell.viewWithTag(1) as! UIImageView).image = (cell.viewWithTag(1) as! UIImageView).image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        (cell.viewWithTag(1) as! UIImageView).tintColor = UIColor(red: 230.0/255.0, green: 27.0/255.0, blue: 88.0/255.0, alpha: 1.0)
        (cell.viewWithTag(1) as! UIImageView).image = isValidAccount() ? menuArray[indexPath.row].first?.value : nonActiveMenuArray[indexPath.section]?[indexPath.row].first?.value
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = [indexPath.section:indexPath.row]
        tableView.reloadData()
        let str = isValidAccount() ? menuArray[indexPath.row].first?.key : nonActiveMenuArray[indexPath.section]![indexPath.row].first?.key
        switch str {
        case AppConstants.Menu.dieseaseDiary :
            AppHelper.getInstance().pushControllerWithData(id: "DiseaseDiaryViewController", controllerName: self, data: false)
        case AppConstants.Menu.dietStats :
            AppHelper.getInstance().pushControllerWithData(id: "DietStatsViewController", controllerName: self, data: false)
        case AppConstants.Menu.dietDiary :
            AppHelper.getInstance().pushControllerWithData(id: "DiaryViewController", controllerName: self, data: false)
        case AppConstants.Menu.healthTips :
            AppHelper.getInstance().pushControllerWithData(id: "BlogsViewController", controllerName: self, data: false)
        case AppConstants.Menu.recepies :
            AppHelper.getInstance().pushView(identifier: "RecepiesViewController", controllerName: self)
            //AppHelper.getInstance().pushView(identifier: "TabBar1ViewController", controllerName: self)
        case AppConstants.Menu.payment :
            break
        case AppConstants.Menu.feedback :
            AppHelper.getInstance().pushView(identifier: "FeedbackViewController", controllerName: self)
        case AppConstants.Menu.refer_earn:
            AppHelper.getInstance().pushView(identifier: "GetReferralViewController", controllerName: self)
        case AppConstants.Menu.setting:
            AppHelper.getInstance().pushView(identifier: "ReminderViewController", controllerName: self)
        case AppConstants.Menu.logout:
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Are you sure you want to logout?", cancelButtonName: "NO", otherButtonName: "YES", controllerName: self, isAlertOnly: false)
            
        case AppConstants.Menu.facebook:
            UIApplication.shared.open(URL(string : "https://www.facebook.com/DietitianShreya/")!, options: [:], completionHandler: { (status) in
            })
        case AppConstants.Menu.twitter:
            UIApplication.shared.open(URL(string : "https://mobile.twitter.com/DietitianShreya")!, options: [:], completionHandler: { (status) in
            })
        case AppConstants.Menu.instagram:
            UIApplication.shared.open(URL(string : "https://www.instagram.com/dietitianshreya")!, options: [:], completionHandler: { (status) in
            })
        case AppConstants.Menu.youtube:
            UIApplication.shared.open(URL(string : "https://www.youtube.com/channel/UCRP40I-Avt-flLYNt5hYlrA")!, options: [:], completionHandler: { (status) in
            })
        default:
            break
        }
    }
}

extension MenuViewController : alertPositiveButtonDelegate {
    func alertPositiveButtonPressed() {
        AppHelper.getInstance().pushView(identifier: "LoginViewController", controllerName: self)
        AppHelper.getInstance().logOut()
    }
    
    func alertNegativeButtonPressed() {        
    }
}
