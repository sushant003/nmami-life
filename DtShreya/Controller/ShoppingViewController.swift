//
//  ShoppingViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 23/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class ShoppingViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension ShoppingViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (data as! [String]).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = (data as! [String])[indexPath.row]
        return cell
    }
}
