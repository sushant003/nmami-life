//
//  DiaryViewControllerViewController.swift
//  Meditician
//
//  Created by Sushant Jugran on 23/04/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit
import SideMenuController

class DiaryViewController: BaseViewController {

    @IBOutlet weak var diaryTV: UITableView!
    @IBOutlet weak var serverStatusView: UIView!
    @IBOutlet weak var serverStatusImage: UIImageView!
    @IBOutlet weak var serverStatusLabel: UILabel!
    @IBOutlet weak var headerView: CustomView!
    
    var tableData : [DiaryModel] = [DiaryModel]()
    var hasDiary : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        serverStatusImage.isHidden = true
        getClientDiary()
    }
    
    override func viewDidLayoutSubviews() {
        headerView.setGradientBackground()
        for cell in diaryTV.visibleCells {
            (cell.viewWithTag(1) as! CustomView).createShadow()
        }
    }
    
    func setServerStatus(text : String) {
        serverStatusView.isHidden = false
        serverStatusImage.isHidden = false
        serverStatusLabel.text = text
    }
    
    //MARK: IBAction
    
    @IBAction func onClickbackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickCreateDiary(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        AppHelper.getInstance().pushControllerWithData(id: "CreateDiaryViewController", controllerName: self, data: ["date":formatter.string(from: Date()),"controller":self])
    }
    
    //MARK: Webservice Methods
    func getClientDiary() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.serverStatusView)
            tableData.removeAll()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            NotificationCenter.default.addObserver(self,selector: #selector(self.diaryCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_diary_service_notify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.date:formatter.string(from: Date())], url: AppConstants.Clients.get_diary_service_suffix, notificationKey: AppConstants.Clients.get_diary_service_notify, controllerName: self, requestType: "")
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Net_Error)
        }
    }
    
    @objc func diaryCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.serverStatusView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                serverStatusView.isHidden = true
                let resultDict: [[String:Any]] = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as? [[String:Any]])!
                prepareDiaryData(serviceData: resultDict)
                if tableData.count == 0 {
                    setServerStatus(text: "There is no entry in your diary")
                    return
                }
                hasDiary = true
                diaryTV.dataSource = self
                diaryTV.reloadData()
            }
            else {
                hasDiary = false
                setServerStatus(text: message!)
            }
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Server_Error)
        }
    }
    
    func prepareDiaryData(serviceData: [[String:Any]]) {
        for diary in serviceData {
            if diary[AppConstants.ServiceKeys.data] as! Bool{
                if let entry = diary[AppConstants.ServiceKeys.entry] as? [String:Any] {
                    var diaryModel = DiaryModel()
                    diaryModel.date = diary[AppConstants.ServiceKeys.date] as! String
                    diaryModel.steps = "\(entry[AppConstants.ServiceKeys.steps]!)"
                    diaryModel.weight = "\(entry[AppConstants.ServiceKeys.weight]!)"
                    diaryModel.caloriesBurnt = "\(entry[AppConstants.ServiceKeys.calories]!)"
                    diaryModel.sleephours = "\(entry[AppConstants.ServiceKeys.sleepHours]!)"
                    diaryModel.cheat = entry[AppConstants.ServiceKeys.cheatMeals] == nil ? "N.A" : "\(entry[AppConstants.ServiceKeys.cheatMeals]!)"
                    tableData.append(diaryModel)
                }
            }
        }
    }
}

extension DiaryViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        (cell.viewWithTag(2) as! UILabel).text = tableData[indexPath.row].weight
        (cell.viewWithTag(3) as! UILabel).text = tableData[indexPath.row].sleephours
        (cell.viewWithTag(4) as! UILabel).text = tableData[indexPath.row].cheat
        (cell.viewWithTag(5) as! UILabel).text = tableData[indexPath.row].steps
        (cell.viewWithTag(6) as! UILabel).text = tableData[indexPath.row].caloriesBurnt
        (cell.viewWithTag(7) as! UILabel).text = tableData[indexPath.row].date
        return cell
    }
}

extension DiaryViewController : throwData {
    func sendPopedData(data: Any) {
        self.getClientDiary()
    }
}

