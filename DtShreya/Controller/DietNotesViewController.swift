//
//  DietNotesViewController.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 02/04/18.
//  Copyright © 2018 Medicians. All rights reserved.
//

import UIKit

class DietNotesViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var serverStatusView: UIView!
    @IBOutlet weak var serverStatusLabel: UILabel!
    @IBOutlet weak var serverStatusImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        if (data as! [String]).count == 0 {
            setServerStatus(text: "No notes found")
        }
        else {
            serverStatusView.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    
    func setServerStatus(text : String) {
        serverStatusView.isHidden = false
        serverStatusImage.image = #imageLiteral(resourceName: "serverError")
        serverStatusLabel.text = text
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (data as! [String]).count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dietNoteCell")
        cell?.textLabel?.text = (data as! [String])[indexPath.row]
        cell?.textLabel?.font = UIFont.systemFont(ofSize: 14)
        cell?.textLabel?.numberOfLines = 0
        return cell!
    }
    
}
