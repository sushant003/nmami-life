//
//  BMIViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 19/09/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class BMIViewController: BaseViewController {
    
    @IBOutlet weak var bmiCategoryLabel: UILabel!
    @IBOutlet weak var currentBMILabel: UILabel!
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var currentWeight: UILabel!
    @IBOutlet weak var currentBMI: UILabel!
    @IBOutlet weak var idealWeight: UILabel!
    @IBOutlet weak var idealBMI: UILabel!
    @IBOutlet weak var targetWeightLabel: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var manageWeightLabel: UILabel!
    @IBOutlet weak var timeRequiredLabel: UILabel!
    
    var weightDifference : Double = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        statusImage.isHidden = true
        getUserBMI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
    }
    
    //MARK: IBaction
    @IBAction func onClickDecreaseCount(_ sender: Any) {
        let weight = (Double(targetWeightLabel.text!)! - 1) - (Double(currentWeight.text!)!)
        targetWeightLabel.text = "\(Int(targetWeightLabel.text!)! - 1)"
        timeRequiredLabel.text =  "\(abs(Int(weight*7.5)))"
    }
    
    @IBAction func onClickIncreaseCount(_ sender: Any) {
        let weight = (Double(targetWeightLabel.text!)! + 1) - (Double(currentWeight.text!)!)
        targetWeightLabel.text = "\(Int(targetWeightLabel.text!)! + 1)"
        timeRequiredLabel.text =  "\(abs(Int(weight*7.5)))"
        
    }
    
    @IBAction func onClickBrowsePlan(_ sender: Any) {
        setUserTargetWeight()
    }
    
    //MARK: Webservice Methods
    func getUserBMI() {
        if AppHelper.getInstance().isNetworkAvailable() {
            NotificationCenter.default.addObserver(self,selector: #selector(self.catchNotification),name: NSNotification.Name(rawValue: AppConstants.SignupForm.BMIServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: AppConstants.SignupForm.BMIServiceSuffix, notificationKey: AppConstants.SignupForm.BMIServiceNotify, controllerName: self,requestType: "")
            AppHelper.getInstance().showProgressIndicator(view: self.view)
        }
        else {
            statusImage.isHidden = false
            statusLabel.text = AppConstants.ErrorMessags.Net_Error
        }
    }
    
    @objc func catchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                statusView.isHidden = true
                let result = serviceData.userInfo?[AppConstants.ServiceKeys.result] as! [String:String]
                currentBMILabel.text = result[AppConstants.ServiceKeys.currentBMI]
                currentBMI.text = result[AppConstants.ServiceKeys.currentBMI]
                currentWeight.text = result[AppConstants.ServiceKeys.currentWeight]
                idealBMI.text = result[AppConstants.ServiceKeys.idealBMI]
                idealWeight.text = result[AppConstants.ServiceKeys.idealWeight]
                if Double(result[AppConstants.ServiceKeys.currentBMI]!) != nil {
                    let currentBMI = Double(result[AppConstants.ServiceKeys.currentBMI]!)!
                    if currentBMI <= 18.5 {
                        bmiCategoryLabel.text = "Underweight"
                    }
                    else if currentBMI > 18.5 && currentBMI <= 24.9 {
                        bmiCategoryLabel.text = "Normal"
                    }
                    else if currentBMI > 25 && currentBMI <= 29.9 {
                        bmiCategoryLabel.text = "Overweight"
                    }
                    else {
                        bmiCategoryLabel.text = "Obese"
                    }
                }
                guard let weight = Double(idealWeight.text!) else {
                    targetWeightLabel.text = "0"
                    return
                }
                guard let currentWeight = Double(currentWeight.text!) else {
                    targetWeightLabel.text = "0"
                    return
                }                
                weightDifference = currentWeight - weight
                if weightDifference < 0 {
                    if AppHelper.getInstance().diseaseArray.count == 0 {
                        manageWeightLabel.isHidden = true
                    }
                    else {
                        var absoluteDifference = String(format: "%.2f", weightDifference)
                        absoluteDifference.remove(at: absoluteDifference.startIndex)
                        
                        manageWeightLabel.text = "You need to gain \(absoluteDifference) kgs while managing your \(AppHelper.getInstance().diseaseArray.joined(separator: ","))*"
                    }
                }
                else if weightDifference > 0 {
                    if AppHelper.getInstance().diseaseArray.count == 0 {
                        manageWeightLabel.isHidden = true
                    }
                    else {
                        let absoluteDifference = String(format: "%.2f", weightDifference)
                        manageWeightLabel.text = "You need to lose \(absoluteDifference) kgs while managing your \(AppHelper.getInstance().diseaseArray.joined(separator: ","))*"
                    }
                }
                else if weightDifference == 0.0 {
                    if AppHelper.getInstance().diseaseArray.count == 0 {
                        
                        manageWeightLabel.text = "You BMI seems to be inline. Do you want to maintain your weight?"
                    }
                    else {
                        manageWeightLabel.text = "You BMI seems to be inline. Diet will help you manage your  \(AppHelper.getInstance().diseaseArray.joined(separator: ","))"
                    }
                    
                }
                timeRequiredLabel.text = "\(abs(Int(weightDifference*7.5))) days"
                
                
                targetWeightLabel.text =   "\(Int(weight))"
            }
            else {
                statusImage.isHidden = false
                statusLabel.text = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            }
        }
        else {
            statusImage.isHidden = false
            statusLabel.text = AppConstants.ErrorMessags.Server_Error
        }
    }
    
    func setUserTargetWeight() {
        if AppHelper.getInstance().isNetworkAvailable() {
            NotificationCenter.default.addObserver(self,selector: #selector(self.weightCatchNotification),name: NSNotification.Name(rawValue: AppConstants.SignupForm.BMIServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.weight: targetWeightLabel.text!], url: AppConstants.SignupForm.setTargetWeightServiceSuffix, notificationKey: AppConstants.SignupForm.BMIServiceNotify, controllerName: self,requestType: "")
            AppHelper.getInstance().showProgressIndicator(view: self.view)
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func weightCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                AppHelper.getInstance().pushView(identifier: "CustomMenuViewController", controllerName: self)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}

extension Double {
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
