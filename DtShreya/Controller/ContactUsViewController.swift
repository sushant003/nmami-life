//
//  ContactUsViewController.swift
//  DtShreya
//
//  Created by kwikard on 14/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class ContactUsViewController: BaseViewController {

    var tableData : [[String]] = [["Chandigarh Clinic","SCO: 411-412, First Floor, Sector: 35-c, Chandigarh","+91-7307303835","spteamshreya@gmail.com"],["Panchkula Clinic","SCO: 215, Sector: 14, BEL Colony, Panchkula, Haryana - 134109","+91-7307303835","spteamshreya@gmail.com"],["Ludhiyana Clinic","483-R, 2nd Floor, Near Deep Hospital, Model Towhn, Ludhiyana","+91-7307303835","spteamshreya@gmail.com"],["Patiala Clinic","SCO: 9, First Floor, Bhupindra road, Punjabi Bagh, Patiala","+91-7307303835","spteamshreya@gmail.com"],["Ambala Clinic","434 Manuali House, Prem Nagar Road, near petrol pump, Prem Nagar","+91-7307303835","spteamshreya@gmail.com"],["South Delhi Clinic","M46, M Block, Main market, above Canara Bank, GK-2, South Delhi","+91-7307303835","spteamshreya@gmail.com"],["Jalandhar Clinic","SCO: 26, above Forkwork Restaraunt, choti baradari, Garha Road, Jalandhar, Punjab- 144022","+91-7307303835","spteamshreya@gmail.com"],["Amritsar","1/A First Floor, Opposite Bansal Sweets, Shiv market, Lawrence Road, Amritsar Punjab","+91-7307303835","spteamshreya@gmail.com"],["West Delhi Clinic","#8 Club Road, Opposite Hair Master, Near Bikaner Sweets, Punjabi Bagh, Delhi","+91-7307303835","spteamshreya@gmail.com"],["Mumbai Clinic","206 Shri Krishna Building, Near Fun Republic, New Link Road, Andheri West, Mumbai","+91-7307303835","spteamshreya@gmail.com"]]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension ContactUsViewController : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        (cell.viewWithTag(1) as! UILabel).text = tableData[indexPath.row][0]
        (cell.viewWithTag(2) as! UILabel).text = tableData[indexPath.row][1]
        (cell.viewWithTag(3) as! UILabel).text = tableData[indexPath.row][2]
        (cell.viewWithTag(4) as! UILabel).text = tableData[indexPath.row][3]
        return cell
    }
}
