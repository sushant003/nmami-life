//
//  FeedbackViewController.swift
//  DtShreya
//
//  Created by kwikard on 12/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit
import Cosmos

class FeedbackViewController: BaseViewController {

    @IBOutlet weak var feedbackTV: UITextView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var userImage: CustomImage!
    @IBOutlet weak var serverStatusView: UIView!
    @IBOutlet weak var serverStatusImage: UIImageView!
    @IBOutlet weak var serverStatusLabel: UILabel!
    @IBOutlet weak var dietitianName: UILabel!
    var dietitianId : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        serverStatusImage.isHidden = true
        feedbackTV.layer.cornerRadius = 5
        feedbackTV.text = "Leave a comment here..."
        feedbackTV.textColor = UIColor.lightGray
        getIds()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
    }
    
    func setServerStatus(text : String) {
        serverStatusView.isHidden = false
        serverStatusImage.isHidden = false
        serverStatusLabel.text = text
    }
    
    //MARK: IBAction
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func onClickSubmit(_ sender: Any) {
        self.view.endEditing(true)
        if feedbackTV.text == "" || feedbackTV.text == "Leave a comment here..." {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please write a review first.", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
        
        else {
            giveReview()
        }
    }
    
    //MARK: Webservce methods
    
    func getIds() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.serverStatusView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.getDietitianInfoNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.getDietitianIDServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: AppConstants.Dashboard.getDietitianIDServiceSuffix, notificationKey: AppConstants.Dashboard.getDietitianIDServiceNotify, controllerName: self,requestType: "")
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Net_Error)
        }
    }
    
    @objc func getDietitianInfoNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.serverStatusView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                serverStatusView.isHidden = true
                
                let resultDict: [[String:Any]] = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as? [[String:Any]])!
                if resultDict.count == 0 {
                    setServerStatus(text: message!)
                    return
                }
                for item in resultDict {
                    dietitianName.text = item[AppConstants.ServiceKeys.dietitianName] as? String
                    AppHelper.getInstance().loadImageIntoImageView(imageUrl: item[AppConstants.ServiceKeys.dietitianPic] as! String, imageView: userImage, placeHolder: "user_placeholder")
                    dietitianId = item[AppConstants.ServiceKeys.dietitianId] as! Int
                }
            }
            else {
                setServerStatus(text: message!)
            }
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Server_Error)
        }
    }
    
    func giveReview() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.giveReviewCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.getDietitianIDServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: ["from":AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),"to":dietitianId,"star":ratingView.rating,"review":feedbackTV.text!], url: AppConstants.Dashboard.giveReviewServiceSuffix, notificationKey: AppConstants.Dashboard.getDietitianIDServiceNotify, controllerName: self,requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func giveReviewCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Review submitted successfully", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: message!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}

extension FeedbackViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Leave a comment here..."
            textView.textColor = UIColor.lightGray
        }
    }
}

