//
//  DietStatsViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 26/09/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit
import Charts
import DropDown

class DietStatsViewController: BaseViewController {

    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var filterLabel: UILabel!
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var dateContainerView: CustomView!
    @IBOutlet weak var dropDownPickerView: CustomView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var chartContainer: CustomView!
    
    var datePickerController : DatePickerViewController! = nil
    
    var dietStatModel : [DietStatsModel] = [DietStatsModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        datePickerController = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as? DatePickerViewController
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        dateLabel.text = formatter.string(from: Date())
        getDietStats()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
        chartContainer.createShadow()
    }
    
    //MARK: IBAction
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onTapDatePicker(_ sender: Any) {
        self.view.endEditing(true)
        self.datePickerController.data = ["pickerType":"date"]
        self.datePickerController.datePickerDelegate = self
        self.addChildViewController(self.datePickerController)
        self.view.addSubview(self.datePickerController.view)
        self.datePickerController.didMove(toParentViewController: self)
    }
    
    @IBAction func onTapDropDown(_ sender: Any) {
        AppHelper.getInstance().prepareDropdown(anchor: self.dropDownPickerView, dropdownList: [AppConstants.Other.weekly,AppConstants.Other.monthly], controller: self)
    }
    
    //MARK: Webservice Methods
    func getDietStats() {
        if AppHelper.getInstance().isNetworkAvailable() {
            NotificationCenter.default.addObserver(self,selector: #selector(self.catchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_diet_stats_service_notify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.date:dateLabel.text!,AppConstants.ServiceKeys.tag:(filterLabel.text == AppConstants.Other.weekly ? "2" : "3")], url: AppConstants.Clients.get_diet_stats_service_suffix, notificationKey: AppConstants.Clients.get_diet_stats_service_notify, controllerName: self,requestType: "")
            AppHelper.getInstance().showProgressIndicator(view: self.view)
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func catchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            dietStatModel.removeAll()
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                let result = serviceData.userInfo?[AppConstants.ServiceKeys.result] as! [[String:Any]]
                for stats in result  {
                    if stats[AppConstants.ServiceKeys.data] as! Int != 0 {
                        let statsModel = DietStatsModel(date: stats[AppConstants.ServiceKeys.date] as! String, taken: Int(stats[AppConstants.ServiceKeys.taken] as! String)!, missed: Int(stats[AppConstants.ServiceKeys.missed] as! String)!)
                        dietStatModel.append(statsModel)
                    }
                }
                if dietStatModel.count != 0 {
                    setLineGraph()
                    setPieChart()
                }
                else {
                    lineChartView.clear()
                    pieChartView.clear()
                }
            }
            else {
                
                lineChartView.clear()
                pieChartView.clear()
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    func setLineGraph() {
        let formatter : BarChartFormatter = BarChartFormatter()
        let xaxis : XAxis = XAxis()
        var taken : [ChartDataEntry] = []
        var missed : [ChartDataEntry] = []
        var days : [String]! = [String]()
        lineChartView.xAxis.resetCustomAxisMax()
        lineChartView.xAxis.resetCustomAxisMin()
        lineChartView.xAxis.valueFormatter = nil
        BarChartFormatter.days.removeAll()
        for i in 0..<dietStatModel.count {
            let dateformatter = DateFormatter()
            dateformatter.dateFormat = "dd-MM-yyyy"
            let date = dateformatter.date(from: dietStatModel[i].date)
            let dateformatter1 = DateFormatter()
            dateformatter1.dateFormat = "E"
            days.append(dateformatter1.string(from: date!))
            if filterLabel.text == "Monthly" {
                let takentData = ChartDataEntry(x: Double(dietStatModel[i].date.components(separatedBy: "-")[0])!, y: Double(dietStatModel[i].taken))
                let missedData = ChartDataEntry(x: Double(dietStatModel[i].date.components(separatedBy: "-")[0])!, y: Double(dietStatModel[i].missed))
                taken.append(takentData)
                missed.append(missedData)
            }
            else {
                let takentData = ChartDataEntry(x: Double(i), y: Double(dietStatModel[i].taken), data: days as AnyObject)
                let missedData = ChartDataEntry(x: Double(i), y: Double(dietStatModel[i].missed), data: days as AnyObject)
                taken.append(takentData)
                missed.append(missedData)
            }
        }
        
        let line1 = LineChartDataSet(values: taken, label: "Taken  ")
        line1.valueFormatter = YAxisValueFormatter() as? IValueFormatter
        let line2 = LineChartDataSet(values: missed, label: "Missed  ")
        let data = LineChartData(dataSets: [line1,line2])
        if filterLabel.text != "Monthly" {
            
            formatter.setXAxisArray(data: days)
            xaxis.valueFormatter = formatter
            lineChartView.xAxis.valueFormatter = xaxis.valueFormatter
            lineChartView.xAxis.spaceMin = 0.5
            lineChartView.xAxis.spaceMax = 0.5
            lineChartView.xAxis.labelCount = dietStatModel.count
            lineChartView.xAxis.granularityEnabled  = true
            lineChartView.xAxis.granularity = 1
        }
        else {
            lineChartView.xAxis.axisMinimum = 0
            lineChartView.xAxis.axisMaximum = 31
            lineChartView.xAxis.labelCount = 15
        }
        
        lineChartView.xAxis.labelPosition = XAxis.LabelPosition.bottom
        lineChartView.xAxis.drawGridLinesEnabled = false
        lineChartView.leftAxis.drawGridLinesEnabled = false
        lineChartView.rightAxis.enabled = false
        
        line1.valueFont = UIFont.systemFont(ofSize: 10)
        line2.valueFont = UIFont.systemFont(ofSize: 10)
        line1.colors = [AppConstants.Colors.greenColor]
        line2.colors = [AppConstants.Colors.blueColor]
        
        let formatter1 = NumberFormatter()
        formatter1.minimumFractionDigits = 0
        data.setValueFormatter(DefaultValueFormatter(formatter:formatter1))
        line1.circleRadius = 3
        line1.highlightEnabled = false
        line2.highlightEnabled = false
        line2.circleRadius = 3
        line1.circleColors = [AppConstants.Colors.blueColor]
        line2.circleColors = [AppConstants.Colors.greenColor]
        lineChartView.chartDescription?.text = ""
        lineChartView.leftAxis.axisMinimum = 0
        lineChartView.leftAxis.axisMaximum = 12
        lineChartView.data?.clearValues()
        lineChartView.data = data
        lineChartView.notifyDataSetChanged()
    }
    
    func setPieChart() {
        let takenCount = dietStatModel.reduce(0) { (first, second) -> Int in
            return first + second.taken
        }
        let missedCount = dietStatModel.reduce(0) { (first, second) -> Int in
            return first + second.missed
        }
        let takenCountEntry = PieChartDataEntry(value: Double(takenCount), label: "\(takenCount)")
        let missedCountEntry = PieChartDataEntry(value: Double(missedCount), label: "\(missedCount)")
        let dataSet = PieChartDataSet(values: [takenCountEntry,missedCountEntry], label: "")
        dataSet.colors = [AppConstants.Colors.greenColor,AppConstants.Colors.blueColor]
        dataSet.valueTextColor = UIColor.clear
        dataSet.entryLabelColor = UIColor.white
        let data = PieChartData(dataSet: dataSet)
        dataSet.entryLabelFont = UIFont.systemFont(ofSize: 11)
        pieChartView.legend.enabled = false
        
        pieChartView.data = data
        pieChartView.chartDescription?.text = ""
        pieChartView.notifyDataSetChanged()
    }
}

extension DietStatsViewController : CustomDropDownDelegate {
    func getSelectedItem(item: String) {
        filterLabel.text = item
        lineChartView.clear()
        lineChartView.clearValues()
        lineChartView.clearAllViewportJobs()
//        pieChartView.clear()
        setLineGraph()
//        setPieChart()
    }
}

//MARK: Datepicker Delegate
extension DietStatsViewController : DatePickerListener {
    func setDateFromPicker(date: Any) {
        datePickerController.view.removeFromSuperview()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateLabel.text = dateFormatter.string(from: date as! Date)
        getDietStats()
    }
    
    func dismissDatePicker() {
        datePickerController.view.removeFromSuperview()
        getDietStats()
    }
}
