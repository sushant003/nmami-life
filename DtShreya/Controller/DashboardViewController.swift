//
//  DashboardViewController.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 19/02/18.
//  Copyright © 2018 Medicians. All rights reserved.
//
import UIKit

class DashboardViewController: BaseViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var tergetLabel: UILabel!
    @IBOutlet weak var achievedLabel: UILabel!
    @IBOutlet weak var steps: UILabel!
    @IBOutlet weak var caloriesBurnt: UILabel!
    @IBOutlet weak var dietitianName: UILabel!
    @IBOutlet weak var programName: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var articleTV: UITableView!
    
    @IBOutlet weak var serverStatusView: UIView!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var serverStatusLabel: UILabel!
    
    @IBOutlet weak var boxOne: CustomView!
    @IBOutlet weak var boxTwo: CustomView!
    @IBOutlet weak var boxThree: CustomView!
    @IBOutlet weak var boxFour: CustomView!
    var healthKit = HealthKit()
    var serverDataModel : DashboardModel = DashboardModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        statusImage.isHidden = true
        healthKit.checkAuthorization()
        healthKit.delegate = self
        getHomeData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        boxOne.createShadow()
        boxTwo.createShadow()
        boxThree.createShadow()
        boxFour.createShadow()
        articleTV.visibleCells.forEach { (cell) -> Void  in
            (cell.viewWithTag(1) as! CustomView).createShadow()
        }
        tableHeightConstraint.constant = articleTV.contentSize.height
        
    }
    
    
    //MARK: IBAction
    @IBAction func onClickReadMore(_ sender: UIButton) {
        let indexPath = articleTV.indexPath(for: sender.superview?.superview?.superview as! UITableViewCell)!
        AppHelper.getInstance().pushControllerWithData(id: "NavigationWebViewController", controllerName: self, data: ["link":serverDataModel.trending[indexPath.row].link,"pageName":serverDataModel.trending[indexPath.row].title])
//        UIApplication.shared.open(URL(string : serverDataModel.trending[indexPath.row].link)!, options: [:], completionHandler: { (status) in
//        })
    }
    
    @IBAction func onClickShare(_ sender: UIButton) {
        let indexPath = articleTV.indexPath(for: sender.superview?.superview?.superview as! UITableViewCell)!
        let shareItem = [serverDataModel.trending[indexPath.row].link]
        let avc = UIActivityViewController(activityItems: shareItem, applicationActivities: nil)
        self.present(avc, animated: true, completion: nil)
    }
    
    //MARK: Webservice Method
    func getHomeData() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.serverStatusView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.homeCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.getDashboardDataServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: AppConstants.Dashboard.getDashboardDataServiceSuffix, notificationKey: AppConstants.Dashboard.getDashboardDataServiceNotify, controllerName: self,requestType: "")
        }
        else {
            serverStatusView.isHidden = false
            serverStatusLabel.text = AppConstants.ErrorMessags.Net_Error
            statusImage.isHidden = false
        }
    }
    
    @objc func homeCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.serverStatusView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                serverStatusView.isHidden = true
                self.prepareHomeData(serverDict : serviceData.userInfo![AppConstants.ServiceKeys.result] as! [String:Any])
//                articleTV.dataSource = self
//                articleTV.delegate = self
                articleTV.reloadData()
                
            }
            else {
                serverStatusView.isHidden = false
                serverStatusLabel.text = message
                statusImage.isHidden = false
            }
        }
        else {
            serverStatusView.isHidden = false
            serverStatusLabel.text = AppConstants.ErrorMessags.Server_Error
            statusImage.isHidden = false
        }
    }
    
    func prepareHomeData(serverDict : [String:Any]) {
        serverDataModel.achive = serverDict[AppConstants.ServiceKeys.achieved] as! String
        serverDataModel.target = serverDict[AppConstants.ServiceKeys.target] as! String
        serverDataModel.name = serverDict[AppConstants.ServiceKeys.name] as! String
        serverDataModel.phone = serverDict[AppConstants.ServiceKeys.phone] as! [String]
        serverDataModel.profileImage = serverDict[AppConstants.ServiceKeys.image] as! String
        serverDataModel.currentPlan = serverDict[AppConstants.ServiceKeys.currentPlan] as! String
        (serverDict[AppConstants.ServiceKeys.trending] as! [[String:String]]).forEach { (trending) in
            var trend = TrendingModel()
            trend.title = trending[AppConstants.ServiceKeys.title]!
            trend.image = trending[AppConstants.ServiceKeys.image]!
            trend.link = trending[AppConstants.ServiceKeys.link]!
            trend.description = trending[AppConstants.ServiceKeys.description]!
            serverDataModel.trending.append(trend)
        }
        setData()
    }
    
    func setData() {
        dietitianName.text = serverDataModel.name
        programName.text = serverDataModel.currentPlan
        tergetLabel.text = serverDataModel.target + " Kg"
        achievedLabel.text = serverDataModel.achive + " Kg"
        AppHelper.getInstance().loadImageIntoImageView(imageUrl: (AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.image) as? String)!, imageView: profileImageView, placeHolder: "user_placeholder")
        AppHelper.getInstance().setValueForUserDefault(value: serverDataModel.profileImage, key: AppConstants.userDefaultConstants.profileImage)
        AppHelper.getInstance().setValueForUserDefault(value: serverDataModel.name, key: AppConstants.userDefaultConstants.userName)
        
    }
}

extension DashboardViewController : HealthKitDelegate {
    func authorizationCompleted() {
        healthKit.recentSteps { (steps, error) in
            if error == nil {
                DispatchQueue.main.async {
                    self.steps.text = "\(steps)"
                }
                
            }
        }
        healthKit.energyBurnt { (calories, error) in
            if error == nil {
                DispatchQueue.main.async {
                    self.caloriesBurnt.text = "\(calories)"
                }
            }
        }
    }
}

extension DashboardViewController : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serverDataModel.trending.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        (cell.viewWithTag(2) as! UILabel).text = serverDataModel.trending[indexPath.row].title
        (cell.viewWithTag(4) as! UILabel).text = serverDataModel.trending[indexPath.row].description
        AppHelper.getInstance().loadImageIntoImageView(imageUrl: serverDataModel.trending[indexPath.row].image, imageView: (cell.viewWithTag(3) as! UIImageView), placeHolder: "")
//        (cell.viewWithTag(1) as! CustomView).createShadow()
        self.viewDidLayoutSubviews()
        return cell
    }
    
}
