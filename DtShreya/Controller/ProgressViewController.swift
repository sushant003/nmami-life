//
//  ProgressViewController.swift
//  Meditician
//
//  Created by Sushant Jugran on 20/04/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit
import AVFoundation
import Charts

class ProgressViewController: BaseViewController {
    
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var chartContainerView: CustomView!
    @IBOutlet weak var targetContainerView: CustomView!
    @IBOutlet weak var achievedContainerView: CustomView!
    @IBOutlet weak var targetWeightLabel: UILabel!
    @IBOutlet weak var targetBMILabel: UILabel!
    @IBOutlet weak var achievedWeightLabel: UILabel!
    @IBOutlet weak var achievedBMILabel: UILabel!
    
    var progressData : ProgressModel = ProgressModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        statusImage.isHidden = true
        getProgress()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        chartContainerView.createShadow()
        targetContainerView.createShadow()
        achievedContainerView.createShadow()
    }
    
    //MARK: IBAction
    @IBAction func onClickAddButton(_ sender: Any) {
        AppHelper.getInstance().pushControllerWithData(id: "AddProgressViewController", controllerName: self, data: self)
    }
    
    
    //MARK: Webservice Methods
    func getProgress() {
        progressData = ProgressModel()
        if AppHelper.getInstance().isNetworkAvailable() {
            NotificationCenter.default.addObserver(self,selector: #selector(self.catchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_progress_service_notify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: AppConstants.Clients.get_progress_service_suffix, notificationKey: AppConstants.Clients.get_progress_service_notify, controllerName: self,requestType: "")
            AppHelper.getInstance().showProgressIndicator(view: self.view)
        }
        else {
            statusImage.isHidden = false
            statusImage.isHidden = false
            statusLabel.text = AppConstants.ErrorMessags.Net_Error
        }
    }
    
    @objc func catchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                statusView.isHidden = true
                let result = serviceData.userInfo?[AppConstants.ServiceKeys.result] as! [String:Any]
                progressData.achievedWeight = Float(result[AppConstants.ServiceKeys.achivedWeight] as! String)
                progressData.achievedBMI = Float(result[AppConstants.ServiceKeys.achivedBMI] as! String)
                progressData.targetWeight = Float(result[AppConstants.ServiceKeys.targetWeight] as! String)
                progressData.targetBMI = Float(result[AppConstants.ServiceKeys.targetBMI] as! String)
                for item in result[AppConstants.ServiceKeys.info] as! [[String:Any]] {
                    var infoObject = ProgressInfo()
                    infoObject.bmi = Float(item[AppConstants.ServiceKeys.BMI] as! String)
                    infoObject.date = (item[AppConstants.ServiceKeys.date] as? String)
                    infoObject.sugar = Float("\(item[AppConstants.ServiceKeys.sugar]!)")
                    infoObject.weight = Float(item[AppConstants.ServiceKeys.weight] as! String)
                    progressData.info.append(infoObject)
                }
                targetWeightLabel.text = "Weight: \(progressData.targetWeight!) Kgs"
                achievedWeightLabel.text = "Weight: \(progressData.achievedWeight!) Kgs"
                targetBMILabel.text = "BMI: \(progressData.targetBMI!)"
                achievedBMILabel.text = "BMI:\(progressData.achievedBMI!)"
                if progressData.info.count > 0 {
                    setLineGraph()
                }
            }
            else {
                statusImage.isHidden = false
                statusImage.isHidden = false
                statusLabel.text = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            }
        }
        else {
            statusImage.isHidden = false
            statusImage.isHidden = false
            statusLabel.text = AppConstants.ErrorMessags.Server_Error
        }
    }
    
    func setLineGraph() {
        var weight : [ChartDataEntry] = []
        var bmi : [ChartDataEntry] = []
        
        for i in 0..<progressData.info.count{
            
            
            let weightData = ChartDataEntry(x: Double(progressData.info[i].date.components(separatedBy: "-")[0])!, y: Double(progressData.info[i].weight))
            let bmiData = ChartDataEntry(x: Double(progressData.info[i].date.components(separatedBy: "-")[0])!, y: Double(progressData.info[i].bmi))
            
            weight.append(weightData)
            bmi.append(bmiData)
        }
        lineChartView.xAxis.axisMinimum = 0
        lineChartView.xAxis.axisMaximum = 31
        lineChartView.xAxis.labelCount = 15
        lineChartView.xAxis.labelPosition = XAxis.LabelPosition.bottom
        lineChartView.xAxis.drawGridLinesEnabled = false
        lineChartView.leftAxis.drawGridLinesEnabled = false
        lineChartView.rightAxis.enabled = false
        let line1 = LineChartDataSet(values: weight, label: "Weight Loss  ")
        let line3 = LineChartDataSet(values: bmi, label: "BMI")
        line1.colors = [AppConstants.Colors.blueColor]
        line3.colors = [AppConstants.Colors.greenColor]
        let data = LineChartData(dataSets: [line1,line3])
        line1.circleRadius = 3
        line3.circleRadius = 3
        line1.circleColors = [AppConstants.Colors.blueColor]
        line3.circleColors = [AppConstants.Colors.blueColor]
        lineChartView.chartDescription?.text = ""
        lineChartView.leftAxis.axisMinimum = 0
        lineChartView.leftAxis.axisMaximum = 200
        lineChartView.data = data
    }
}

extension ProgressViewController : throwData {
    func sendPopedData(data: Any) {
        getProgress()
    }
}
