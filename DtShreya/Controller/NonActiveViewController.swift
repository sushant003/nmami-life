//
//  NonActiveViewController.swift
//  DtShreya
//
//  Created by kwikard on 14/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class NonActiveViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var serverStatusView: UIView!
    @IBOutlet weak var serverStatusLabel: UILabel!
    @IBOutlet weak var serverStatusImage: UIImageView!
    var tableData : [NonActiveDashboardModel] = [NonActiveDashboardModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        serverStatusImage.image = UIImage(named: "")
        // Do any additional setup after loading the view.
        getData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for cell in tableView.visibleCells {
            (cell.viewWithTag(1) as! CustomView).createShadow()
        }
    }
    
    func setServerStatus(text : String) {
        serverStatusView.isHidden = false
        serverStatusImage.image = #imageLiteral(resourceName: "serverError")
        serverStatusLabel.text = text
    }
    
    //MARK: Webservice Methods
    func getData() {
        tableData.removeAll()
        var selectedListName = ""
        switch (data as! Int) {
        case 0:
            selectedListName = "Activities"
        case 1:
            selectedListName = "Blog"
        case 2:
            selectedListName = "Health Tips"
        default:
            break
        }
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.serverStatusView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.homeCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.getDashboardCategoryNotify),object: nil)
            Webservice.getInstance().hitService(parameter: ["name":selectedListName], url: AppConstants.Dashboard.getDashboardCategorySuffix, notificationKey: AppConstants.Dashboard.getDashboardCategoryNotify, controllerName: self,requestType: "")
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Net_Error)
        }
    }

    @objc func homeCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.serverStatusView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                
                let resultDict: [[String:String]] = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as? [[String:String]])!
                for dict in resultDict {
                    let model = NonActiveDashboardModel(title: dict["title"]!, imagelink: dict["imagelink"]!, bloglink: dict["bloglink"]!)
                    tableData.append(model)
                }
                if tableData.count == 0 {
                    setServerStatus(text: "No data found")
                }
                else {
                    serverStatusView.isHidden = true
                    self.tableView.delegate = self
                    self.tableView.dataSource = self
                    tableView.reloadData()
                    self.viewDidLayoutSubviews()
                }
                
            }
            else {
                setServerStatus(text: message!)
            }
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Server_Error)
        }
    }
    
    //MARK: IABAction
    
    @IBAction func onClickOpenLink(_ sender: UIButton) {
        let indexPath = tableView.indexPath(for: sender.superview?.superview?.superview as! UITableViewCell)!
        UIApplication.shared.open(URL(string : tableData[indexPath.row].bloglink)!, options: [:], completionHandler: { (status) in
        })
    }
    
    @IBAction func onClickShare(_ sender: UIButton) {
        let indexPath = tableView.indexPath(for: sender.superview?.superview?.superview as! UITableViewCell)!
        let shareItem = [tableData[indexPath.row].bloglink]
        let avc = UIActivityViewController(activityItems: shareItem, applicationActivities: nil)
        self.present(avc, animated: true, completion: nil)
    }
    
}

extension NonActiveViewController : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        (cell.viewWithTag(2) as! UILabel).text = tableData[indexPath.row].title
        AppHelper.getInstance().loadImageIntoImageView(imageUrl: tableData[indexPath.row].imagelink, imageView: (cell.viewWithTag(3) as! UIImageView), placeHolder: "")
        return cell
    }
}
