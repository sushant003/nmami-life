//
//  CreateDiaryViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 13/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class CreateDiaryViewController: BaseViewController {
    
    @IBOutlet weak var sleepHoursTF: UITextField!
    @IBOutlet weak var weightTF: UITextField!
    @IBOutlet weak var cheatTF: UITextField!
    @IBOutlet weak var stepsTF: UITextField!
    @IBOutlet weak var calorieBurnt: UITextField!
    @IBOutlet weak var headerView: CustomView!
    var healthKit = HealthKit()
    override func viewDidLoad() {
        super.viewDidLoad()
        healthKit.checkAuthorization()
        healthKit.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
    }
    
    //IBAction
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickSaveButton(_ sender: Any) {
        let params = [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),
                      AppConstants.ServiceKeys.date:(data as! [String:Any])["date"]!,
                      AppConstants.ServiceKeys.weight:weightTF.text!,
                      AppConstants.ServiceKeys.steps:stepsTF.text!,
                      AppConstants.ServiceKeys.sleepHours:sleepHoursTF.text!,
                      AppConstants.ServiceKeys.calories:calorieBurnt.text!,
                      AppConstants.ServiceKeys.cheatMeals:cheatTF.text!,
        ]
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.homeCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Appointments.create_diary_service_notify),object: nil)
            Webservice.getInstance().hitService(parameter: params as NSDictionary, url: AppConstants.Appointments.create_diary_service_suffix, notificationKey: AppConstants.Appointments.create_diary_service_notify, controllerName: self,requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func homeCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                ((data as! [String:Any])["controller"] as! throwData).sendPopedData(data: "")
                navigationController?.popViewController(animated: true)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: message!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}

extension CreateDiaryViewController : HealthKitDelegate {
    func authorizationCompleted() {
        healthKit.recentSteps { (steps, error) in
            if error == nil {
                DispatchQueue.main.async {
                    self.stepsTF.text = "\(steps)"
                }
            }
        }
        healthKit.energyBurnt { (calories, error) in
            if error == nil {
                DispatchQueue.main.async {
                    self.calorieBurnt.text = "\(calories)"
                }
            }
        }
    }
}
