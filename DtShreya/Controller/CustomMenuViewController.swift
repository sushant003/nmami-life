//
//  CustomMenuViewController.swift
//  GDI
//
//  Created by Sushant Jugran on 02/07/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit
import SideMenuController

class CustomMenuViewController: SideMenuController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let navigationController = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        let menuController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        sideMenuController?.embed(sideViewController: navigationController)
        sideMenuController?.embed(centerViewController: menuController)
    }
}
