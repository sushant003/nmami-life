//
//  WeeklyDietViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 12/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class WeeklyDietViewController: BaseViewController {
    
    @IBOutlet weak var headerView: CustomView!
    
    @IBOutlet weak var dietTableView: UITableView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dateCV: UICollectionView!
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    var supplementCollapsed : Bool = true
    var selectedIndex : Int = 0
    var mealData : [String:[MealModel]] =  [String:[MealModel]]()
    var sortedDateArray : [String]? = nil
    var supplements : [String:[String]] = [String:[String]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let headerNib = UINib.init(nibName: "HeaderView", bundle: Bundle.main)
        dietTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "HeaderView")
        collectionViewFlowLayout.itemSize = CGSize(width: view.frame.width/7, height: 60)
        dietTableView.sectionFooterHeight = 2.0
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-yyyy"
        dateLabel.text = formatter.string(from: Date())
        statusImage.isHidden = true
        getDietOfSevenDays()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
    }
    
    //MARK: IBAction
    @IBAction func onClickBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickReplaceDiet(_ sender: UIButton) {
        let indexPath = dietTableView.indexPath(for: sender.superview?.superview as! UITableViewCell)!
        mealData[sortedDateArray![selectedIndex]]![indexPath.section].meal[indexPath.row].mealChanged = !mealData[sortedDateArray![selectedIndex]]![indexPath.section].meal[indexPath.row].mealChanged
        dietTableView.reloadRows(at: [IndexPath(row: indexPath.row, section: indexPath.section)], with: .none)
    }
    
    //MARK: Webservice methods
    //formatter.string(from: Date())
    func getDietOfSevenDays() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.dietTableView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.getDietCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_seven_service_notify),object: nil)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.date:formatter.string(from: Date())], url: AppConstants.Clients.get_seven_diet_service_suffix, notificationKey: AppConstants.Clients.get_seven_service_notify, controllerName: self, requestType: "")
        }
        else {
            statusView.isHidden = false
            statusImage.isHidden = false
            statusLabel.text = AppConstants.ErrorMessags.Net_Error
        }
    }
    
    @objc func getDietCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.dietTableView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                statusView.isHidden = true
                let resultDict: [[String:Any]] = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as! [[String:Any]])
                prepareTableData(serviceData: resultDict)
                dietTableView.dataSource = self
                dietTableView.delegate = self
                if mealData[sortedDateArray![selectedIndex]]?.count == 0 {
                    statusView.isHidden = false
                    statusImage.isHidden = false
                    statusLabel.text = "No Diet Found"
                }
                else {
                    statusView.isHidden = true
                    
                    dietTableView.reloadData()
                }
            }
            else {
                statusView.isHidden = false
                statusImage.isHidden = false
                statusLabel.text = message
            }
        }
        else {
            statusView.isHidden = false
            statusImage.isHidden = false
            statusLabel.text = AppConstants.ErrorMessags.Server_Error
        }
    }
    
    func prepareTableData(serviceData : [[String:Any]]) {
        for data in serviceData {
            var meals : [MealModel] = [MealModel]()
            if data[AppConstants.ServiceKeys.data] as! Int == 1 {
                for (key,value) in (data[AppConstants.ServiceKeys.entry] as! [String:Any])[AppConstants.ServiceKeys.diet] as! [String:Any] {
                    var meal : MealModel!
                    var subMeal : [SubMealModel] = [SubMealModel]()
                    if ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool) {
                        for i in 0..<((value as! [String:Any])[AppConstants.ServiceKeys.meal] as! [String]).count {
                            subMeal.append(SubMealModel(firstFoodName: ((value as! [String:Any])[AppConstants.ServiceKeys.meal] as! [String])[i], secondFoodName: ((value as! [String:Any])[AppConstants.ServiceKeys.meal1] as! [String])[i], firstFoodQuantity: ((value as! [String:Any])[AppConstants.ServiceKeys.quantity] as! [String])[i], secondFoodQuantity: ((value as! [String:Any])[AppConstants.ServiceKeys.quantity1] as! [String])[i], option: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool), mealChanged: false))
                        }
                    }
                    else {
                        for i in 0..<((value as! [String:Any])[AppConstants.ServiceKeys.meal] as! [String]).count {
                            subMeal.append(SubMealModel(firstFoodName: ((value as! [String:Any])[AppConstants.ServiceKeys.meal] as! [String])[i], secondFoodName: "", firstFoodQuantity: ((value as! [String:Any])[AppConstants.ServiceKeys.quantity] as! [String])[i], secondFoodQuantity: "", option: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool), mealChanged: false))
                        }
                    }
                    switch key {
                    case AppConstants.ServiceKeys.firstMeal:
                        meal = MealModel(name: AppConstants.ServiceKeys.firstMealName, meal: subMeal, collapsed: true, mealIndex: 0, missed: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool))
                    case AppConstants.ServiceKeys.secondMeal:
                        meal = MealModel(name: AppConstants.ServiceKeys.secondMealName, meal: subMeal, collapsed: true, mealIndex: 1, missed: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool))
                    case AppConstants.ServiceKeys.thirdMeal:
                        meal = MealModel(name: AppConstants.ServiceKeys.thirdMealName, meal: subMeal, collapsed: true, mealIndex: 2, missed: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool))
                    case AppConstants.ServiceKeys.fourthMeal:
                        meal = MealModel(name: AppConstants.ServiceKeys.fourthMealName, meal: subMeal, collapsed: true, mealIndex: 3, missed: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool))
                    case AppConstants.ServiceKeys.fifthMeal:
                        meal = MealModel(name: AppConstants.ServiceKeys.fifthMealName, meal: subMeal, collapsed: true, mealIndex: 4, missed: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool))
                    case AppConstants.ServiceKeys.sixthMeal:
                        meal = MealModel(name: AppConstants.ServiceKeys.sixthMealName, meal: subMeal, collapsed: true, mealIndex: 5, missed: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool))
                    case AppConstants.ServiceKeys.seventhMeal:
                        meal = MealModel(name: AppConstants.ServiceKeys.seventhMealName, meal: subMeal, collapsed: true, mealIndex: 6, missed: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool))
                    case AppConstants.ServiceKeys.eighthMeal:
                        meal = MealModel(name: AppConstants.ServiceKeys.eightMealName, meal: subMeal, collapsed: true, mealIndex: 7, missed: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool))
                    default:
                        break
                    }
                    if key != "nine" {
                        if meal.meal.count != 0 {
                        meals.append(meal!)
                        }
                    }
                }
                supplements[data[AppConstants.ServiceKeys.date] as! String] = ((data[AppConstants.ServiceKeys.entry] as! [String:Any])[AppConstants.ServiceKeys.supplements] as! [String]).map({ (sup) in
                    return sup
                })
            }
            meals = meals.sorted(by: { (first, second) -> Bool in
                first.index < second.index
            })
            mealData[data[AppConstants.ServiceKeys.date] as! String] = meals
            
        }
        sortedDateArray = Array(mealData.keys).sorted()
    }
    
    func commitMealActions(meal:String,actionType:Int) {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.statusView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.changeDietStatusNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_diet_service_notify),object: nil)
            let formatter = DateFormatter() 
            formatter.dateFormat = "dd-MM-yyyy"
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.date:sortedDateArray![selectedIndex],AppConstants.ServiceKeys.mealNumber:meal,AppConstants.ServiceKeys.action:actionType], url: AppConstants.Clients.change_diet_status_suffix, notificationKey: AppConstants.Clients.get_diet_service_notify, controllerName: self, requestType: "")
            AppHelper.getInstance().showProgressIndicator(view: statusView)
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func changeDietStatusNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.statusView)
        if(serviceData.userInfo != nil) {
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Request recorded", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: (serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String)!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}


//MARK: Tableview datasource and delegate
extension WeeklyDietViewController : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if mealData[sortedDateArray![selectedIndex]]!.count == 0 {
            return 0
        }
        else {
            if supplements[sortedDateArray![selectedIndex]]!.count == 0 {
                return (mealData[sortedDateArray![selectedIndex]]?.count)!
            }
            else {
                return (mealData[sortedDateArray![selectedIndex]]?.count)! + 1
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section < mealData.count  {
            return mealData[sortedDateArray![selectedIndex]]![section].collapsed ? 0 : mealData[sortedDateArray![selectedIndex]]![section].meal.count
        }
        else {
            return supplementCollapsed ? 0 : supplements[sortedDateArray![selectedIndex]]!.count
        }        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as! HeaderView
        headerView.delegate = self
        headerView.seactionIndex = section
        headerView.collapseButton.isHidden = true
        headerView.actionButton.isHidden = false
        headerView.dropDownButton.isHidden = false
        if section < mealData[sortedDateArray![selectedIndex]]!.count {
            headerView.isCollapsed = mealData[sortedDateArray![selectedIndex]]![section].collapsed
            headerView.headerLabel.text = mealData[sortedDateArray![selectedIndex]]![section].mealTimeName
        }
        else {
            headerView.isCollapsed = supplementCollapsed
            headerView.headerLabel.text = AppConstants.ServiceKeys.supplementsName
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 25
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "mealCell", for: indexPath)
        if indexPath.section == mealData[sortedDateArray![selectedIndex]]!.count {
            (cell.viewWithTag(1) as! UILabel).text = supplements[sortedDateArray![selectedIndex]]![indexPath.row]
            (cell.viewWithTag(2) as! UILabel).text = ""
            (cell.viewWithTag(3) as! UIButton).isHidden = true
            
            for constraint in (cell.viewWithTag(3) as! UIButton).constraints {
                if constraint.identifier == "height" {
                    constraint.constant = -5
                }
            }
        }
        else {
            (cell.viewWithTag(1) as! UILabel).text = mealData[sortedDateArray![selectedIndex]]![indexPath.section].meal[indexPath.row].mealChanged ? mealData[sortedDateArray![selectedIndex]]![indexPath.section].meal[indexPath.row].secondFoodName : mealData[sortedDateArray![selectedIndex]]![indexPath.section].meal[indexPath.row].firstFoodName
            (cell.viewWithTag(2) as! UILabel).text = mealData[sortedDateArray![selectedIndex]]![indexPath.section].meal[indexPath.row].mealChanged ? mealData[sortedDateArray![selectedIndex]]![indexPath.section].meal[indexPath.row].secondFoodQuantity : mealData[sortedDateArray![selectedIndex]]![indexPath.section].meal[indexPath.row].firstFoodQuantity
            
            if !mealData[sortedDateArray![selectedIndex]]![indexPath.section].meal[indexPath.row].option {
                (cell.viewWithTag(3) as! UIButton).isHidden = true
                for constraint in (cell.viewWithTag(3) as! UIButton).constraints {
                    if constraint.identifier == "height" {
                        constraint.constant = -5
                    }
                }
            }
            else {
                (cell.viewWithTag(3) as! UIButton).isHidden = false
                for constraint in (cell.viewWithTag(3) as! UIButton).constraints {
                    if constraint.identifier == "height" {
                        constraint.constant = 20
                    }
                }
            }
        }
        cell.selectionStyle = .none
        return cell
    }
   
}

//MARK: UICollectionview datasource and delegate
extension WeeklyDietViewController : UICollectionViewDataSource,UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dateCell", for: indexPath)
        let dayName : UILabel = cell.viewWithTag(1) as! UILabel
        let dayNumber : UILabel = cell.viewWithTag(2) as! UILabel
        let cellDate : Date = Calendar.current.date(byAdding: .day, value: indexPath.row, to: Date())!
        let dayNameFormatter = DateFormatter()
        let dayNumberFormatter = DateFormatter()
        if selectedIndex == indexPath.item {
            dayNumber.backgroundColor = UIColor.white
            dayNumber.textColor = UIColor(red: 56/255.0, green: 164/255.0, blue: 177/255.0, alpha: 1.0)
        }
        else {
            dayNumber.backgroundColor = UIColor.clear
            dayNumber.textColor = UIColor.white
        }
        dayNumber.layer.masksToBounds = true
        dayNameFormatter.dateFormat = "EEE"
        dayNumberFormatter.dateFormat = "dd"
        dayName.text = dayNameFormatter.string(from: cellDate)
        dayNumber.text = dayNumberFormatter.string(from: cellDate)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        collectionView.reloadData()
        dietTableView.reloadData()
        supplementCollapsed = true
        if (mealData[sortedDateArray![selectedIndex]]?.count)! == 0 {
            statusView.isHidden = false
            statusImage.isHidden = false
            statusLabel.text = "No Diet Found"
        }
        else {
            statusView.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section != mealData[sortedDateArray![selectedIndex]]?.count {
            return true
        }
        else {
            return true
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var actionArray: [UITableViewRowAction] = [UITableViewRowAction]()
        var mealName : String = ""
        switch indexPath.section {
        case 0:
            mealName = AppConstants.ServiceKeys.firstMeal
        case 1:
            mealName = AppConstants.ServiceKeys.secondMeal
        case 2:
            mealName = AppConstants.ServiceKeys.thirdMeal
        case 3:
            mealName = AppConstants.ServiceKeys.fourthMeal
        case 4:
            mealName = AppConstants.ServiceKeys.fifthMeal
        case 5:
            mealName = AppConstants.ServiceKeys.sixthMeal
        case 6:
            mealName = AppConstants.ServiceKeys.seventhMeal
        case 7:
            mealName = AppConstants.ServiceKeys.eighthMeal
        default:
            break
        }
        let missedAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Missed", handler:{action, indexpath in
            if !(self.mealData[self.sortedDateArray![self.selectedIndex]]?[indexPath.section].missed)! {
                self.commitMealActions(meal: mealName, actionType: 0)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Request already recorded", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        });
        let takenAction = UITableViewRowAction(style: UITableViewRowActionStyle.normal, title: "Taken", handler:{action, indexpath in
            self.commitMealActions(meal: mealName, actionType: 1)
        });
        actionArray.append(missedAction)
        actionArray.append(takenAction)
        return actionArray
    }
}

extension WeeklyDietViewController: SeactionHeaderClicked {
    func commitAction(section: Int) {
        var mealName : String = ""
        switch section {
        case 0:
            mealName = AppConstants.ServiceKeys.firstMeal
        case 1:
            mealName = AppConstants.ServiceKeys.secondMeal
        case 2:
            mealName = AppConstants.ServiceKeys.thirdMeal
        case 3:
            mealName = AppConstants.ServiceKeys.fourthMeal
        case 4:
            mealName = AppConstants.ServiceKeys.fifthMeal
        case 5:
            mealName = AppConstants.ServiceKeys.sixthMeal
        case 6:
            mealName = AppConstants.ServiceKeys.seventhMeal
        case 7:
            mealName = AppConstants.ServiceKeys.eighthMeal
        default:
            break
        }
        
        let alertView = UIAlertController(title:AppConstants.AppName , message: "What action you want to perform", preferredStyle: UIAlertControllerStyle.actionSheet)
        alertView.addAction(UIAlertAction(title: "Missed", style: UIAlertActionStyle.default, handler: { action in
            if !self.mealData[self.sortedDateArray![self.selectedIndex]]![section].missed {
                self.commitMealActions(meal: mealName, actionType: 0)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Request already recorded", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }))
        alertView.addAction(UIAlertAction(title: "Taken", style: UIAlertActionStyle.default, handler: { action in
            self.commitMealActions(meal: mealName, actionType: 1)
        }))
        alertView.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler: { action in
        }))
        self.present(alertView, animated: true, completion: nil)
    }
    
    func reloadTable(section: Int,collapsed : Bool) {
        mealData[sortedDateArray![selectedIndex]]![section].collapsed = collapsed
        dietTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .none)
    }
}
