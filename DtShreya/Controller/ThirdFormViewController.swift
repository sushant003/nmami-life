//
//  ThirdFormViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 19/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit
import DropDown

class ThirdFormViewController: BaseViewController {
    
    @IBOutlet weak var foodSpecificationButton: UIButton!
    @IBOutlet weak var allergyTF: UITextField!
    @IBOutlet weak var likesTF: UITextField!
    @IBOutlet weak var dislikeTF: UITextField!
    var delegate : throwData!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        (self.view as! CustomView).setVerticalGradient()
    }
    
    //MARK: IBActions
    @IBAction func onClickSkipButton(_ sender: Any) {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
    }
        
    @IBAction func onClickFoodDropDown(_ sender: UIButton) {
        AppHelper.getInstance().prepareDropdown(anchor: sender, dropdownList: [AppConstants.Other.veg,AppConstants.Other.nonveg], controller: self)
    }
    
    @IBAction func onClickNextButton(_ sender: Any) {
        if AppHelper.getInstance().isNetworkAvailable() {
            let params = [AppConstants.ServiceKeys.eating:foodSpecificationButton.title(for: .normal)!,AppConstants.ServiceKeys.foodallergies:allergyTF.text!,AppConstants.ServiceKeys.foodlove:likesTF.text!,AppConstants.ServiceKeys.foodhate:dislikeTF.text!,AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)] as [String : Any]
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.catchNotification),name: NSNotification.Name(rawValue: AppConstants.SignupForm.formThreeServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: params as NSDictionary, url: AppConstants.SignupForm.formThreeServiceSuffix, notificationKey: AppConstants.SignupForm.formThreeServiceNotify, controllerName: self,requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func catchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                delegate.sendPopedData(data: 1)
//                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
//                self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: message!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}

extension ThirdFormViewController : CustomDropDownDelegate {
    func getSelectedItem(item: String) {
        foodSpecificationButton.setTitle(item, for: .normal)
    }
}
