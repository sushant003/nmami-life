//
//  WalkThroughViewController.swift
//  GDI
//
//  Created by Sushant Jugran on 23/08/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit

class WalkThroughViewController: BaseViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var pageCV: UICollectionView!
    @IBOutlet weak var buttonContainer: UIView!
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var skipButton: UIButton!
    let walkThroughImageArray : [[String: UIImage]] = [[AppConstants.WalkThrough.first:#imageLiteral(resourceName: "walkthrough_1")],[AppConstants.WalkThrough.second:#imageLiteral(resourceName: "walkthrough_2")],[AppConstants.WalkThrough.third:#imageLiteral(resourceName: "walkthrough_3")]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewFlowLayout.itemSize = CGSize(width: self.view.frame.width, height: self.view.frame.height - UIApplication.shared.statusBarFrame.height)
        pageControl.transform = CGAffineTransform(scaleX: 2, y: 2)
    }
    //
    
    @IBAction func onClickLogin(_ sender: Any) {
        AppHelper.getInstance().pushView(identifier: "LoginViewController", controllerName: self)
    }
    
    @IBAction func onClickSignUp(_ sender: Any) {
        AppHelper.getInstance().pushView(identifier: "SignUpViewController", controllerName: self)
    }
    
    @IBAction func onClickSkipButton(_ sender: Any) {
        self.pageCV.scrollToItem(at:IndexPath(item: 2, section: 0), at: .right, animated: false)
        buttonContainer.isHidden = false
        skipButton.isHidden = true
    }
    
}

extension WalkThroughViewController : UICollectionViewDataSource,UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return walkThroughImageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        (cell.viewWithTag(1) as! UIImageView).image = walkThroughImageArray[indexPath.row].first?.value
        (cell.viewWithTag(2) as! UILabel).text = walkThroughImageArray[indexPath.row].first?.key
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = Double(Float(scrollView.contentOffset.x) / Float(scrollView.frame.width))
        self.pageControl.currentPage = Int(round(contentOffset))
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage  = Int(scrollView.contentOffset.x / self.view.frame.width)
        if pageControl.currentPage == 2 {
            buttonContainer.isHidden = false
            skipButton.isHidden = true
        }
        else {
            buttonContainer.isHidden = true
            skipButton.isHidden = false
        }
    }
}
