//
//  ProfileViewController.swift
//  DtShreya
//
//  Created by kwikard on 10/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {

    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var duratiobLabel: UILabel!
    @IBOutlet weak var trialStartedLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var planStartLabel: UILabel!
    @IBOutlet weak var planEndLabel: UILabel!
    
    @IBOutlet weak var referralCodeButton: CustomButton!
    @IBOutlet weak var profileImageView: CustomImage!
    @IBOutlet weak var planLabel: UILabel!
    @IBOutlet weak var containerView: CustomView!
    
    @IBOutlet weak var serverStatusView: UIView!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var serverStatusLabel: UILabel!
    
    var profileData : ProfileModel? = nil
    
    var selectedGender = "male"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getBasicInfo()
        statusImage.isHidden = true
        getBasicInfo()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        containerView.createShadow()
    }
    
    //MARK: Webservice Method
    func getBasicInfo() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.serverStatusView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.profileCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.getProfleDataServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: AppConstants.Dashboard.getProfileDataServiceSuffix, notificationKey: AppConstants.Dashboard.getProfleDataServiceNotify, controllerName: self,requestType: "")
        }
        else {
            serverStatusView.isHidden = false
            serverStatusLabel.text = AppConstants.ErrorMessags.Net_Error
            statusImage.isHidden = false
        }
    }
    
    @objc func profileCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.serverStatusView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                serverStatusView.isHidden = true
                let result = serviceData.userInfo?[AppConstants.ServiceKeys.result] as! [String:Any]
                nameLabel.text = result[AppConstants.ServiceKeys.name] as? String
                AppHelper.getInstance().loadImageIntoImageView(imageUrl: (AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.image) as? String)!, imageView: profileImageView, placeHolder: "user_placeholder")
                genderLabel.text = result[AppConstants.ServiceKeys.gender] as? String
                dobLabel.text = result[AppConstants.ServiceKeys.dob] as? String
                profileData = ProfileModel(name: nameLabel.text!, gender: genderLabel.text!, age: dobLabel.text!)
                planLabel.text = result[AppConstants.ServiceKeys.currentPlan] as? String
                duratiobLabel.text = result[AppConstants.ServiceKeys.duration] as? String
                referralCodeButton.setTitle(result[AppConstants.ServiceKeys.reffercode] as? String, for: .normal)
                paymentLabel.text = (result[AppConstants.ServiceKeys.payment] as? String == nil) ? "N.A" : (result[AppConstants.ServiceKeys.payment] as! String)
                planStartLabel.text = result[AppConstants.ServiceKeys.planStartDate] as? String
                planEndLabel.text = result[AppConstants.ServiceKeys.planEndDate] as? String
                trialStartedLabel.text = result[AppConstants.ServiceKeys.trialstart] as? String
            }
            else {
                serverStatusView.isHidden = false
                serverStatusLabel.text = message
                statusImage.isHidden = false
            }
        }
        else {
            serverStatusView.isHidden = false
            serverStatusLabel.text = AppConstants.ErrorMessags.Server_Error
            statusImage.isHidden = false
        }
    }
    
    
    
    //MARK: IBAction
    @IBAction func onClickCopyCode(_ sender: Any) {
        makeActionSheet()
    }
    
    @IBAction func onClickEditButton(_ sender: Any) {
        AppHelper.getInstance().pushControllerWithData(id: "EditProfileViewController", controllerName: self, data: ["data":profileData!,"controller":self])
    }
    
    func makeActionSheet() {
        let alertController = UIAlertController(title: AppConstants.AppName, message: "Copy your referral code.", preferredStyle: .actionSheet)
        let copyAction = UIAlertAction(title: "Copy", style: .destructive, handler: { (action) -> Void in
            UIPasteboard.general.string = self.referralCodeButton.title(for: .normal)
        })
        alertController.addAction(copyAction)
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
        })
        alertController.addAction(cancelButton)
        self.navigationController!.present(alertController, animated: true, completion: nil)
    }
    
}

extension ProfileViewController : throwData {
    func sendPopedData(data: Any) {
        serverStatusView.isHidden = false
        serverStatusLabel.text = ""
        statusImage.isHidden = true
        getBasicInfo()
    }
}
