//
//  ReminderViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 04/10/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class ReminderViewController: BaseViewController {

    @IBOutlet weak var dietSwitch: UISwitch!
    @IBOutlet weak var statsSwitch: UISwitch!
    @IBOutlet weak var promealSwitch: UISwitch!
    @IBOutlet weak var offerSwitch: UISwitch!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var headerView: CustomView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dietSwitch.transform = CGAffineTransform(scaleX: 0.8, y: 0.75)
        statsSwitch.transform = CGAffineTransform(scaleX: 0.8, y: 0.75)
        promealSwitch.transform = CGAffineTransform(scaleX: 0.8, y: 0.75)
        offerSwitch.transform = CGAffineTransform(scaleX: 0.8, y: 0.75)
        statusImage.isHidden = true
        getReminder()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
    }
    
    //MARK: IBAction
    
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickSaveButton(_ sender: Any) {
        saveReminder()
    }
    
    //MARK: Webservice methods
    func getReminder() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.statusView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.getReminderCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.getReminderNotify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: AppConstants.Dashboard.getReminderSuffix, notificationKey: AppConstants.Dashboard.getReminderNotify, controllerName: self,requestType: "")
        }
        else {
            statusView.isHidden = false
            statusImage.isHidden = false
            statusLabel.text = AppConstants.ErrorMessags.Net_Error
        }
    }
    
    @objc func getReminderCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.statusView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                let result = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as? [String:Bool])!
                statusView.isHidden = true
                statsSwitch.setOn(result[AppConstants.ServiceKeys.stats]!, animated: true)
                promealSwitch.setOn(result[AppConstants.ServiceKeys.preMeal]!, animated: true)
                offerSwitch.setOn(result[AppConstants.ServiceKeys.offer]!, animated: true)
                dietSwitch.setOn(result[AppConstants.ServiceKeys.markDiet]!, animated: true)
            }
            else {
                statusView.isHidden = false
                statusImage.isHidden = false
                statusLabel.text = message!
            }
        }
        else {
            statusView.isHidden = false
            statusImage.isHidden = false
            statusLabel.text = AppConstants.ErrorMessags.Server_Error
        }
    }
    
    func saveReminder() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.statusView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.saveReminderCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.getReminderNotify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.stats:statsSwitch.isOn,AppConstants.ServiceKeys.preMeal:promealSwitch.isOn,AppConstants.ServiceKeys.offer:offerSwitch.isOn,AppConstants.ServiceKeys.markDiet:dietSwitch.isOn], url: AppConstants.Dashboard.saveReminderSuffix, notificationKey: AppConstants.Dashboard.getReminderNotify, controllerName: self,requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            
        }
    }
    
    @objc func saveReminderCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.statusView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: message!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}
