//
//  SignUpViewController.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 08/04/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit
import FirebaseAuth
import GoogleSignIn
import FirebaseAuthUI
import FirebaseFacebookAuthUI
import FirebaseGoogleAuthUI

class SignUpViewController: ParentAuthUIViewController,socialLoginListener {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var headerView: CustomView!
    var userName : String = ""
    var isFBLogin = true
    override func viewDidLoad() {
        super.viewDidLoad()
        AppHelper.getInstance().setValueForUserDefault(value: "1", key: AppConstants.userDefaultConstants.isInstalledForFirstTime)
        usernameTextField.attributedPlaceholder = NSAttributedString(string: "Username",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        confirmPasswordTF.attributedPlaceholder = NSAttributedString(string: "Confirm Password",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        self.authUI.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
    }

    //MARK: IBAction
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func logInTapped(_ sender: Any) {
        AppHelper.getInstance().pushView(identifier: "LoginViewController", controllerName: self)
    }
    
    @IBAction func onClickFBLogin(_ sender: Any) {
        self.authUI.providers = [FUIFacebookAuth()]
        let provider = self.authUI.providers.first as! FUIFacebookAuth
        provider.signIn(withDefaultValue: "", presenting: self, completion: self.didSignIn)
        isFBLogin = true
    }
    
    @IBAction func onClickGoogleLogin(_ sender: Any) {
        self.authUI.providers = [FUIGoogleAuth()]
        let provider = self.authUI.providers.first as! FUIGoogleAuth
        provider.signIn(withDefaultValue: "", presenting: self, completion: self.didSignIn)
        isFBLogin = false
//        AppHelper.getInstance().googleLoginMethod(controller: self, listener: self)
//        GIDSignIn.sharedInstance().uiDelegate = self
//        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func signUpButtonTapped(_ sender: Any) {
        self.view.endEditing(true)
        AppHelper.getInstance().removeUserDefaultForKey(key: AppConstants.ServiceKeys.image)
        if usernameTextField.text?.count != 0 && passwordTextField.text?.count != 0 && emailTextField.text?.count != 0 {
            if usernameTextField.text == "" || emailTextField.text == "" || passwordTextField.text == "" || confirmPasswordTF.text == "" {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please enter all the details", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            if !AppHelper.getInstance().isValidEmail(testStr: emailTextField.text!) {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Email_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            else if (passwordTextField.text?.count)! < 6 {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Password_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            else if passwordTextField.text != confirmPasswordTF.text {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Password_Match_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            if !AppHelper.getInstance().isNetworkAvailable() {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            DispatchQueue.main.async {
                AppHelper.getInstance().showProgressIndicator(view: self.view)
            }
            self.loginToFirebase()
        }
    }
    //MARK: Webservice method
    
    func loginToServer(params : [String:String]) {
        
        DataService.ds.postRequest(urlExt: "signup/", params: params) { (data, error) in
            DispatchQueue.main.async {
                AppHelper.getInstance().hideProgressIndicator(view: self.view)
            }
            if let data = data {
                let message  = data[AppConstants.ServiceKeys.message] as? String
                if let status = data[AppConstants.ServiceKeys.status] as? Bool {
                    if status == true {
                        DispatchQueue.main.async {
                            self.saveSignUpData(dict: data)
                            AppHelper.getInstance().setValueForUserDefault(value: params[AppConstants.ServiceKeys.email]!, key: AppConstants.ServiceKeys.email)
                            AppHelper.getInstance().alertWithTextField(title: "Have any referral code?", placeholder: "Enter code", text: "", controllerName: self, leftButtonTite: "Apply", isNumericTextField: false)
                        }
                    }
                    else {
                        AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "\(message!)", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                    }
                }
                else {
                    DispatchQueue.main.async {
                        AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "\(message!)", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                }
            }
        }
    }
    
    func applyReferralCode(code : String) {
        AppHelper.getInstance().showProgressIndicator(view: self.view)
        DataService.ds.postRequest(urlExt: "setreffercode/", params: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.code:code]) { (data, error) in
            DispatchQueue.main.sync {
                AppHelper.getInstance().hideProgressIndicator(view: self.view)
            }
            if let data = data {
                
                if let status = data[AppConstants.ServiceKeys.status] as? Bool {
                    
                    if status != true {
                        DispatchQueue.main.sync {
                            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Code not applied. Want to try again?", cancelButtonName: "No", otherButtonName: "Yes", controllerName: self, isAlertOnly: false)
                        }
                    }
                    else {
                        DispatchQueue.main.sync {
                            AppHelper.getInstance().pushControllerWithData(id: "SetupFormViewController", controllerName: self, data: self.userName == "" ? self.usernameTextField.text! : self.userName)
                        }                        
                    }
                }
                else {
                    DispatchQueue.main.async {
                        AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Unable to process your request. Want to try again?", cancelButtonName: "No", otherButtonName: "Yes", controllerName: self, isAlertOnly: false)
                    }
                }
                
            }
            else {
                DispatchQueue.main.async {
                    AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                }
            }
        }
    }
    
    func saveSignUpData(dict : [String:Any]) {
        AppHelper.getInstance().setValueForUserDefault(value: "1", key: AppConstants.userDefaultConstants.isLoggedIn)
        AppHelper.getInstance().setValueForUserDefault(value: dict[AppConstants.ServiceKeys.username]!, key: AppConstants.userDefaultConstants.userName)
        AppHelper.getInstance().setValueForUserDefault(value: dict[AppConstants.ServiceKeys.userId]!, key:  AppConstants.ServiceKeys.userId)
        //dict[AppConstants.ServiceKeys.dieterStatus]!
        AppHelper.getInstance().setValueForUserDefault(value: dict[AppConstants.ServiceKeys.status]!, key: AppConstants.ServiceKeys.dieterStatus)
        //        AppHelper.getInstance().setValueForUserDefault(value: usernameTextField.text!, key: AppConstants.ServiceKeys.email)
    }
    
    //SocialLoginListener
    func loginSuccess(socialData: Any) {
        authoriseSocialSignIn(credential : socialData as! AuthCredential)
        
    }
    
    func loginCancelled() {
    }
    
    //MARK: Firebase methods
    
    func authoriseSocialSignIn(credential : AuthCredential) {
        DispatchQueue.main.async {
            AppHelper.getInstance().showProgressIndicator(view: self.view)
        }
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if error != nil {
                AppHelper.getInstance().hideProgressIndicator(view: self.view)
                return
            }
            let params = [AppConstants.ServiceKeys.username:authResult?.user.displayName,AppConstants.ServiceKeys.email:authResult?.user.email,AppConstants.ServiceKeys.uid:authResult?.user.uid]
            self.userName = (authResult?.user.displayName)!
            self.loginToServer(params: params as! [String : String])
        }
    }
    
    func loginToFirebase() {
        Auth.auth().signIn(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!) { (user, error) in
            if error == nil {
               let params = [AppConstants.ServiceKeys.username:self.usernameTextField.text!,AppConstants.ServiceKeys.email:self.emailTextField.text!,AppConstants.ServiceKeys.uid:user?.uid]
                self.loginToServer(params: params as! [String : String])
            }
            else {
                self.createFirebaseAccount()
            }
        }
    }
    
    func createFirebaseAccount() {
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
            if error == nil {
                let params = [AppConstants.ServiceKeys.username:self.usernameTextField.text!,AppConstants.ServiceKeys.email:self.emailTextField.text!,AppConstants.ServiceKeys.uid:user?.uid]
                self.loginToServer(params: params as! [String : String])
            }
            else {
                AppHelper.getInstance().hideProgressIndicator(view: self.view)
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: (error?.localizedDescription)!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
    }
}

//MARK:AuthUI Delegate
extension SignUpViewController: FUIAuthDelegate {
    
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        switch error {
        case .some(let error as NSError) where UInt(error.code) == FUIAuthErrorCode.userCancelledSignIn.rawValue:
            print("User cancelled sign-in")
        case .some(let error as NSError) where error.userInfo[NSUnderlyingErrorKey] != nil:
            print("Login error: \(error.userInfo[NSUnderlyingErrorKey]!)")
        case .some(let error):
            print("Login error: \(error.localizedDescription)")
        case .none:
            let params = [AppConstants.ServiceKeys.username:authDataResult?.additionalUserInfo?.profile!["name"] ?? "",AppConstants.ServiceKeys.email:authDataResult?.additionalUserInfo?.profile!["email"] ?? "",AppConstants.ServiceKeys.uid:authDataResult?.user.uid ?? ""] as [String : Any]
            if isFBLogin {
                AppHelper.getInstance().setValueForUserDefault(value: (authDataResult?.additionalUserInfo?.profile!["picture"] as! [String:Any])["url"] ?? "", key: AppConstants.ServiceKeys.image)
                self.loginToServer(params: params as! [String : String])
            }
            else {
                AppHelper.getInstance().setValueForUserDefault(value: authDataResult?.additionalUserInfo?.profile!["picture"] ?? "", key: AppConstants.ServiceKeys.image)
                self.loginToServer(params: params as! [String : String])
            }
            
        }
        
    }
}

//MARK:GoogleUI Delegate
extension SignUpViewController : GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension SignUpViewController : AlertTextDelegate {
    func getInputText(text: String) {
        applyReferralCode(code: text)
    }
    
    func dismissAlertView() {
        AppHelper.getInstance().pushControllerWithData(id: "SetupFormViewController", controllerName: self, data: self.userName == "" ? self.usernameTextField.text! : self.userName)
    }
}

extension SignUpViewController : alertPositiveButtonDelegate {
    func alertPositiveButtonPressed() {
        AppHelper.getInstance().alertWithTextField(title: "Have any referral code?", placeholder: "Enter code", text: "", controllerName: self, leftButtonTite: "Apply", isNumericTextField: false)
    }
    
    func alertNegativeButtonPressed() {
        AppHelper.getInstance().pushControllerWithData(id: "SetupFormViewController", controllerName: self, data: self.userName == "" ? self.usernameTextField.text! : self.userName)
    }
}
