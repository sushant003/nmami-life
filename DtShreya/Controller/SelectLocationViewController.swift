//
//  SelectLocationViewController.swift
//  Nmami Life
//
//  Created by Sushant Jugran on 04/11/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class SelectLocationViewController: BaseViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var tempCountryList : [[String:Any]] = [[String:Any]]()
    var tempCityList : [String] = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        headerLabel.text = (data as! [String:Any])["header"] as? String
        if (data as! [String:Any])["value"] as? [[String:Any]] != nil {
            tempCountryList = (data as! [String:Any])["value"] as! [[String:Any]]
        }
        else {
            tempCityList = (data as! [String:Any])["value"] as! [String]
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
    }
    
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension SelectLocationViewController : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (data as! [String:Any])["value"] as? [[String:Any]] != nil {
            return tempCountryList.count
        }
        return tempCityList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if (data as! [String:Any])["value"] as? [[String:Any]] != nil {
            cell.textLabel?.text = tempCountryList[indexPath.row][AppConstants.Other.countryName] as? String
        }
        else {
            cell.textLabel?.text = tempCityList[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (data as! [String:Any])["value"] as? [[String:Any]] != nil {
            ((data as! [String:Any])["controller"] as! throwData).sendPopedData(data: tempCountryList[indexPath.row])
        }
        else {
            ((data as! [String:Any])["controller"] as! throwData).sendPopedData(data: tempCityList[indexPath.row])
        }
        navigationController?.popViewController(animated: true)
    }
}


extension SelectLocationViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        
        if searchBar.text != "" {
            if (data as! [String:Any])["value"] as? [[String:Any]] != nil {
                tempCountryList = (data as! [String:Any])["value"] as! [[String:Any]]
                tempCountryList = tempCountryList.filter { (value) -> Bool in
                    if (value[AppConstants.Other.countryName] as! String).range(of: searchBar.text!) != nil  {
                        return true
                    }
                    else {
                        return false
                    }
                }
            }
            else {
                tempCityList = (data as! [String:Any])["value"] as! [String]
                tempCityList = tempCityList.filter { (value) -> Bool in
                    if value.range(of: searchBar.text!) != nil  {
                        return true
                    }
                    else {
                        return false
                    }
                }
            }
        }
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
}
