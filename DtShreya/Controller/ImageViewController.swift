//
//  ImageViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 24/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit
import Firebase

class ImageViewController: BaseViewController {
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (data as! String).hasPrefix("gs:") {
            let islandRef = Storage.storage().reference(forURL: (data as! String))
            // Download in memory with a maximum allowed size of 1MB (1 * 1024 * 1024 bytes)
            islandRef.getData(maxSize: 3 * 1024 * 1024) { data, error in
                if error != nil {
                    // Uh-oh, an error occurred!
                } else {
                    // Data for "images/island.jpg" is returned
                    let image = UIImage(data: data!)
                    self.imageView.image = image
                }
            }
        }
        else {
            AppHelper.getInstance().loadImageIntoImageView(imageUrl: (data as! String), imageView: imageView, placeHolder: "placeholder_attachement")
        }        
    }

    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    

}
