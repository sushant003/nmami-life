//
//  SetupFormViewController.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 26/03/18.
//  Copyright © 2018 Medicians. All rights reserved.
//

import UIKit
import DropDown

class SetupFormViewController: BaseViewController {
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var dobTF: UITextField!
    @IBOutlet weak var contactTF: UITextField!
    @IBOutlet weak var weightTF: UITextField!
    @IBOutlet weak var feetTF: UITextField!
    @IBOutlet weak var inchesTF: UITextField!
    @IBOutlet weak var countryTF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var countryCodeTF: UITextField!
    @IBOutlet weak var otherReasonTF: UITextField!
    
    
    @IBOutlet weak var otherTF: UITextField!    
    @IBOutlet weak var gradientView: TriangleView!
    @IBOutlet weak var containerView: CustomView!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var weightButton: UIButton!
    
    var datePickerController : DatePickerViewController! = nil
    var selectedGender = "male"
    var weight : Double = 0.0
    var height : Double = 0.0
    var dropDownCaller = 0  // 1 for how you know about us
    var countryData : [[String:Any]] = [[String:Any]]()
    var cityData : [String] = [String]()
    var selectedCountryData : [String:Any] = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DatabaseManager().openDatabase()
        nameTF.text = data as? String
        nameTF.attributedPlaceholder = NSAttributedString(string: "Name",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        contactTF.attributedPlaceholder = NSAttributedString(string: "Phone Number",
                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        dobTF.attributedPlaceholder = NSAttributedString(string: "Age",
                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        
        weightTF.attributedPlaceholder = NSAttributedString(string: "xx.xx",
                                                                attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        feetTF.attributedPlaceholder = NSAttributedString(string: "feet",
                                                                attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        inchesTF.attributedPlaceholder = NSAttributedString(string: "inches",
                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        otherTF.attributedPlaceholder = NSAttributedString(string: "How did you know about us?",
                                                            attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        datePickerController = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as? DatePickerViewController
        maleButton.setImage(#imageLiteral(resourceName: "form_male").withRenderingMode(.alwaysTemplate), for: .normal)
        maleButton.tintColor = AppConstants.Colors.greenColor
        if let rs = DatabaseManager.database.executeQuery("SELECT * FROM countries", withArgumentsIn:[]) {
            while rs.next() {
                self.countryData.append([AppConstants.Other.countryName:rs.resultDictionary!["name"]!,AppConstants.Other.countryId: rs.resultDictionary!["id"]!,AppConstants.Other.countryCode :rs.resultDictionary!["phonecode"]!])
            }
        } else {
            print("select failure: \(DatabaseManager.database.lastErrorMessage())")
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        containerView.createShadow()
        (self.view as! CustomView).setVerticalGradient()
    }
    
    //MARK: IBActions
    
    @IBAction func onClickMale(_ sender: UIButton) {
        selectedGender = "male"
        femaleButton.setTitleColor(UIColor.lightGray, for: .normal)
        maleButton.setTitleColor(AppConstants.Colors.greenColor, for: .normal)
        maleButton.setImage(#imageLiteral(resourceName: "form_male").withRenderingMode(.alwaysTemplate), for: .normal)
        maleButton.imageView?.tintColor = AppConstants.Colors.greenColor
        femaleButton.titleLabel?.textColor = UIColor.lightGray        
        femaleButton.imageView?.tintColor = UIColor.darkGray
    }
    
    @IBAction func onClickFemale(_ sender: UIButton) {
        selectedGender = "female"
        maleButton.setTitleColor(UIColor.lightGray, for: .normal)
        femaleButton.setTitleColor(AppConstants.Colors.greenColor, for: .normal)
        femaleButton.setImage(#imageLiteral(resourceName: "form_female").withRenderingMode(.alwaysTemplate), for: .normal)
        femaleButton.imageView?.tintColor = AppConstants.Colors.greenColor
        maleButton.titleLabel?.textColor = UIColor.lightGray
        maleButton.imageView?.tintColor = UIColor.darkGray
    }
    
    @IBAction func onClickWeightDropdown(_ sender: UIButton) {
        dropDownCaller = 0
        AppHelper.getInstance().prepareDropdown(anchor: sender, dropdownList: [AppConstants.Other.kgs,AppConstants.Other.lbs], controller: self)
    }
    
    @IBAction func onClickHeightDropdown(_ sender: UIButton) {
        dropDownCaller = 0
        AppHelper.getInstance().prepareDropdown(anchor: sender, dropdownList: [AppConstants.Other.feet,AppConstants.Other.cms], controller: self)
    }
    
    @IBAction func onClickDOB(_ sender: Any) {
        self.view.endEditing(true)
        self.datePickerController.data = ["pickerType":"date"]
        self.datePickerController.datePickerDelegate = self
        self.addChildViewController(self.datePickerController)
        self.view.addSubview(self.datePickerController.view)
        self.datePickerController.didMove(toParentViewController: self)
    }
    
    @IBAction func onClickCountry(_ sender: Any) {
        AppHelper.getInstance().pushControllerWithData(id: "SelectLocationViewController", controllerName: self, data: ["controller":self,"value":countryData,"header":"Select Country"])
    }
    
    @IBAction func onClickCity(_ sender: Any) {
        cityData.removeAll()
        if let rs = DatabaseManager.database.executeQuery("SELECT * FROM 'states'where country_id = \(selectedCountryData[AppConstants.Other.countryId] as! Int)", withArgumentsIn:[]) {
            while rs.next() {
                cityData.append(rs.resultDictionary!["name"]! as! String)
            }
        } else {
            print("select failure: \(DatabaseManager.database.lastErrorMessage())")
        }
        AppHelper.getInstance().pushControllerWithData(id: "SelectLocationViewController", controllerName: self, data: ["controller":self,"value":cityData,"header":"Select City"])
    }
    
    @IBAction func onClickHowYouKnow(_ sender: UIButton) {
        dropDownCaller = 1
        AppHelper.getInstance().prepareDropdown(anchor: sender, dropdownList: [AppConstants.Other.google,AppConstants.Other.facebook,AppConstants.Other.twitter,AppConstants.Other.linkedin,AppConstants.Other.newsletter,AppConstants.Other.other], controller: self)
    }
    
    @IBAction func onClickNext(_ sender: Any) {
        if AppHelper.getInstance().isNetworkAvailable() {
//            if (dobTF.text == "" || contactTF.text == "" || weightTF.text == "" || feetTF.text == "") || inchesTF.text == ""{
//                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please fill all the relevant data.", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
//                return
//            }
            if nameTF.text == "" {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please insert your name.", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            if contactTF.text == "" {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please insert your contact number.", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            
            if (contactTF.text?.count)! > 16 || (contactTF.text?.count)! < 6 {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Phone_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            if dobTF.text == "" {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please select your date of birth", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            if weightTF.text == "" {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please insert your weight", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            if (feetTF.text!) == "" {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please insert your height", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            if let h = Int(feetTF.text!) {
                if h > 8 {
                    AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Height cannot be greater than 8 inches", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                    return
                }
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please insert valid height", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            if let i = Int(inchesTF.text!) {
                if i > 11 {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Height cannot be greater than 11 inches", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
                }
            }
        
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please insert valid height", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            if countryTF.text?.count == 0 {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please select your country", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            if cityTF.text?.count == 0 {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please select your state", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            let absoluteDifference = String(format: "%.2f", calculateHeightInCMS())
            let val = Float(absoluteDifference)!
            let params = [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.phone:(countryCodeTF.text! + contactTF.text!),AppConstants.ServiceKeys.gender:selectedGender,AppConstants.ServiceKeys.dob:dobTF.text!,AppConstants.ServiceKeys.weight:weight > 0.0 ? weight : getheightAndWeight(item: AppConstants.Other.kgs),AppConstants.ServiceKeys.height:val,AppConstants.ServiceKeys.about:(otherTF.text! == AppConstants.Other.other ? otherReasonTF.text! : otherTF.text!),AppConstants.ServiceKeys.code:"balk_123",AppConstants.ServiceKeys.name:nameTF.text!,AppConstants.ServiceKeys.country:selectedCountryData[AppConstants.Other.countryName]!,AppConstants.ServiceKeys.state:cityTF.text!] as [String : Any]
//            if height > 350 {
//                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.validHeightError, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
//                return
//            }
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.catchNotification),name: NSNotification.Name(rawValue: AppConstants.SignupForm.formOneServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: params as NSDictionary, url: AppConstants.SignupForm.formOneServiceSuffix, notificationKey: AppConstants.SignupForm.formOneServiceNotify, controllerName: self,requestType: "")
        }
        else {
           AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func catchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                AppHelper.getInstance().pushView(identifier: "BMIViewController", controllerName: self)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: message!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    func getheightAndWeight(item : String) -> Double {
        guard let inputWeight = Double(weightTF.text!) else {
            return 0.0
        }
        if item == AppConstants.Other.kgs {
            weight = inputWeight
            weightButton.setTitle(AppConstants.Other.kgs, for: .normal)
        }
        else {
            weight = inputWeight * 0.45
            weightButton.setTitle(AppConstants.Other.lbs, for: .normal)
        }
        return weight
    }
    
    func calculateHeightInCMS() -> Double {
        if let inch = Double(inchesTF.text!) {
            height = Double(inch) * 2.54
        }
        if let feet = Double(feetTF.text!) {
            height += Double(feet) * 30.48
        }
        return height
        
    }
}

extension SetupFormViewController : CustomDropDownDelegate {
    func getSelectedItem(item: String) {
        if dropDownCaller == 0 {
            getheightAndWeight(item: item)
        }
        else {
            if item == AppConstants.Other.other {
                otherReasonTF.isHidden = false
            }
            else {
                otherReasonTF.isHidden = true
            }
            otherTF.text = item
            
        }
    }
}

//MARK: Datepicker Delegate
extension SetupFormViewController : DatePickerListener {
    func setDateFromPicker(date: Any) {
        datePickerController.view.removeFromSuperview()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dobTF.text = dateFormatter.string(from: date as! Date)
    }
    
    func dismissDatePicker() {
        datePickerController.view.removeFromSuperview()
    }
}

extension SetupFormViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == feetTF || textField == inchesTF {
            if (textField.text?.count)! > 2 && string != ""{
                return false
            }
            return string.rangeOfCharacter(from: CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")) == nil
            
        }
        else {
            if (textField.text?.count)! > 4 && string != ""{
                return false
            }
            return string.rangeOfCharacter(from: CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")) == nil
            
        }
    }
}

extension SetupFormViewController : throwData {
    func sendPopedData(data: Any) {
        if (data as? [String:Any]) != nil {
            selectedCountryData = data as! [String:Any]
            countryCodeTF.text = "+\(selectedCountryData[AppConstants.Other.countryCode]!)"
            countryTF.text = "\(selectedCountryData[AppConstants.Other.countryName]!)"
        }
        else {
            cityTF.text = data as? String
        }
    }
}
