//
//  CompleteProfileViewController.swift
//  Nmami Life
//
//  Created by Sushant Jugran on 31/10/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class CompleteProfileViewController: BaseViewController {
    
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var firstContainerView: UIView!
    @IBOutlet weak var secondContainerView: UIView!
    var firstFormController : SecondFormViewController! = nil
    var secondFormController : ThirdFormViewController! = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        firstFormController = self.storyboard?.instantiateViewController(withIdentifier: "SecondFormViewController") as? SecondFormViewController
        firstFormController.delegate = self
        secondFormController = self.storyboard?.instantiateViewController(withIdentifier: "ThirdFormViewController") as? ThirdFormViewController
        secondFormController.delegate = self
        firstContainerView.frame.size = CGSize(width: self.view.frame.width, height: self.view.frame.height - 130)
        secondContainerView.frame.size = CGSize(width: self.view.frame.width, height: self.view.frame.height - 130)
        
        self.addChildViewController(self.firstFormController)
        self.view.addSubview(self.firstFormController.view)
        self.firstFormController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 130)
        self.firstFormController.view.center = self.firstContainerView.center
        self.firstFormController.didMove(toParentViewController: self)
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidLayoutSubviews() {
        headerView.setGradientBackground()
    }
    
    @IBAction func onClickbackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onTapSegment(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            secondFormController.view.removeFromSuperview()
            self.addChildViewController(self.firstFormController)
            self.view.addSubview(self.firstFormController.view)
            self.firstFormController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 130)
            self.firstFormController.view.center = self.firstContainerView.center
            self.firstFormController.didMove(toParentViewController: self)
        case 1:
            firstFormController.view.removeFromSuperview()
            self.addChildViewController(self.secondFormController)
            self.view.addSubview(self.secondFormController.view)
            self.secondFormController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 130)
            self.secondFormController.view.center = self.secondContainerView.center
            self.secondFormController.didMove(toParentViewController: self)
        default:
            break
        }
    }
}

extension CompleteProfileViewController : throwData {
    func sendPopedData(data: Any) {
        if data as! Int == 0 {
            firstFormController.view.removeFromSuperview()
            self.addChildViewController(self.secondFormController)
            self.view.addSubview(self.secondFormController.view)
            self.secondFormController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 130)
            self.secondFormController.view.center = self.secondContainerView.center
            self.secondFormController.didMove(toParentViewController: self)
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
