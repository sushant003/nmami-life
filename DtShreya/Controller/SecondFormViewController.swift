//
//  SecondFormViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 19/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit
import DropDown

class SecondFormViewController: BaseViewController {
    
    @IBOutlet weak var medicalHistoryTextView: UITextView!
    @IBOutlet weak var medicalHistoryYesButton: UIButton!
    @IBOutlet weak var medicalHistoryNoButton: UIButton!
    @IBOutlet weak var hardDrinkYesButton: UIButton!
    @IBOutlet weak var hardDrinkNoButon: UIButton!
    @IBOutlet weak var manageDiseaseYesButton: UIButton!
    @IBOutlet weak var manageDiseaseNoButton: UIButton!
    @IBOutlet weak var physicalHistoryButton: UIButton!
    @IBOutlet weak var eatOutButton: UIButton!
    @IBOutlet weak var addDiseaseViewContainer: UIView!
    @IBOutlet weak var diseaseCV: UICollectionView!
    @IBOutlet weak var diseaseCVFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var diseaseCVHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var sliderView: UISlider!
    @IBOutlet weak var scrollContainer: CustomView!
    @IBOutlet weak var reportCV: UICollectionView!
    @IBOutlet weak var reportCVFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var reportHeightConstraint: NSLayoutConstraint!
    
    var diseaseArray : [String] = [String]()
    var reportArray : [String] = []
    let imagePicker = UIImagePickerController()
    var delegate : throwData!
    override func viewDidLoad() {
        super.viewDidLoad()        
        medicalHistoryYesButton.isSelected = true
        medicalHistoryNoButton.isSelected = false
        hardDrinkYesButton.isSelected = true
        hardDrinkNoButon.isSelected = false
        manageDiseaseYesButton.isSelected = true
        manageDiseaseNoButton.isSelected = false
        medicalHistoryTextView.text = "Please type here..."
        medicalHistoryTextView.textColor = UIColor.darkGray
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        scrollContainer.createShadow()
//        (self.view as! CustomView).setVerticalGradient()
    }
    
    func makeActionSheet() {
        let alertController = UIAlertController(title: AppConstants.AppName, message: "Choose image from", preferredStyle: .actionSheet)
        let gallary = UIAlertAction(title: "Gallery", style: .default, handler: { (action) -> Void in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
            
        })
        let camera = UIAlertAction(title: "Camera", style: .default, handler: { (action) -> Void in
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        alertController.addAction(gallary)
        alertController.addAction(camera)
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
        })
        alertController.addAction(cancelButton)
        self.navigationController!.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: IBAction
    
    @IBAction func onClickSkipButton(_ sender: Any) {
        AppHelper.getInstance().pushView(identifier: "ThirdFormViewController", controllerName: self)
    }
    
    @IBAction func onClickSliderView(_ sender: UISlider) {
        sliderView.value = round(sender.value / 1) * 1
        print(sliderView.value)
    }
    
    @IBAction func onClickAddDisease(_ sender: Any) {
        AppHelper.getInstance().pushControllerWithData(id: "AddDiseaseViewController", controllerName: self, data: ["value":diseaseArray,"controller":self])
    }
    
    @IBAction func onClickDeleteReport(_ sender: UIButton) {
        let indexPath = reportCV.indexPath(for: sender.superview?.superview as! UICollectionViewCell)
        
        reportArray.remove(at: (reportArray.count == 5 ? indexPath?.item : ((indexPath?.item)! - 1))!)
        reportCV.reloadData()
    }
    
    @IBAction func onClickYesMedicalHistory(_ sender: Any) {
        medicalHistoryYesButton.isSelected = true
        medicalHistoryNoButton.isSelected = false
        medicalHistoryYesButton.setTitleColor(AppConstants.Colors.greenColor, for: .normal)
        medicalHistoryYesButton.setImage(#imageLiteral(resourceName: "form_radio_active"), for: .normal)
        medicalHistoryNoButton.setImage(#imageLiteral(resourceName: "form_radio_inactive"), for: .normal)
        medicalHistoryNoButton.setTitleColor(UIColor.lightGray, for: .normal)
        medicalHistoryTextView.isHidden = false
    }
    
    @IBAction func onClickNoMedicalHistory(_ sender: Any) {
        medicalHistoryYesButton.isSelected = false
        medicalHistoryNoButton.isSelected = true
        medicalHistoryNoButton.setTitleColor(AppConstants.Colors.greenColor, for: .normal)
        medicalHistoryNoButton.setImage(#imageLiteral(resourceName: "form_radio_active"), for: .normal)
        medicalHistoryYesButton.setImage(#imageLiteral(resourceName: "form_radio_inactive"), for: .normal)
        medicalHistoryYesButton.setTitleColor(UIColor.lightGray, for: .normal)
        medicalHistoryTextView.isHidden = true
    }
    
    @IBAction func onClickPhysicalActivityDropDown(_ sender: UIButton) {
        AppHelper.getInstance().prepareDropdown(anchor: sender, dropdownList: [AppConstants.Other.active,AppConstants.Other.semiActive,AppConstants.Other.none], controller: self)
    }
    
    @IBAction func onClickEatOutDropDown(_ sender: UIButton) {
        var arr : [Any] = Array(1...10)
        arr = arr.map({ (item)  in
            "\(item) Time"
        })        
        AppHelper.getInstance().prepareDropdown(anchor: sender, dropdownList: arr as! [String], controller: self)
    }
    
    @IBAction func onClickYesHardDrinks(_ sender: Any) {
        hardDrinkYesButton.isSelected = true
        hardDrinkNoButon.isSelected = false
        hardDrinkYesButton.setTitleColor(AppConstants.Colors.greenColor, for: .normal)
        hardDrinkYesButton.setImage(#imageLiteral(resourceName: "form_radio_active"), for: .normal)
        hardDrinkNoButon.setImage(#imageLiteral(resourceName: "form_radio_inactive"), for: .normal)
        hardDrinkNoButon.setTitleColor(UIColor.lightGray, for: .normal)
    }
    
    @IBAction func onClickNoHardDrinks(_ sender: Any) {
        hardDrinkYesButton.isSelected = false
        hardDrinkNoButon.isSelected = true
        hardDrinkNoButon.setTitleColor(AppConstants.Colors.greenColor, for: .normal)
        hardDrinkNoButon.setImage(#imageLiteral(resourceName: "form_radio_active"), for: .normal)
        hardDrinkYesButton.setImage(#imageLiteral(resourceName: "form_radio_inactive"), for: .normal)
        hardDrinkYesButton.setTitleColor(UIColor.lightGray, for: .normal)
    }
    
    @IBAction func onClickYesManageDisease(_ sender: Any) {
        manageDiseaseYesButton.isSelected = true
        manageDiseaseNoButton.isSelected = false
        manageDiseaseYesButton.setTitleColor(AppConstants.Colors.greenColor, for: .normal)
        manageDiseaseYesButton.setImage(#imageLiteral(resourceName: "form_radio_active"), for: .normal)
        manageDiseaseNoButton.setImage(#imageLiteral(resourceName: "form_radio_inactive"), for: .normal)
        manageDiseaseNoButton.setTitleColor(UIColor.lightGray, for: .normal)
        addDiseaseViewContainer.isHidden = false
    }
    
    @IBAction func onClickNoManageDisease(_ sender: Any) {
        manageDiseaseNoButton.isSelected = true
        manageDiseaseYesButton.isSelected = false
        manageDiseaseNoButton.setTitleColor(AppConstants.Colors.greenColor, for: .normal)
        manageDiseaseNoButton.setImage(#imageLiteral(resourceName: "form_radio_active"), for: .normal)
        manageDiseaseYesButton.setImage(#imageLiteral(resourceName: "form_radio_inactive"), for: .normal)
        manageDiseaseYesButton.setTitleColor(UIColor.lightGray, for: .normal)
        addDiseaseViewContainer.isHidden = true
        diseaseArray.removeAll()
        resizeHeightOfCV()
    }
    
    //MARK: Webservice methods
    @IBAction func onClickNextButton(_ sender: Any) {
        if AppHelper.getInstance().isNetworkAvailable() {
            let params = [AppConstants.ServiceKeys.medical:(medicalHistoryTextView.text == "Please type here..." ? "" : medicalHistoryTextView.text!),AppConstants.ServiceKeys.physicalActivity:physicalHistoryButton.title(for: .normal)!,AppConstants.ServiceKeys.serious:Int(sliderView.value),AppConstants.ServiceKeys.manage:manageDiseaseYesButton.isSelected ? "yes" : "no",AppConstants.ServiceKeys.eatouts:eatOutButton.title(for: .normal)!,AppConstants.ServiceKeys.hardDrinks:hardDrinkYesButton.isSelected ? "yes": "no",AppConstants.ServiceKeys.disease:diseaseArray.joined(separator: ","),AppConstants.ServiceKeys.url:reportArray,AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)]
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.catchNotification),name: NSNotification.Name(rawValue: AppConstants.SignupForm.formTwoServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: params as NSDictionary, url: AppConstants.SignupForm.formTwoServiceSuffix, notificationKey: AppConstants.SignupForm.formTwoServiceNotify, controllerName: self,requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func catchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                AppHelper.getInstance().diseaseArray = diseaseArray
                delegate.sendPopedData(data: 0)
                //AppHelper.getInstance().pushView(identifier: "ThirdFormViewController", controllerName: self)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: message!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}

extension SecondFormViewController : throwData {
    func sendPopedData(data: Any) {
        diseaseArray = data as! [String]
        resizeHeightOfCV()
        diseaseCV.reloadData()
    }
}

extension SecondFormViewController : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == diseaseCV {
            return diseaseArray.count
        }
        return reportArray.count != 5 ? (reportArray.count + 1) : 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == diseaseCV {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
            let name : UILabel = cell.viewWithTag(2) as! UILabel
            name.text = diseaseArray[indexPath.item]
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath)
            (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "add")
            if reportArray.count == 5 {
                AppHelper.getInstance().loadImageIntoImageView(imageUrl: reportArray[indexPath.row], imageView: (cell.viewWithTag(1) as! UIImageView), placeHolder: "placeholder_attachement")
            }
            else if reportArray.count != 0 && indexPath.row > 0 {
//                (cell.viewWithTag(2) as! UIButton).isHidden = indexPath.item == 0 ? true : false
                AppHelper.getInstance().loadImageIntoImageView(imageUrl: reportArray[indexPath.row - 1], imageView: (cell.viewWithTag(1) as! UIImageView), placeHolder: "")
            }
            else {
                (cell.viewWithTag(2) as! UIButton).isHidden = true
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == diseaseCV {
            var calculatedSize : CGSize =   (diseaseArray[indexPath.item]).size(withAttributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12.0)])
            calculatedSize.width += 25
            calculatedSize.height = 40
            return calculatedSize
        }
        else {
            return CGSize(width: 60, height: 80)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == reportCV {
            if indexPath.item == 0 && reportArray.count != 5 {                
                imagePicker.delegate = self
                makeActionSheet()
            }
            else {
                
                AppHelper.getInstance().pushControllerWithData(id: "ImageViewController", controllerName: self, data: reportArray[indexPath.row - 1])
            }
        }
    }
    
    func resizeHeightOfCV() {
        diseaseCVHeightConstraint.constant = CGFloat(diseaseArray.count%2 == 0 ? (diseaseArray.count/2)*40 : ((diseaseArray.count + 1)/2)*40)
    }
}

extension SecondFormViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let uploader = AppDelegate.getDelegate().cloudinary.createUploader()
            DispatchQueue.main.async {
                AppHelper.getInstance().showProgressIndicator(view: self.view)
            }
            uploader.upload(data: (UIImagePNGRepresentation(pickedImage)! as Data?)!, uploadPreset: AppConstants.cloudinaryPresetName, params: nil, progress: nil) { (result, error) in
                DispatchQueue.main.async {
                    AppHelper.getInstance().hideProgressIndicator(view: self.view)
                }
                if error == nil {
                    self.reportArray.append((result?.url)!)
                    self.reportCV.reloadData()
                }
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension SecondFormViewController : CustomDropDownDelegate {
    func getSelectedItem(item: String) {
        if item == AppConstants.Other.active || item == AppConstants.Other.semiActive || item == AppConstants.Other.none {
           physicalHistoryButton.setTitle(item, for: .normal)
        }
        else {
           eatOutButton.setTitle(item, for: .normal)
        }
    }
}

extension SecondFormViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Please type here..." {
            textView.text = ""
        }
        textView.textColor = UIColor.black
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Please type here..."
            textView.textColor = UIColor.darkGray
        }
    }
}
