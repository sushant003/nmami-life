//
//  NavigationWebViewController.swift
//  Kwikard
//
//  Created by kwikard on 31/08/17.
//  Copyright © 2017 kwikard. All rights reserved.
//

import UIKit

class NavigationWebViewController: BaseViewController,UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var headerView: CustomView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWebView()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
    }
    

    func loadWebView()
    {
        webView.isOpaque = false
        headerLabel.text = (data as! [String: String])["pageName"]!
        if AppHelper.getInstance().isNetworkAvailable() {
            let url = URL.init(string: (data as! [String:String])["link"]!)
            let request : NSURLRequest = NSURLRequest.init(url: url!)
            webView.delegate = self
            webView.loadRequest(request as URLRequest)
            webView.scrollView.bounces = false
        }
        else
        {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        AppHelper.getInstance().showProgressIndicator(view: view)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        AppHelper.getInstance().hideProgressIndicator(view: view)
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        AppHelper.getInstance().hideProgressIndicator(view: view)
        AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
                return false
            } else {
                return false
                // Fallback on earlier versions
            }
        }
        return true
    }
    
    @IBAction func onClickBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
