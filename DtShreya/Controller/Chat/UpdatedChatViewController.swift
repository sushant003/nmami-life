//
//  UpdatedChatViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 17/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit
import Firebase
import Photos
import Alamofire

class UpdatedChatViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate {    
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    @IBOutlet var messageTV: UITextView!
    
    @IBOutlet var scrollContentHeightConstraint: NSLayoutConstraint!
    @IBOutlet var chatTV: UITableView!
    @IBOutlet weak var scollView: UIScrollView!
    @IBOutlet weak var serverStatusView: UIView!
    @IBOutlet weak var serverStatusImage: UIImageView!
    @IBOutlet weak var serverStatusLabel: UILabel!
    var dietitianId : Int = 0
    private let imageURLNotSetKey = "NOTSET"
    var scrolledToTop : Bool = false
    lazy var storageRef: StorageReference = Storage.storage().reference(forURL: "gs://nmamilife-c3e76.appspot.com")
    private lazy var firebaseReference : DatabaseReference = DatabaseReference()
    private var updatedMessageRefHandle: DatabaseHandle?
    private var newMessageRefHandle: DatabaseHandle?
    var messageModel : [MessageModel] = [MessageModel]()
    let imagePicker = UIImagePickerController()
    var uploadingMessage : String = ""
    var uploadingImageIndex : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self        
        checkIfChatAvailable()
        serverStatusImage.isHidden = true
        registerForKeyboardEvent()
        scollView.keyboardDismissMode = .onDrag
        
    }
    
    func checkIfChatAvailable() {
        firebaseReference = Database.database().reference().child("Messages").child("\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))")
        firebaseReference.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.childrenCount == 0 {
                self.getIds()
            }
            else {
                self.getCoversations()
            }
        })
    }
    
    func getCoversations() {
        firebaseReference = Database.database().reference().child("Messages").child("\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))")
        newMessageRefHandle = firebaseReference.observe(.childAdded, with: {(snapshot) -> Void in
            let messageData = snapshot.value as! [String:Any]
            if let time = messageData["timestamp"] as! String?, let text = messageData["message"] as! String?, text.count > 0 {
                if messageData["sender"] as! String? ==  "\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))" {
                    let message = MessageModel(message: text, time: AppHelper.getInstance().getTimeFromDate(date: time), type: 1)
                    self.messageModel.append(message)
                    self.insertCell()
                }
                else {
                    if text == "%%UPDATEPROGRESS%%" {
                        let message = MessageModel(message: text, time: AppHelper.getInstance().getTimeFromDate(date: time), type: 6)
                        self.messageModel.append(message)
                    }
                    else {
                        let message = MessageModel(message: text, time: AppHelper.getInstance().getTimeFromDate(date: time), type: 2)
                        self.messageModel.append(message)
                    }
                    
                    self.insertCell()
                }
            }
            else if let _ = messageData["imageurl"],
                let time = messageData["timestamp"] as! String? {
                let fileUrl = messageData["imageurl"] as! String?
                if messageData["sender"] as! String? ==  "\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))" {
                    if fileUrl! == "abc" {
                        self.uploadingImageIndex = self.messageModel.count
                    }
                    let message = MessageModel(message: "", time: AppHelper.getInstance().getTimeFromDate(date: time), image: fileUrl!, type: 3)
                    self.messageModel.append(message)
                    self.insertCell()
                }
                else {
                    let message = MessageModel(message: "", time: AppHelper.getInstance().getTimeFromDate(date: time), image: fileUrl!, type: 4)
                    self.messageModel.append(message)
                    self.insertCell()
                }
            }
            else if let time = messageData["timestamp"] as! String? {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let date = formatter.date(from: time)
                let formatter1 = DateFormatter()
                formatter1.dateFormat = "EE, dd-MMM"
                let message = MessageModel(message: "", time: formatter1.string(from: date!), image: "", type: 5)
                self.messageModel.append(message)
                self.insertCell()
            }
        })
    }
    
    func insertCell() {
        self.chatTV.insertRows(at: [IndexPath(row: (self.messageModel.count) - 1, section: 0)], with: UITableViewRowAnimation.automatic)
        let index: IndexPath = IndexPath(row: (self.messageModel.count) - 1, section: 0)
        DispatchQueue.main.async {
            if !self.scrolledToTop {
            self.chatTV.scrollToRow(at: index, at: .bottom, animated: true)
            }
        }
    }
    
    
    func setServerStatus(text : String) {
        serverStatusView.isHidden = false
        serverStatusImage.isHidden = false
        serverStatusLabel.text = text
    }
    
    //MARK: Webservce methods
    
    func getIds() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.serverStatusView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.getDietitianInfoNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.getDietitianIDServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: AppConstants.Dashboard.getDietitianIDServiceSuffix, notificationKey: AppConstants.Dashboard.getDietitianIDServiceNotify, controllerName: self,requestType: "")
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Net_Error)
        }
    }
    
    @objc func getDietitianInfoNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.serverStatusView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                serverStatusView.isHidden = true
                
                let resultDict: [[String:Any]] = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as? [[String:Any]])!
                if resultDict.count == 0 {
                    setServerStatus(text: message!)
                    return
                }
                for item in resultDict {
                    dietitianId = item[AppConstants.ServiceKeys.dietitianId] as! Int
                    if messageModel.count == 0 {
                        let itemRef = Database.database().reference().child("Messages").child("\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))").childByAutoId()
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let messageDictionary1 = [
                            "timestamp":formatter.string(from: Date())
                            ] as [String : Any]
                        itemRef.setValue(messageDictionary1)
                        
                        let itemRef1 = Database.database().reference().child("Messages").child("\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))").childByAutoId()
                        let messageDictionary = [
                            "message": "Hello! I am your nutritionist. We are happy you are taking your first step in your Look Great,Be Fit,Feel Alive journey with us!\nI have all your details and would be creating and explaining you your diet now.\nAre you comfortable chatting or would like to get on a call with me?",
                            "sender":"\(dietitianId)",
                            "timestamp":formatter.string(from: Date())
                            ] as [String : Any]
                        itemRef1.setValue(messageDictionary)
                    }
                    getCoversations()
                }
            }
            else {
                setServerStatus(text: message!)
            }
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Server_Error)
        }
    }
    
    //MARK: table view delegate and datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell? = nil
        if messageModel[indexPath.row].type == 5 {
            cell = tableView.dequeueReusableCell(withIdentifier: "timeSectionCell", for: indexPath)
            (cell?.viewWithTag(1) as! UILabel).text = messageModel[indexPath.row].time
        }
        if messageModel[indexPath.row].type == 6 {
            cell = tableView.dequeueReusableCell(withIdentifier: "updateProgressCell", for: indexPath)
        }
        if messageModel[indexPath.row].type == 3 {
            cell = tableView.dequeueReusableCell(withIdentifier: "myImageAttachement", for: indexPath)
            DispatchQueue.main.async {
                (cell?.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "placeholder_attachement")
            }
            if messageModel[indexPath.row].attachedImage.hasPrefix("gs:") {
                let islandRef = Storage.storage().reference(forURL: messageModel[indexPath.row].attachedImage)
                islandRef.getData(maxSize: 8 * 1024 * 1024) { data, error in
                    if error != nil {
                    }
                    else {
                        let image = UIImage(data: data!)
                        (cell?.viewWithTag(1) as! UIImageView).image = image
                    }
                }
            }
            else {
            AppHelper.getInstance().loadImageIntoImageView(imageUrl: messageModel[indexPath.row].attachedImage, imageView: (cell?.viewWithTag(1) as! UIImageView), placeHolder: "placeholder_attachement")
            }
            (cell?.viewWithTag(2) as! UILabel).text = messageModel[indexPath.row].time
        }
        else if  messageModel[indexPath.row].type == 4 {
             cell = tableView.dequeueReusableCell(withIdentifier: "otherImageAttachement", for: indexPath)
            DispatchQueue.main.async {
                (cell?.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "placeholder_attachement")
            }
            if messageModel[indexPath.row].attachedImage.hasPrefix("gs:") {
                let islandRef = Storage.storage().reference(forURL: messageModel[indexPath.row].attachedImage)
                islandRef.getData(maxSize: 8 * 1024 * 1024) { data, error in
                    if error != nil {
                    }
                    else {
                        let image = UIImage(data: data!)
                        (cell?.viewWithTag(1) as! UIImageView).image = image
                    }
                }
            }
            else {
                AppHelper.getInstance().loadImageIntoImageView(imageUrl: messageModel[indexPath.row].attachedImage, imageView: (cell?.viewWithTag(1) as! UIImageView), placeHolder: "placeholder_attachement")
            }
            (cell?.viewWithTag(2) as! UILabel).text = messageModel[indexPath.row].time
        }
        else if  messageModel[indexPath.row].type == 1 {
            cell = tableView.dequeueReusableCell(withIdentifier: "myChatCell", for: indexPath)
            (cell?.viewWithTag(1) as! UITextView).text = messageModel[indexPath.row].message
            (cell?.viewWithTag(2) as! UILabel).text = messageModel[indexPath.row].time
        }
        else if  messageModel[indexPath.row].type == 2 {
            cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath)
            (cell?.viewWithTag(1) as! UITextView).text = messageModel[indexPath.row].message
            (cell?.viewWithTag(2) as! UILabel).text = messageModel[indexPath.row].time
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if messageModel[indexPath.row].type == 1 || messageModel[indexPath.row].type == 2 {
            return 80
        }
        else {
            return 175
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK: IBAction
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickAttachmentButton(_ sender: Any) {
        self.view.endEditing(true)
        self.imagePicker.sourceType = .photoLibrary
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func onClickSendButton(_ sender: Any) {
        if messageTV.text == "" || messageTV.text == "Write your message" {
            return
        }
        let itemRef = Database.database().reference().child("Messages").child("\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))").childByAutoId()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let messageDictionary = [             
            "message": messageTV.text!,
            "sender":AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),
            "timestamp":formatter.string(from: Date())
            ] as [String : Any]
//        let message = messageTV.text!
        self.view.endEditing(true)
        itemRef.setValue(messageDictionary)
//        sendMessageNotification(message: message)
    }
    
    @IBAction func onClickOpenImage(_ sender: UIButton) {
        let indexPath : IndexPath = chatTV.indexPath(for: sender.superview?.superview?.superview as! UITableViewCell)!
        if messageModel[indexPath.row].type == 3 || messageModel[indexPath.row].type == 4 {
            AppHelper.getInstance().pushControllerWithData(id: "ImageViewController", controllerName: self, data: messageModel[indexPath.row].attachedImage)
        }
    }
    
    @IBAction func onClickAddProgress(_ sender: UIButton) {
        AppHelper.getInstance().alertWithTextField(title: "Update Progress", placeholder: "Enter your weight", text: "", controllerName: self, leftButtonTite: "submit", isNumericTextField: true)
    }
    
    //MARK: Webservice methods
    
    func setUserWeight(weight : String) {
        if AppHelper.getInstance().isNetworkAvailable() {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            NotificationCenter.default.addObserver(self,selector: #selector(self.catchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.add_progress_service_notify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.weight:weight,AppConstants.ServiceKeys.date:formatter.string(from: Date())], url: AppConstants.Clients.add_progress_service_suffix, notificationKey: AppConstants.Clients.add_progress_service_notify, controllerName: self,requestType: "")
            AppHelper.getInstance().showProgressIndicator(view: self.view)
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func catchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
//                navigationController?.popViewController(animated: true)
//                (data as! throwData).sendPopedData(data: "")
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: (serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String)!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    
    /*
    func sendMessageNotification(message: String) {
        let queue = DispatchQueue(label: "abc")
        queue.async {
            
            Alamofire.request(AppConstants.mainUrl.appending("chatnotification/"), method: .post, parameters: ["to":self.dietitianId,"from":AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),"message":message], encoding: JSONEncoding.default,headers: nil).validate(contentType: ["text/html"]).responseJSON {
                (response) in
                if(response.result.value != nil && Alamofire.JSONSerialization.isValidJSONObject(response.result.value!)) {
                }
                else {
                    print(response.error?.localizedDescription ?? "")
                }
            }
        }
    }
    */
    func setImageURL(_ url: String) {
        let itemRef = Database.database().reference().child("Messages").child("\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))").childByAutoId()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let messageItem = [
            "imageurl":url,
            "sender": AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),
            "timestamp":formatter.string(from: Date())
            ] as [String : Any]
//        itemRef.setValue(messageItem)
        itemRef.setValue(messageItem) { (error, reference) in
            self.uploadingMessage = reference.key
        }
       // sendMessageNotification(message: "\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.name)) sent you a image")
    }
}

extension UpdatedChatViewController : AlertTextDelegate {
    func dismissAlertView() {
        
    }
    
    func getInputText(text: String) {
        if text.count != 0 {
            setUserWeight(weight: text)
        }
    }
}

extension UpdatedChatViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if #available(iOS 11.0, *) {
            if let photoReferenceUrl = info[UIImagePickerControllerPHAsset] as? URL {
                let assets = PHAsset.fetchAssets(withALAssetURLs: [photoReferenceUrl], options: nil)
                let asset = assets.firstObject
                asset?.requestContentEditingInput(with: nil, completionHandler: { (contentEditingInput, info) in
                    let imageFileURL = contentEditingInput?.fullSizeImageURL
                    let path = "chat_photos/" + "\(photoReferenceUrl.lastPathComponent)"
//                    DispatchQueue.main.async {
//                        AppHelper.getInstance().showProgressIndicator(view: self.view)
//                    }
                    self.setImageURL("abc")
                    self.storageRef.child(path).putFile(from: imageFileURL!, metadata: nil) { (metadata, error) in
//                        DispatchQueue.main.async {
//                            AppHelper.getInstance().hideProgressIndicator(view: self.view)
//                        }
                        if let error = error {
                            print("Error uploading photo: \(error.localizedDescription)")
                            return
                        }
                        let itemRef = Database.database().reference().child("Messages").child("\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))").child(self.uploadingMessage)
                        itemRef.updateChildValues(["imageurl" : "\(metadata!.downloadURL()!)"], withCompletionBlock: { (error, reference) in
                            self.messageModel[self.uploadingImageIndex].attachedImage = "\(metadata!.downloadURL()!)"
                            self.chatTV.reloadRows(at: [IndexPath(item: self.uploadingImageIndex, section: 0)], with: .automatic)
                        })
                    }
                })
            }
            else {
                let image = info[UIImagePickerControllerOriginalImage] as! UIImage
                let imageData = UIImageJPEGRepresentation(image, 1.0)
                let imagePath = "\(Int(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
                let metadata = StorageMetadata()
                metadata.contentType = "image/jpeg"
                let path = "chat_photos/" + "\(imagePath)"
//                DispatchQueue.main.async {
//                    AppHelper.getInstance().showProgressIndicator(view: self.view)
//                }
                self.setImageURL("abc")
                storageRef.child(path).putData(imageData!, metadata: metadata) { (metadata, error) in
//                    DispatchQueue.main.async {
//                        AppHelper.getInstance().hideProgressIndicator(view: self.view)
//                    }
                    if let error = error {
                        print("Error uploading photo: \(error)")
                        return
                    }
                    
                    let itemRef = Database.database().reference().child("Messages").child("\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))").child(self.uploadingMessage)
                    itemRef.updateChildValues(["imageurl" : "\(metadata!.downloadURL()!)"], withCompletionBlock: { (error, reference) in
                        self.messageModel[self.uploadingImageIndex].attachedImage = "\(metadata!.downloadURL()!)"
                        self.chatTV.reloadRows(at: [IndexPath(item: self.uploadingImageIndex, section: 0)], with: .automatic)
                    })
                }
            }
        } else {
            if let photoReferenceUrl = info[UIImagePickerControllerReferenceURL] as? URL {
                let assets = PHAsset.fetchAssets(withALAssetURLs: [photoReferenceUrl], options: nil)
                let asset = assets.firstObject
                asset?.requestContentEditingInput(with: nil, completionHandler: { (contentEditingInput, info) in
                    let imageFileURL = contentEditingInput?.fullSizeImageURL
                    let path = "chat_photos/" + "\(photoReferenceUrl.lastPathComponent)"
                    self.setImageURL("abc")
                    self.storageRef.child(path).putFile(from: imageFileURL!, metadata: nil) { (metadata, error) in
                        if let error = error {
                            print("Error uploading photo: \(error.localizedDescription)")
                            return
                        }
                        let itemRef = Database.database().reference().child("Messages").child("\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))").child(self.uploadingMessage)
                        itemRef.updateChildValues(["imageurl" : "\(metadata!.downloadURL()!)"], withCompletionBlock: { (error, reference) in
                            self.messageModel[self.uploadingImageIndex].attachedImage = "\(metadata!.downloadURL()!)"
                        })
                    }
                })
            }
            else {
                let image = info[UIImagePickerControllerOriginalImage] as! UIImage
                let imageData = UIImageJPEGRepresentation(image, 1.0)
                let imagePath = "\(Int(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
                let metadata = StorageMetadata()
                let path = "chat_photos/" + "\(imagePath)"
                metadata.contentType = "image/jpeg"
                self.setImageURL("abc")
                storageRef.child(path).putData(imageData!, metadata: metadata) { (metadata, error) in
                    if let error = error {
                        print("Error uploading photo: \(error)")
                        return
                    }
                    let itemRef = Database.database().reference().child("Messages").child("\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))").child(self.uploadingMessage)
                    itemRef.updateChildValues(["imageurl" : "\(metadata!.downloadURL()!)"], withCompletionBlock: { (error, reference) in
                        self.messageModel[self.uploadingImageIndex].attachedImage = "\(metadata!.downloadURL()!)"
                    })
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: Textfield delegate
extension UpdatedChatViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.text = "Write your message"
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.contentSize.height >= 90 {
        }
        else {
            heightConstraint.constant = messageTV.contentSize.height
        }
        if messageModel.count != 0 {
            chatTV.scrollToRow(at: IndexPath(row: messageModel.count - 1, section: 0), at: .bottom, animated: false)
        }
    }
    
    func registerForKeyboardEvent() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidOpen(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidClose(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func unregisterKeyboardEvent() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil);NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardDidOpen(notification:Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            if scrollContentHeightConstraint.constant == 0 {
                let keyboardRectangle = keyboardFrame.cgRectValue
                scrollContentHeightConstraint.constant -= keyboardRectangle.height
                if messageModel.count != 0 {
                    chatTV.scrollToRow(at: IndexPath(row: messageModel.count - 1, section: 0), at: .bottom, animated: false)
                }
            }
        }
    }
    
    @objc func keyboardDidClose(notification:Notification) {
        if scrollContentHeightConstraint.constant != 0 {
            scrollContentHeightConstraint.constant = 0
        }
        heightConstraint.constant = 30
    }
}
