//
//  TabBarViewController.swift
//  Meditician
//
//  Created by Sushant Jugran on 09/04/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit
import  SideMenuController

class TabBarViewController: BaseViewController {
    @IBOutlet weak var tabBatItem: UITabBar!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tabBarTitle: UILabel!
    @IBOutlet weak var nonActiveTabBarView: UIView!
    @IBOutlet weak var tapbaritem1: UITabBar!
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var weeklyDietButton: UIButton!
    
    var controllersList: [Int : BaseViewController] = [Int : BaseViewController]()
    
    override func viewDidLoad() {        
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        tabBatItem.tintColor = UIColor(displayP3Red: 56/255.0, green: 164/255.0, blue: 177/255.0, alpha: 1.0)
        tapbaritem1.tintColor = UIColor(displayP3Red: 56/255.0, green: 164/255.0, blue: 177/255.0, alpha: 1.0)
        if isValidUser() {
            tabBatItem.selectedItem = (tabBatItem.items?[0])!
            tabBar(tabBatItem, didSelect: tabBatItem.selectedItem!)
            nonActiveTabBarView.isHidden = true
        }
        else {
            tapbaritem1.selectedItem = (tapbaritem1.items?[0])!
            tabBar(tapbaritem1, didSelect: tapbaritem1.selectedItem!)
        }
        DispatchQueue.main.async {
            AppHelper.getInstance().saveDeviceToken(controller: nil)
        }        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
    }
    
    func isValidUser() -> Bool {
        if AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.dieterStatus) as! String == "1" {
            return true
        }
        return true
    }
    
    func getShoppingList() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.getDietCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.shopping_list_service_notify),object: nil)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.date:formatter.string(from: Date())], url: AppConstants.Clients.shopping_list_service_suffix, notificationKey: AppConstants.Clients.shopping_list_service_notify, controllerName: self, requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func getDietCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                let resultData: [String] = (serviceData.userInfo?["list"] as! [String])
                AppHelper.getInstance().pushControllerWithData(id: "ShoppingViewController", controllerName: self, data: resultData)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "\(message!)", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    //MARK: IBAction
    @IBAction func onClickMenu(_ sender: Any) {
        sideMenuController?.toggle()
    }
    
    @IBAction func onClickDiary(_ sender: Any) {
        AppHelper.getInstance().pushControllerWithData(id: "DiaryViewController", controllerName: self, data: false)
    }
    
    
    @IBAction func onClickWeeklyDiet(_ sender: Any) {
        AppHelper.getInstance().pushView(identifier: "WeeklyDietViewController", controllerName: self)
    }
    
    @IBAction func onClickShopping(_ sender: Any) {
        getShoppingList()
    }
    
    @IBAction func onClickUserProfile(_ sender: Any) {
        AppHelper.getInstance().pushView(identifier: "ProfileViewController", controllerName: self)
    }
}

extension TabBarViewController : UITabBarDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        switch item.tag {
        case 0:
            if isValidUser() {
                tabBarTitle.text = "Dashboard"
                //  if(!controllersList.keys.contains(item.tag)) {
                let controller = AppHelper.getInstance().GetViewControllerIntance(id: "DashboardViewController", controllerName: self) as! BaseViewController
                addViewController(controller: controller)
                weeklyDietButton.isHidden = true
                controllersList[item.tag] = controller
            }
            else {
                tabBarTitle.text = "Activity"
                let controller = AppHelper.getInstance().GetViewControllerIntance(id: "NonActiveViewController", controllerName: self) as! BaseViewController
                controller.data = 0
                addViewController(controller: controller)
                weeklyDietButton.isHidden = true
                controllersList[item.tag] = controller
            }
        case 1:
            if isValidUser() {
                tabBarTitle.text = "Chat"
                let controller = AppHelper.getInstance().GetViewControllerIntance(id: "UpdatedChatViewController", controllerName: self) as! BaseViewController
                addViewController(controller: controller)
                weeklyDietButton.isHidden = true
                controllersList[item.tag] = controller
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: "User Confirmation Awaited...", message: "You are not a confirmed user yet! Try login again for update.", cancelButtonName: "Cancel",otherButtonName: "Login", controllerName: self,isAlertOnly: false)
            }
            
        case 2:
            if isValidUser() {
                tabBarTitle.text = "Diet"
                let controller = AppHelper.getInstance().GetViewControllerIntance(id: "DietViewController", controllerName: self) as! BaseViewController
                addViewController(controller: controller)
                weeklyDietButton.isHidden = false
                controllersList[item.tag] = controller
            }
            else {
                tabBarTitle.text = "Tips"
                let controller = AppHelper.getInstance().GetViewControllerIntance(id: "NonActiveViewController", controllerName: self) as! BaseViewController
                controller.data = 2
                addViewController(controller: controller)
                weeklyDietButton.isHidden = true
                controllersList[item.tag] = controller
            }
        case 4:
            if isValidUser() {
                tabBarTitle.text = "Profile"
                let controller = AppHelper.getInstance().GetViewControllerIntance(id: "ProfileViewController", controllerName: self) as! BaseViewController
                controller.data = true
                addViewController(controller: controller)
                weeklyDietButton.isHidden = true
                controllersList[item.tag] = controller
            }
            else {
                tabBarTitle.text = "Blog"
                let controller = AppHelper.getInstance().GetViewControllerIntance(id: "NonActiveViewController", controllerName: self) as! BaseViewController
                controller.data = 1
                addViewController(controller: controller)
                weeklyDietButton.isHidden = true
                controllersList[item.tag] = controller
            }
        default:
            if isValidUser() {
                tabBarTitle.text = "Progress"
                let controller = AppHelper.getInstance().GetViewControllerIntance(id: "ProgressViewController", controllerName: self) as! BaseViewController
                addViewController(controller: controller)
                weeklyDietButton.isHidden = true
                controllersList[item.tag] = controller
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: "User Confirmation Awaited...", message: "You are not a confirmed user yet! Try login again for update.", cancelButtonName: "Cancel",otherButtonName: "Login", controllerName: self,isAlertOnly: false)
            }
        }
    }
    
    func addViewController(controller: BaseViewController) {
        for contrlr in childViewControllers {
            if(contrlr == controller) {
                return
            }
            else {
                contrlr.removeFromParentViewController()
            }
        }
        for view in containerView.subviews {
            view.removeFromSuperview()
        }
        
        addChildViewController(controller)
        controller.view.frame = containerView.bounds
        containerView.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
    }
    
}

extension TabBarViewController : alertPositiveButtonDelegate {
    func alertPositiveButtonPressed() {
        AppHelper.getInstance().pushView(identifier: "LoginViewController", controllerName: self)
    }
    
    func alertNegativeButtonPressed() {
    }
}
