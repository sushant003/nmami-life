//
//  DiseaseDiaryViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 25/09/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit
import SideMenuController

class DiseaseDiaryViewController: BaseViewController {

    @IBOutlet weak var diseaseTV: UITableView!
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    
    var diseaseDiaryData : [DiseaseDiaryModel] = [DiseaseDiaryModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        statusImage.isHidden = true
        let headerNib = UINib.init(nibName: "HeaderView", bundle: Bundle.main)
        diseaseTV.register(headerNib, forHeaderFooterViewReuseIdentifier: "HeaderView")
        diseaseTV.sectionFooterHeight = 2.0
        getDiseaseDiary()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
    }
    
    //MARK: IBAction
    
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickAddDiary(_ sender: Any) {
        AppHelper.getInstance().pushControllerWithData(id: "CreateDiseaseDiaryViewController", controllerName: self, data: self)
    }
    
    
    //MARK: Webservice methods
    func getDiseaseDiary() {
        diseaseDiaryData.removeAll()
        if AppHelper.getInstance().isNetworkAvailable() {
            NotificationCenter.default.addObserver(self,selector: #selector(self.getDiseaseDiaryCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_disease_diary_notify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: AppConstants.Clients.get_disease_diary_suffix, notificationKey: AppConstants.Clients.get_disease_diary_notify, controllerName: self, requestType: "")
            AppHelper.getInstance().showProgressIndicator(view: statusView)
        }
        else {
            statusImage.isHidden = false
            statusLabel.text = AppConstants.ErrorMessags.Net_Error
        }
    }
    
    @objc func getDiseaseDiaryCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.statusView)
        if(serviceData.userInfo != nil) {
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                statusView.isHidden = true
                let resultDict: [[String:Any]] = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as! [[String:Any]])
                prepareTableData(serviceData: resultDict)
                diseaseTV.dataSource = self
                diseaseTV.delegate = self
                diseaseTV.reloadData()
            }
            else {
                statusView.isHidden = false
                statusImage.isHidden = false
                statusLabel.text = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            }
        }
        else {
            statusView.isHidden = false
            statusImage.isHidden = false
            statusLabel.text = AppConstants.ErrorMessags.Server_Error
        }
    }
    
    func prepareTableData(serviceData: [[String:Any]]) {
        for disease in serviceData {
            let notes = (disease[AppConstants.ServiceKeys.notes] as! [[String:String]]).map { (item) -> String in
                return item[AppConstants.ServiceKeys.note]!
            }
            print(notes)
            let diseaseDiaryModel = DiseaseDiaryModel(collapsed: false, diseaseName: disease[AppConstants.ServiceKeys.diseaseName] as! String, diseaseNotes: notes)
            diseaseDiaryData.append(diseaseDiaryModel)
        }
    }
}

extension DiseaseDiaryViewController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return diseaseDiaryData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return diseaseDiaryData[section].collapsed ? 0 : diseaseDiaryData[section].diseaseNotes.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as! HeaderView
        headerView.isCollapsed = diseaseDiaryData[section].collapsed
        headerView.containerView.backgroundColor = AppConstants.Colors.blueColor.withAlphaComponent(0.5)
        headerView.headerLabel.text = diseaseDiaryData[section].diseaseName
        headerView.seactionIndex = section
        headerView.delegate = self
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        (cell.viewWithTag(1) as! UILabel).text = diseaseDiaryData[indexPath.section].diseaseNotes[indexPath.row]
        
        if indexPath.row == diseaseDiaryData[indexPath.section].diseaseNotes.count - 1 {
            if let constraint = ((cell.viewWithTag(2))!.constraints.filter{$0.firstAttribute == .height}.first) {
                constraint.constant = 1
            }
        }
        else {
            if let constraint = ((cell.viewWithTag(2))!.constraints.filter{$0.firstAttribute == .height}.first) {
                constraint.constant = 0.5
            }
        }
        return cell
    }
    
    //To disable floating header
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let sectionHeaderHeight: CGFloat = 40
        if scrollView.contentOffset.y <= sectionHeaderHeight &&
            scrollView.contentOffset.y >= 0 {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0)
        } else if scrollView.contentOffset.y >= sectionHeaderHeight {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0)
        }
    }
}

extension DiseaseDiaryViewController: SeactionHeaderClicked {
    func commitAction(section: Int) {        
    }
    
    func reloadTable(section: Int,collapsed : Bool) {
        diseaseDiaryData[section].collapsed = collapsed
        diseaseTV.reloadSections(NSIndexSet(index: section) as IndexSet, with: UITableViewRowAnimation.fade)
    }
}

extension DiseaseDiaryViewController : throwData {
    func sendPopedData(data: Any) {
        getDiseaseDiary()
    }
}
