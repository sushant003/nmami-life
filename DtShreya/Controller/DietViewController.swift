//
//  CreateDietViewController.swift
//  Meditician
//
//  Created by Sushant Jugran on 10/07/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit

class DietViewController: BaseViewController {
    
    @IBOutlet weak var dietTableView: UITableView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    
    var mealData : [MealModel] =  [MealModel]()
    var supplements : [String] = [String]()
    var supplementCollapsed : Bool = true
    var recipeName : String = ""    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusImage.isHidden = true
        let headerNib = UINib.init(nibName: "HeaderView", bundle: Bundle.main)
        dietTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "HeaderView")
        dietTableView.sectionFooterHeight = 2.0
        getDiet()
    }
    
    //MARK: IBAction
    
    @IBAction func onClickReplaceItem(_ sender: UIButton) {
        let indexPath = dietTableView.indexPath(for: sender.superview?.superview as! UITableViewCell)!
        mealData[indexPath.section].meal[indexPath.row].mealChanged = !mealData[indexPath.section].meal[indexPath.row].mealChanged
        dietTableView.reloadRows(at: [IndexPath(row: indexPath.row, section: indexPath.section)], with: .none)
    }
    
    //MARK: Webservice Methods
    func getDiet() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.statusView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.getDietCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_diet_service_notify),object: nil)
            mealData.removeAll()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"            
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.date:formatter.string(from: Date())], url: AppConstants.Clients.get_diet_service_suffix, notificationKey: AppConstants.Clients.get_diet_service_notify, controllerName: self, requestType: "")
            AppHelper.getInstance().showProgressIndicator(view: statusView)
        }
        else {
            statusImage.isHidden = false
            statusLabel.text = AppConstants.ErrorMessags.Net_Error
        }
    }
    
    @objc func getDietCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.statusView)
        if(serviceData.userInfo != nil) {
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                let resultDict: [String:Any] = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as! [String:Any])
                if (serviceData.userInfo?[AppConstants.ServiceKeys.recallStatus] as! Bool) {
                    statusView.isHidden = true
                    prepareTableData(serviceData: resultDict)
                    dietTableView.dataSource = self
                    dietTableView.delegate = self
                    dietTableView.reloadData()                   
                }
                else {
                    statusImage.isHidden = false
                    AppHelper.getInstance().showAlert(alertTitle: "Diet Recall Pending!", message: "Fill the diet recall form to unlock your free trial.", cancelButtonName: "Cancel", otherButtonName: "Ok", controllerName: self, isAlertOnly: false)
                }
            }
            else {
                statusView.isHidden = false
                statusImage.isHidden = false
                statusLabel.text = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            }
        }
        else {
            statusView.isHidden = false
            statusImage.isHidden = false
            statusLabel.text = AppConstants.ErrorMessags.Server_Error
        }
    }
    
    func prepareTableData(serviceData : [String:Any]) {
        for (key,value) in serviceData[AppConstants.ServiceKeys.diet] as! [String:Any]  {
            var meal : MealModel!
            var subMeal : [SubMealModel] = [SubMealModel]()
            if ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool) {
                for i in 0..<((value as! [String:Any])[AppConstants.ServiceKeys.meal] as! [String]).count {
                    subMeal.append(SubMealModel(firstFoodName: ((value as! [String:Any])[AppConstants.ServiceKeys.meal] as! [String])[i], secondFoodName: ((value as! [String:Any])[AppConstants.ServiceKeys.meal1] as! [String])[i] == "0" ? "" : ((value as! [String:Any])[AppConstants.ServiceKeys.meal1] as! [String])[i], firstFoodQuantity: ((value as! [String:Any])[AppConstants.ServiceKeys.quantity] as! [String])[i], secondFoodQuantity: ((value as! [String:Any])[AppConstants.ServiceKeys.quantity1] as! [String])[i] == "0" ? "" : ((value as! [String:Any])[AppConstants.ServiceKeys.quantity1] as! [String])[i], option: ((value as! [String:Any])[AppConstants.ServiceKeys.meal1] as! [String])[i] == "0" ? false : true, mealChanged: false))
                }
                //((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool)
            }
            else {
                for i in 0..<((value as! [String:Any])[AppConstants.ServiceKeys.meal] as! [String]).count {
                    subMeal.append(SubMealModel(firstFoodName: ((value as! [String:Any])[AppConstants.ServiceKeys.meal] as! [String])[i], secondFoodName: "", firstFoodQuantity: ((value as! [String:Any])[AppConstants.ServiceKeys.quantity] as! [String])[i], secondFoodQuantity: "", option: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool), mealChanged: false))
                }
            }
            switch key {
            case AppConstants.ServiceKeys.firstMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.firstMealName, meal: subMeal, collapsed: true, mealIndex: 0, missed: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool))
            case AppConstants.ServiceKeys.secondMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.secondMealName, meal: subMeal, collapsed: true, mealIndex: 1, missed: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool))
            case AppConstants.ServiceKeys.thirdMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.thirdMealName, meal: subMeal, collapsed: true, mealIndex: 2, missed: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool))
            case AppConstants.ServiceKeys.fourthMeal:
                
                meal = MealModel(name: AppConstants.ServiceKeys.fourthMealName, meal: subMeal, collapsed: true, mealIndex: 3, missed: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool))
            case AppConstants.ServiceKeys.fifthMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.fifthMealName, meal: subMeal, collapsed: true, mealIndex: 4, missed: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool))
            case AppConstants.ServiceKeys.sixthMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.sixthMealName, meal: subMeal, collapsed: true, mealIndex: 5, missed: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool))
            case AppConstants.ServiceKeys.seventhMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.seventhMealName, meal: subMeal, collapsed: true, mealIndex: 6, missed: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool))
            case AppConstants.ServiceKeys.eighthMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.eightMealName, meal: subMeal, collapsed: true, mealIndex: 7, missed: ((value as! [String:Any])[AppConstants.ServiceKeys.option] as! Bool))
            default:
                break
            }
            if key != "nine" {
                if meal.meal.count != 0 {
                    mealData.append(meal!)
                }
            }
        }
        supplements = serviceData[AppConstants.ServiceKeys.supplements] as! [String]
        mealData = mealData.sorted(by: { (first, second) -> Bool in
            first.index < second.index
        })
    }
    
    func commitMealActions(meal:String,actionType:Int) {
        if AppHelper.getInstance().isNetworkAvailable() {
            NotificationCenter.default.addObserver(self,selector: #selector(self.changeDietStatusNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_diet_service_notify),object: nil)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.date:formatter.string(from: Date()),AppConstants.ServiceKeys.mealNumber:meal,AppConstants.ServiceKeys.action:actionType], url: AppConstants.Clients.change_diet_status_suffix, notificationKey: AppConstants.Clients.get_diet_service_notify, controllerName: self, requestType: "")
            AppHelper.getInstance().showProgressIndicator(view: statusView)
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func changeDietStatusNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.statusView)
        if(serviceData.userInfo != nil) {
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Request recorded", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: (serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String)!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}

//MARK: Tableview datasource and delegate
extension DietViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        if supplements.count != 0 {
        return mealData.count + 1
        }
        else {
            return mealData.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section < mealData.count  {
            return mealData[section].collapsed ? 0 : mealData[section].meal.count
        }
        else {
            return supplementCollapsed ? 0 : supplements.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as! HeaderView
        headerView.delegate = self
        headerView.seactionIndex = section
        headerView.collapseButton.isHidden = true
        headerView.actionButton.isHidden = false
        headerView.dropDownButton.isHidden = false
        if section < mealData.count {
        headerView.isCollapsed = mealData[section].collapsed
        headerView.headerLabel.text = mealData[section].mealTimeName
        }
        else {
            headerView.isCollapsed = supplementCollapsed
            headerView.headerLabel.text = AppConstants.ServiceKeys.supplementsName
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 25
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mealCell", for: indexPath)
        if indexPath.section == mealData.count {
            (cell.viewWithTag(1) as! UILabel).text = supplements[indexPath.row]
            (cell.viewWithTag(2) as! UILabel).text = ""
            (cell.viewWithTag(3) as! UIButton).isHidden = true
            
            for constraint in (cell.viewWithTag(3) as! UIButton).constraints {
                if constraint.identifier == "height" {
                    constraint.constant = -5
                }
            }
        }
        else {
            (cell.viewWithTag(1) as! UILabel).text = mealData[indexPath.section].meal[indexPath.row].mealChanged ? mealData[indexPath.section].meal[indexPath.row].secondFoodName : mealData[indexPath.section].meal[indexPath.row].firstFoodName
            (cell.viewWithTag(2) as! UILabel).text = mealData[indexPath.section].meal[indexPath.row].mealChanged ? mealData[indexPath.section].meal[indexPath.row].secondFoodQuantity : mealData[indexPath.section].meal[indexPath.row].firstFoodQuantity
            
            if !mealData[indexPath.section].meal[indexPath.row].option {
                (cell.viewWithTag(3) as! UIButton).isHidden = true
                for constraint in (cell.viewWithTag(3) as! UIButton).constraints {
                    if constraint.identifier == "height" {
                        constraint.constant = -5
                    }
                }
            }
            else {
                (cell.viewWithTag(3) as! UIButton).isHidden = false
                for constraint in (cell.viewWithTag(3) as! UIButton).constraints {
                    if constraint.identifier == "height" {
                        constraint.constant = 20
                    }
                }
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        if indexPath.section != mealData.count {
//            return false
//        }
//        else {
//            return false
//        }
//    }
    
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//        var actionArray: [UITableViewRowAction] = [UITableViewRowAction]()
//        var mealName : String = ""
//        switch indexPath.section {
//        case 0:
//            mealName = AppConstants.ServiceKeys.firstMeal
//        case 1:
//            mealName = AppConstants.ServiceKeys.secondMeal
//        case 2:
//            mealName = AppConstants.ServiceKeys.thirdMeal
//        case 3:
//            mealName = AppConstants.ServiceKeys.fourthMeal
//        case 4:
//            mealName = AppConstants.ServiceKeys.fifthMeal
//        case 5:
//            mealName = AppConstants.ServiceKeys.sixthMeal
//        case 6:
//            mealName = AppConstants.ServiceKeys.seventhMeal
//        case 7:
//            mealName = AppConstants.ServiceKeys.eighthMeal
//        default:
//            break
//        }
//        let missedAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Missed", handler:{action, indexpath in
//            if !self.mealData[indexPath.section].missed {
//                self.commitMealActions(meal: mealName, actionType: 0)
//            }
//            else {
//                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Request already recorded", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
//            }
//        });
//        let takenAction = UITableViewRowAction(style: UITableViewRowActionStyle.normal, title: "Taken", handler:{action, indexpath in
//            self.commitMealActions(meal: mealName, actionType: 1)
//        });
//        actionArray.append(missedAction)
//        actionArray.append(takenAction)
//        return actionArray
//    }
}

extension DietViewController: SeactionHeaderClicked {
    func reloadTable(section: Int,collapsed : Bool) {
        if section < mealData.count {
            mealData[section].collapsed = collapsed
        }
        else {
            supplementCollapsed = !supplementCollapsed
        }
        dietTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: UITableViewRowAnimation.fade)
    }
    
    func commitAction(section: Int) {
        var mealName : String = ""
        switch section {
        case 0:
            mealName = AppConstants.ServiceKeys.firstMeal
        case 1:
            mealName = AppConstants.ServiceKeys.secondMeal
        case 2:
            mealName = AppConstants.ServiceKeys.thirdMeal
        case 3:
            mealName = AppConstants.ServiceKeys.fourthMeal
        case 4:
            mealName = AppConstants.ServiceKeys.fifthMeal
        case 5:
            mealName = AppConstants.ServiceKeys.sixthMeal
        case 6:
            mealName = AppConstants.ServiceKeys.seventhMeal
        case 7:
            mealName = AppConstants.ServiceKeys.eighthMeal
        default:
            break
        }
        
        let alertView = UIAlertController(title:AppConstants.AppName , message: "What action you want to perform", preferredStyle: UIAlertControllerStyle.actionSheet)
        alertView.addAction(UIAlertAction(title: "Missed", style: UIAlertActionStyle.default, handler: { action in
            if !self.mealData[section].missed {
                self.commitMealActions(meal: mealName, actionType: 0)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Request already recorded", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }))
        alertView.addAction(UIAlertAction(title: "Taken", style: UIAlertActionStyle.default, handler: { action in
            self.commitMealActions(meal: mealName, actionType: 1)
        }))
        alertView.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler: { action in
        }))
        self.present(alertView, animated: true, completion: nil)
    }
}



extension DietViewController : throwData {
    func sendPopedData(data: Any) {
        if data as! String == "0" {
            statusView.isHidden = false
            statusLabel.text = "No Diet Found"
            statusImage.isHidden = false
        }
        else {
            getDiet()
        }
    }
}

extension DietViewController : alertPositiveButtonDelegate {
    func alertPositiveButtonPressed() {
        AppHelper.getInstance().pushControllerWithData(id: "DietRecallViewController", controllerName: self, data: self)
    }
    
    func alertNegativeButtonPressed() {
    }
}

