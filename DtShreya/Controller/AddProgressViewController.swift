//
//  AddProgressViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 21/09/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class AddProgressViewController: BaseViewController {

    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var weightCountLabel: UILabel!
    @IBOutlet weak var sugarCountLabel: UILabel!
    @IBOutlet weak var bmiCountLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLoad()
        headerView.setGradientBackground()
    }
    
    //MARK: IBAction
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func decreaseWeightCount(_ sender: Any) {
        guard let weight = Int(weightCountLabel.text!) else {
            return
        }
        weightCountLabel.text = "\(weight - 1)"
    }
    
    @IBAction func increaseWeightCount(_ sender: Any) {
        guard let weight = Int(weightCountLabel.text!) else {
            return
        }
        weightCountLabel.text = "\(weight + 1)"
    }
    
    @IBAction func decreaseSugarLevel(_ sender: Any) {
        guard let sugar = Int(sugarCountLabel.text!) else {
            return
        }
        sugarCountLabel.text = "\(sugar - 1)"
    }
    
    @IBAction func increaseSugarLevel(_ sender: Any) {
        guard let sugar = Int(sugarCountLabel.text!) else {
            return
        }
        sugarCountLabel.text = "\(sugar + 1)"
    }
    
    @IBAction func decreaseBMI(_ sender: Any) {
        guard let bmi = Int(bmiCountLabel.text!) else {
            return
        }
        bmiCountLabel.text = "\(bmi - 1)"
    }
    
    @IBAction func increaseBMI(_ sender: Any) {
        guard let bmi = Int(bmiCountLabel.text!) else {
            return
        }
        bmiCountLabel.text = "\(bmi + 1)"
    }
    
    @IBAction func onClickSubmitButton(_ sender: Any) {
        addProgress()
    }
    
    
    func addProgress() {
        if AppHelper.getInstance().isNetworkAvailable() {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            
            NotificationCenter.default.addObserver(self,selector: #selector(self.catchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.add_progress_service_notify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.weight:weightCountLabel.text!,AppConstants.ServiceKeys.sugar:sugarCountLabel.text!,AppConstants.ServiceKeys.BMI:bmiCountLabel.text!,AppConstants.ServiceKeys.date:formatter.string(from: Date())], url: AppConstants.Clients.add_progress_service_suffix, notificationKey: AppConstants.Clients.add_progress_service_notify, controllerName: self,requestType: "")
            AppHelper.getInstance().showProgressIndicator(view: self.view)
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func catchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                navigationController?.popViewController(animated: true)
                (data as! throwData).sendPopedData(data: "")
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: (serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String)!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}
