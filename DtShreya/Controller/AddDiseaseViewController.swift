//
//  AddDiseaseViewController.swift
//  Meditician
//
//  Created by kwikard on 04/05/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit

class AddDiseaseViewController: BaseViewController {
    
    var collectionCellData : [Diseases] = [Diseases]()
    
    @IBOutlet weak var cvFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var diseaseCV: UICollectionView!
    @IBOutlet weak var cvheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerView: CustomView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDisease()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
    }
    
    func resizeHeightOfCV() {
        cvheightConstraint.constant = CGFloat(collectionCellData.count%2 == 0 ? (collectionCellData.count/2)*40 : ((collectionCellData.count + 1)/2)*40)
        if cvheightConstraint.constant > self.view.frame.height - (103 + UIApplication.shared.statusBarFrame.height) {
            cvheightConstraint.constant = self.view.frame.height - (103 + UIApplication.shared.statusBarFrame.height)
        }
    }
    
    //MARK: Webservice Methods
    func getDisease() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.getProfileCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_disease_service_notify),object: nil)
            Webservice.getInstance().hitService(parameter: nil, url: AppConstants.Clients.get_disease_service_suffix, notificationKey: AppConstants.Clients.get_disease_service_notify, controllerName: self, requestType: "get")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func getProfileCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                let resultDict: [String] = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as! [String])
                for item in resultDict {
                    let model : Diseases = Diseases()
                    model.diseaseName = item
                    collectionCellData.append(model)
                }
                if (data as! [String:Any])["singleValue"] == nil {
                for item in (data as! [String:Any])["value"] as! [String] {
                    for disease in collectionCellData.enumerated() {
                        if disease.element.diseaseName == item  {
                            disease.element.isSelected = true
                            break
                        }
                        if disease.offset == collectionCellData.count - 1 {
                            let model : Diseases = Diseases()
                            model.diseaseName = item
                            model.isSelected = true
                            collectionCellData.append(model)
                        }
                    }
                }
                }
                diseaseCV.dataSource = self
                diseaseCV.delegate = self
                resizeHeightOfCV()
                diseaseCV.reloadData()
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "\(message!)", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    //MARK: IBAction
    @IBAction func onClickBackButton(_ sender: Any) {
        var diseases : [String] = [String]()
        for disease in collectionCellData {
            if disease.isSelected {
                diseases.append(disease.diseaseName)
            }
        }
        ((data as! [String:Any])["controller"] as! throwData).sendPopedData(data: diseases)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickAddDisease(_ sender: Any) {
        AppHelper.getInstance().alertWithTextField(title: "Add disease name", placeholder: "Disease name..", text: "", controllerName: self, leftButtonTite: "Done", isNumericTextField: false)
    }
    
}

extension AddDiseaseViewController : AlertTextDelegate {
    func dismissAlertView() {
        
    }
    
    func getInputText(text: String) {
        if text.count != 0 {
            let model : Diseases = Diseases()
            model.diseaseName = text
            model.isSelected = true
            if let _ = (data as! [String:Any])["singleValue"] {
                for disease in collectionCellData {
                    disease.isSelected = false
                }
            }
            collectionCellData.append(model)
            diseaseCV.reloadData()
        }
    }
}

extension AddDiseaseViewController : UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionCellData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let capsuleView : CustomView = cell.viewWithTag(1) as! CustomView
        let name : UILabel = cell.viewWithTag(2) as! UILabel
        name.text = collectionCellData[indexPath.row].diseaseName
        if collectionCellData[indexPath.item].isSelected {
            capsuleView.backgroundColor = UIColor(red: 77/255.0, green: 197/255.0, blue: 89/255.0, alpha: 1.0)
            name.textColor = UIColor.white
        }
        else {
            capsuleView.backgroundColor = UIColor.white
            name.textColor = UIColor(red: 77/255.0, green: 197/255.0, blue: 89/255.0, alpha: 1.0)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var calculatedSize : CGSize =   (collectionCellData[indexPath.item].diseaseName).size(withAttributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14.0)])
        calculatedSize.width += 25
        calculatedSize.height = 40
        return calculatedSize
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let _ = (data as! [String:Any])["singleValue"] {
            for disease in collectionCellData {
                disease.isSelected = false
            }
            collectionCellData[indexPath.row].isSelected = true
            collectionView.reloadData()
            return
        }
        if collectionCellData[indexPath.row].isSelected {
            collectionCellData[indexPath.row].isSelected = false
        }
        else {
            collectionCellData[indexPath.row].isSelected = true
        }
        collectionView.reloadData()
    }
}
