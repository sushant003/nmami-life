//
//  DietRecallViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 02/10/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class DietRecallViewController: BaseViewController {

    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var tableView: UITableView!
    
    var timeArray = ["06:00 AM","09:00 AM","12:00 PM","02:00 PM","04:00 PM","08:00 PM","10:00 PM"]
    var mealArray = [0:"",1:"",2:"",3:"",4:"",5:"",6:""]
    var datePickerController : DatePickerViewController! = nil
    var selectedIndex : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePickerController = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as? DatePickerViewController
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
    }
    
    //MARK: IBAction
    @IBAction func onClickBackButton(_ sender: Any) {
        (data as! throwData).sendPopedData(data: "0")
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickCheckBox(_ sender: UIButton) {
        let indexPath = tableView.indexPath(for: sender.superview?.superview?.superview?.superview?.superview as! DietRecallTableCell)
        if (tableView.cellForRow(at: indexPath!) as! DietRecallTableCell).checkButton.currentImage == UIImage(named: "diet_recall_checkBox_active") {
            (tableView.cellForRow(at: indexPath!) as! DietRecallTableCell).checkButton.setImage(UIImage(named: "diet_recall_checkBox_inactive"), for: .normal)
        }
        else {
            (tableView.cellForRow(at: indexPath!) as! DietRecallTableCell).checkButton.setImage(UIImage(named: "diet_recall_checkBox_active"), for: .normal)
        }
        tableView.reloadData()
    }
    
    @IBAction func onClickSelectTime(_ sender: UIButton) {
        let indexPath = tableView.indexPath(for: sender.superview?.superview?.superview?.superview?.superview as! DietRecallTableCell)
        self.view.endEditing(true)
        selectedIndex = (indexPath?.row)!
        self.datePickerController.data = ["pickerType":"time"]
        self.datePickerController.datePickerDelegate = self
        self.addChildViewController(self.datePickerController)
        self.view.addSubview(self.datePickerController.view)
        self.datePickerController.didMove(toParentViewController: self)
    }
    
    
    @IBAction func onClickSaveButton(_ sender: Any) {
        saveDietRecall()
    }
    
    //MARK: Webservice methods
    
    func saveDietRecall() {
        self.view.endEditing(true)
        if AppHelper.getInstance().isNetworkAvailable() {
            for meal in mealArray {
                if meal.value.count != 0 {
                    break
                }
                if meal.key == 6 {
                    AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please provide atleast one meal.", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                    return
                }
            }
            
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.saveDietRecallNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.save_diet_recall_service_notify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),
                                                            AppConstants.ServiceKeys.earlyMorningTime:timeArray[0],
                                                            AppConstants.ServiceKeys.earlyMorningMeal:mealArray[0]!,
                                                            AppConstants.ServiceKeys.breakfastTime:timeArray[1],
                                                            AppConstants.ServiceKeys.breakfastMeal:mealArray[1]!,
                                                            AppConstants.ServiceKeys.midMorningTime:timeArray[2],
                                                            AppConstants.ServiceKeys.midMorningMeal:mealArray[2]!,
                                                            AppConstants.ServiceKeys.lunchTime:timeArray[3],
                                                            AppConstants.ServiceKeys.lunchMeal:mealArray[3]!,
                                                            AppConstants.ServiceKeys.eveningTime:timeArray[4],
                                                            AppConstants.ServiceKeys.eveningMeal:mealArray[4]!,
                                                            AppConstants.ServiceKeys.dinnerTime:timeArray[5],
                                                            AppConstants.ServiceKeys.dinnerMeal:mealArray[5]!,
                                                            AppConstants.ServiceKeys.postDinnerTime:timeArray[6],
                                                            AppConstants.ServiceKeys.postDinnerMeal:mealArray[6]!], url: AppConstants.Clients.save_diet_recall_service_suffix, notificationKey: AppConstants.Clients.save_diet_recall_service_notify, controllerName: self, requestType: "")
            
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func saveDietRecallNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                navigationController?.popViewController(animated: true)
                (data as! throwData).sendPopedData(data: "")
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: (serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String)!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}

extension DietRecallViewController : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : DietRecallTableCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DietRecallTableCell
        switch indexPath.row {
        case 0:
            cell.dietTypeLabel.text = AppConstants.ServiceKeys.firstMealName
            cell.timeButton.setTitle(timeArray[0], for: .normal)
        case 1:
            cell.dietTypeLabel.text = AppConstants.ServiceKeys.secondMealName
            cell.timeButton.setTitle(timeArray[1], for: .normal)
        case 2:
            cell.dietTypeLabel.text = AppConstants.ServiceKeys.thirdMealName
            cell.timeButton.setTitle(timeArray[2], for: .normal)
        case 3:
            cell.dietTypeLabel.text = AppConstants.ServiceKeys.fourthMealName
            cell.timeButton.setTitle(timeArray[3], for: .normal)
        case 4:
            cell.dietTypeLabel.text = AppConstants.ServiceKeys.fifthMealName
            cell.timeButton.setTitle(timeArray[4], for: .normal)
        case 5:
            cell.dietTypeLabel.text = AppConstants.ServiceKeys.seventhMealName
            cell.timeButton.setTitle(timeArray[5], for: .normal)
        case 6:
            cell.dietTypeLabel.text = AppConstants.ServiceKeys.eightMealName
            cell.timeButton.setTitle(timeArray[6], for: .normal)
        default:
            break
        }
        if cell.checkButton.currentImage == UIImage(named: "diet_recall_checkBox_inactive") {
            cell.dietContainerView.isHidden = true
        }
        else {
            cell.dietContainerView.isHidden = false
        }
        return cell
    }
}

extension DietRecallViewController : DatePickerListener {
    func setDateFromPicker(date: Any) {
        datePickerController.view.removeFromSuperview()
        timeArray[selectedIndex] = date as! String
        tableView.reloadRows(at: [IndexPath(row: selectedIndex, section: 0)], with: .none)
    }
    
    func dismissDatePicker() {
        datePickerController.view.removeFromSuperview()
    }
}

extension DietRecallViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let indexPath = tableView.indexPath(for: textField.superview?.superview?.superview?.superview?.superview as! UITableViewCell)!
        mealArray[indexPath.row] = textField.text
    }
}
