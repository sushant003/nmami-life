//
//  GetReferralViewController.swift
//  Nmami Life
//
//  Created by Sushant Jugran on 11/10/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class GetReferralViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var referCodeButton: CustomButton!
    @IBOutlet weak var referCountLabel: UILabel!
    @IBOutlet weak var serverStatusView: UIView!
    @IBOutlet weak var serverStatusImage: UIImageView!
    @IBOutlet weak var serverStatusLabel: UILabel!
    var tableData = [ReferListModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        serverStatusImage.isHidden = true
        getReferCode()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
    }
    
    func setServerStatus(text : String) {
        serverStatusView.isHidden = false
        serverStatusImage.isHidden = false
        serverStatusLabel.text = text
    }
    
    //MARK: IBAction
    
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickShare(_ sender: Any) {
        let vc = UIActivityViewController(activityItems: ["Hey, Let's get fit together. Join Nmami Life and use my referral code : \(referCodeButton.titleLabel!.text!)"], applicationActivities: [])
        present(vc, animated: true)
    }
    
    //MARK: Webservice methods
    
    func getReferCode() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.serverStatusView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.referCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_refer_code_service_notify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: AppConstants.Clients.get_refer_code_service_suffix, notificationKey: AppConstants.Clients.get_refer_code_service_notify, controllerName: self, requestType: "")
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Net_Error)
        }
    }
    
    @objc func referCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.serverStatusView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                referCodeButton.setTitle((serviceData.userInfo?[AppConstants.ServiceKeys.result] as? String)!, for: .normal)
                getReferralList()
            }
            else {
                setServerStatus(text: message!)
            }
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Server_Error)
        }
    }
    
    func getReferralList() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.serverStatusView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.listCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_refer_list_service_notify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: AppConstants.Clients.get_refer_list_service_suffix, notificationKey: AppConstants.Clients.get_refer_list_service_notify, controllerName: self, requestType: "")
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Net_Error)
        }
    }
    
    @objc func listCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.serverStatusView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                serverStatusView.isHidden = true
                let resultDict: [String:Any] = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as? [String:Any])!
                prepareReferView(serviceData: resultDict)
                tableView.dataSource = self
                tableView.delegate = self
                tableView.reloadData()
            }
            else {
                setServerStatus(text: message!)
            }
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Server_Error)
        }
    }
    
    func prepareReferView(serviceData: [String:Any]) {
        for item in serviceData[AppConstants.ServiceKeys.list] as! [[String:Any]]{
            let referItem = ReferListModel(name: item[AppConstants.ServiceKeys.name] as! String, joined: item[AppConstants.ServiceKeys.joined] as! String, basicInfo: item[AppConstants.ServiceKeys.basicinfo] as! Bool, payment: item[AppConstants.ServiceKeys.payment] as! Bool, trial: item[AppConstants.ServiceKeys.trial] as! Bool)
            tableData.append(referItem)
        }
        referCountLabel.text = "\((serviceData[AppConstants.ServiceKeys.reffer] as! [String:Any])[AppConstants.ServiceKeys.refferachived] as! Int) referrals achieved"
    }
}

extension GetReferralViewController : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        (cell.viewWithTag(2) as! UILabel).text = tableData[indexPath.row].name
        (cell.viewWithTag(4) as! UILabel).text = "Joined On: " + tableData[indexPath.row].joined
        if tableData[indexPath.row].basicInfo {
            (cell.viewWithTag(3) as! UILabel).text = "Status: Signup Complete"
            if tableData[indexPath.row].trial {
                (cell.viewWithTag(3) as! UILabel).text = "Status: Trial User"
                if tableData[indexPath.row].payment {
                    (cell.viewWithTag(3) as! UILabel).text = "Status: Payment complete"
                }
            }
        }
        return cell
    }
}
