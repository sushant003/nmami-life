//
//  CreateDiseaseDiaryViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 26/09/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class CreateDiseaseDiaryViewController: BaseViewController {
    
    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var diseaseNameTF: UITextField!
    @IBOutlet weak var diseaseNoteTV: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        diseaseNameTF.attributedPlaceholder = NSAttributedString(string: "Select",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        diseaseNoteTV.text = "Type Something.."
        diseaseNoteTV.textColor = UIColor.lightGray
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
    }
    
    //MARK: IBAction
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickAddDisease(_ sender: Any) {
        AppHelper.getInstance().pushControllerWithData(id: "AddDiseaseViewController", controllerName: self, data: ["singleValue":true,"controller":self])
    }
    
    @IBAction func onClickSaveButton(_ sender: Any) {
        if diseaseNameTF.text?.count == 0 {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please select disease name", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
        else if diseaseNoteTV.text == "Type Something.." {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please enter note", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
        else {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            let params = [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),
                          AppConstants.ServiceKeys.date:formatter.string(from: Date()),
                          AppConstants.ServiceKeys.note:diseaseNoteTV.text!,
                          AppConstants.ServiceKeys.diseaseName:diseaseNameTF.text!
            ]
            if AppHelper.getInstance().isNetworkAvailable() {
                AppHelper.getInstance().showProgressIndicator(view: self.view)
                NotificationCenter.default.addObserver(self,selector: #selector(self.catchNotification),name: NSNotification.Name(rawValue: AppConstants.Appointments.create_disease_diary_service_notify),object: nil)
                Webservice.getInstance().hitService(parameter: params as NSDictionary, url: AppConstants.Appointments.create_disease_diary_service_suffix, notificationKey: AppConstants.Appointments.create_disease_diary_service_notify, controllerName: self,requestType: "")
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
    }
    
    @objc func catchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                (data as! throwData).sendPopedData(data: "")
                navigationController?.popViewController(animated: true)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: message!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}

extension CreateDiseaseDiaryViewController : throwData {
    func sendPopedData(data: Any) {
        if (data as! [String]).count != 0 {
            diseaseNameTF.text = (data as! [String])[0]
        }        
    }
}

extension CreateDiseaseDiaryViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Type Something.." {
            textView.text = ""
        }
        diseaseNoteTV.textColor = UIColor.black
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            diseaseNoteTV.text = "Type Something.."
            diseaseNoteTV.textColor = UIColor.lightGray
        }
    }
}
