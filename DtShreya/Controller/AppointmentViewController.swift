//
//  AppointmentViewController.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 19/03/18.
//  Copyright © 2018 Medicians. All rights reserved.
//

import UIKit


class AppointmentViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var appointmentTableView: UITableView!
    @IBOutlet weak var serverStatusView: UIView!
    @IBOutlet weak var serverStatusLabel: UILabel!
    @IBOutlet weak var serverStatusImage: UIImageView!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    var rescheduledTime : String = ""
    var appointmentData: [Appointment] = []
    var testData: [Test] = []
    var datePickerController : DatePickerViewController! = nil
    var timePickerController : DatePickerViewController! = nil
    var picker : String = "1"
    var selectedAppointmentIndex : Int = 0
    var refreshControl: UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        serverStatusImage.image = UIImage(named: "")
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControlEvents.valueChanged)
        appointmentTableView.addSubview(refreshControl)
        // Do any additional setup after loading the view.
        datePickerController = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        timePickerController = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        if (data as! Bool){
            headerHeightConstraint.constant = 0
        }
        getAppointmentList()
    }
    
    @objc func refresh(sender:AnyObject) {        
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
        }
        self.getAppointmentList()
    }
    
    func setServerStatus(text : String) {
        serverStatusView.isHidden = false
        serverStatusImage.image = #imageLiteral(resourceName: "serverError")
        serverStatusLabel.text = text
    }
    
    
    @IBAction func onClickbackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: Webservice Methods
    func getAppointmentList() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.serverStatusView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.appointmentListCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Appointments.appointment_list_service_notify),object: nil)
            Webservice.getInstance().hitService(parameter: [((data as! Bool) ? AppConstants.ServiceKeys.userId : "clientId"):AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: (data as! Bool) ? AppConstants.Appointments.appointment_list_service_suffix : AppConstants.Appointments.test_list_service_suffix, notificationKey: AppConstants.Appointments.appointment_list_service_notify, controllerName: self, requestType: "")
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Net_Error)
        }
    }
    
    @objc func appointmentListCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.serverStatusView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                serverStatusView.isHidden = true
                appointmentData.removeAll()
                testData.removeAll()
                let resultDict: [[String:Any]] = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as? [[String:Any]])!
                for dict in resultDict {
                    if (data as! Bool) {
                        let appointment : Appointment = Appointment(data: dict)
                        appointmentData.append(appointment)
                    }
                    else {
                        let test : Test = Test(data: dict)
                        testData.append(test)
                    }
                }
                self.appointmentTableView.delegate = self
                self.appointmentTableView.dataSource = self
                appointmentTableView.reloadData()
            }
            else {
                setServerStatus(text: message!)
            }
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Server_Error)
        }
    }
    
    func rescheduleAppointment(appointmentId : Int) {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.serverStatusView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.rescheduleAppointmentCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Appointments.reschedule_appointment_service_notify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.appointmentId:appointmentId,AppConstants.ServiceKeys.date:rescheduledTime], url: AppConstants.Appointments.reschedule_appointment_suffix, notificationKey: AppConstants.Appointments.reschedule_appointment_service_notify, controllerName: self, requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func rescheduleAppointmentCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.serverStatusView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                appointmentData[selectedAppointmentIndex].changeStatus(status: "Reschedule requested")
                appointmentTableView.reloadRows(at: [IndexPath(row: selectedAppointmentIndex, section: 0)], with: .automatic)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "\(message!)", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    //MARK: Tableview delegate and datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !(data as! Bool) {
            return testData.count
        }
        return appointmentData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if !(data as! Bool) {
            let cell = appointmentTableView.dequeueReusableCell(withIdentifier: "testCell", for: indexPath) as! TestTableViewCell
            let test = testData[indexPath.row]
            cell.setData(test: test)
            return cell
        }
        
        let cell = appointmentTableView.dequeueReusableCell(withIdentifier: "appointmentCell", for: indexPath) as! AppointmentTableViewCell
        let appointment = appointmentData[indexPath.row]
        cell.setData(appointmentDate: appointment.date, appointmentType: appointment.type, dietitianName: appointment.dietitianName, appointmentStatus: appointment.status)
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if appointmentData[indexPath.row].status == "Confirmed" {
            selectedAppointmentIndex = indexPath.row
            var actionArray: [UITableViewRowAction] = [UITableViewRowAction]()
            let rescheduleAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Reschedule Appointment", handler:{action, indexpath in
                self.picker = "1"
                self.datePickerController.data = ["pickerType":"date"]
                self.datePickerController.datePickerDelegate = self
                self.addChildViewController(self.datePickerController)
                self.view.addSubview(self.datePickerController.view)
                self.datePickerController.didMove(toParentViewController: self)
            });
            actionArray.append(rescheduleAction)
            return actionArray
        }
        return []
    }
    
    private func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: IndexPath) -> Bool {
        if (data as! Bool) {
            if appointmentData[indexPath.row].status == "Confirmed" {
                return true
            }
            return false
        }
        return false
        // the cells you would like the actions to appear needs to be editable
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

//MARK: Datepicker Delegate

extension AppointmentViewController : DatePickerListener {
    func setDateFromPicker(date: Any) {
        timePickerController.view.removeFromSuperview()
        datePickerController.view.removeFromSuperview()
        
        if picker == "1" {
            selectEventTime(date: date as! Date)
        }
        else
        {
            rescheduledTime = date as! String
            rescheduleAppointment(appointmentId: self.appointmentData[selectedAppointmentIndex].appointmentId)
        }
    }
    
    func dismissDatePicker() {
        if picker == "1" {
            datePickerController.view.removeFromSuperview()
        }
        else {
            timePickerController.view.removeFromSuperview()
        }
    }
    
    func selectEventTime(date : Date) {
        self.view.endEditing(true)
        picker = "2"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        timePickerController.datePickerDelegate = self
        timePickerController.data = ["pickerType":"time","date":dateFormatter.string(from: date)]
        addChildViewController(timePickerController)
        view.addSubview(timePickerController.view)
        timePickerController.didMove(toParentViewController: self)
    }
}
