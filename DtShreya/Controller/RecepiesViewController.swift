//
//  RecepiesViewController.swift
//  Nmami Life
//
//  Created by Sushant Jugran on 13/10/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class RecepiesViewController: BaseViewController {

    @IBOutlet weak var headerView: CustomView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    var tableData  : [TrendingModel] = [TrendingModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        getBlogs()
        statusImage.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
        tableView.visibleCells.forEach { (cell) -> Void  in
            (cell.viewWithTag(1) as! CustomView).createShadow()
        }
    }
    
    //MARK: IBAction
    
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickReadMore(_ sender: UIButton) {
        let indexPath = tableView.indexPath(for: sender.superview?.superview?.superview as! UITableViewCell)!
         AppHelper.getInstance().pushControllerWithData(id: "NavigationWebViewController", controllerName: self, data: ["link":tableData[indexPath.row].link,"pageName":tableData[indexPath.row].title])
//        UIApplication.shared.open(URL(string : tableData[indexPath.row].link)!, options: [:], completionHandler: { (status) in
//        })
    }
    
    @IBAction func onClickShare(_ sender: UIButton) {
        let indexPath = tableView.indexPath(for: sender.superview?.superview?.superview as! UITableViewCell)!
        let shareItem = [tableData[indexPath.row].link]
        let avc = UIActivityViewController(activityItems: shareItem, applicationActivities: nil)
        self.present(avc, animated: true, completion: nil)
    }
    //MARK: Webservice Methods
    func getBlogs() {
        if AppHelper.getInstance().isNetworkAvailable() {
            NotificationCenter.default.addObserver(self,selector: #selector(self.catchNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.getBlogsServiceNotifier),object: nil)
            
            Webservice.getInstance().hitService(parameter: nil, url: AppConstants.Dashboard.getRecepiesServiceSuffix, notificationKey: AppConstants.Dashboard.getBlogsServiceNotifier, controllerName: self,requestType: "get")
            AppHelper.getInstance().showProgressIndicator(view: self.statusView)
        }
        else {
            statusImage.isHidden = false
            statusLabel.text = AppConstants.ErrorMessags.Net_Error
        }
    }
    
    
    @objc func catchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.statusView)
        if(serviceData.userInfo != nil) {
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                statusView.isHidden = true
                let result = serviceData.userInfo![AppConstants.ServiceKeys.result] as! [[String:String]]
                result.forEach { (trending) in
                    var trend = TrendingModel()
                    trend.title = trending[AppConstants.ServiceKeys.title]!
                    trend.image = trending[AppConstants.ServiceKeys.imageLink]!
                    trend.link = trending[AppConstants.ServiceKeys.link]!
                    trend.description = trending[AppConstants.ServiceKeys.description]!
                    trend.author = trending[AppConstants.ServiceKeys.author]!
                    tableData.append(trend)
                }
                tableView.dataSource = self
                tableView.delegate = self
                tableView.reloadData()
                self.viewDidLayoutSubviews()
            }
            else {
                statusImage.isHidden = false
                statusLabel.text = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            }
        }
        else {
            statusImage.isHidden = false
            statusLabel.text = AppConstants.ErrorMessags.Server_Error
        }
    }
}
//MARK: Tableview delegate and datasource
extension RecepiesViewController : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        (cell.viewWithTag(2) as! UILabel).text = tableData[indexPath.row].title
        //        (cell.viewWithTag(6) as! UILabel).text = tableData[indexPath.row].author
        (cell.viewWithTag(4) as! UILabel).text = tableData[indexPath.row].description
        AppHelper.getInstance().loadImageIntoImageView(imageUrl: tableData[indexPath.row].image, imageView: (cell.viewWithTag(3) as! UIImageView), placeHolder: "")
        //        (cell.viewWithTag(1) as! CustomView).createShadow()
        return cell
    }
    
}
