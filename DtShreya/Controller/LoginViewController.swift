//
//  LoginViewController.swift
//  DtShreya
//  Created by Shlok Kapoor on 08/04/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit
import FirebaseAuth
import GoogleSignIn
import FirebaseAuthUI
import FirebaseFacebookAuthUI
import FirebaseGoogleAuthUI

class LoginViewController: ParentAuthUIViewController,socialLoginListener {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var headerView: CustomView!
    var selectedPhotoUrl = ""
    var isFBLogin = true
    override func viewDidLoad() {
        super.viewDidLoad()
        AppHelper.getInstance().setValueForUserDefault(value: "1", key: AppConstants.userDefaultConstants.isInstalledForFirstTime)
        usernameTextField.attributedPlaceholder = NSAttributedString(string: "Username/Mobile",
                                                               attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        self.authUI.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.setGradientBackground()
    }
    
    //MARK: IBAction
    @IBAction func signInButtonTapped(_ sender: Any) {
        self.view.endEditing(true)
        AppHelper.getInstance().removeUserDefaultForKey(key: AppConstants.ServiceKeys.image)
        let email = usernameTextField.text
        let password = passwordTextField.text
        if password == nil || email == nil {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please enter all the details", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            return
        }
        if !AppHelper.getInstance().isValidEmail(testStr: usernameTextField.text!) {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Email_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            return
        }
        else if (passwordTextField?.text?.count)! < 6 {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Password_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            return
        }
        if !AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            return
        }
        DispatchQueue.main.async {
            AppHelper.getInstance().showProgressIndicator(view: self.view)
        }
        self.loginToFirebase()
    }
    
    @IBAction func signUpButtonTapped(_ sender: Any) {
        AppHelper.getInstance().pushView(identifier: "SignUpViewController", controllerName: self)
    }
    @IBAction func forgotPasswordButtonTapped(_ sender: Any) {
        AppHelper.getInstance().alertWithTextField(title: "Forgot password", placeholder: "Add email id here", text: "", controllerName: self, leftButtonTite: "Send", isNumericTextField: false)
    }
    
    @IBAction func onClickFBLogin(_ sender: Any) {
        self.authUI.providers = [FUIFacebookAuth()]
        let provider = self.authUI.providers.first as! FUIFacebookAuth
        provider.signIn(withDefaultValue: "", presenting: self, completion: self.didSignIn)
        isFBLogin = true
        //        self.signIn(type: FUIFacebookAuth.self)
//        AppHelper.getInstance().faceBookLoginMethod(controller: self, listener: self)
    }
    
    @IBAction func onClickGoogleLogin(_ sender: Any) {
        self.authUI.providers = [FUIGoogleAuth()]
        let provider = self.authUI.providers.first as! FUIGoogleAuth
        provider.signIn(withDefaultValue: "", presenting: self, completion: self.didSignIn)
        isFBLogin = false
//        AppHelper.getInstance().googleLoginMethod(controller: self, listener: self)
//        GIDSignIn.sharedInstance().uiDelegate = self
//        GIDSignIn.sharedInstance().signIn()
    }
    
    //MARK: SocialLoginListener
    func loginSuccess(socialData: Any) {
        authoriseSocialSignIn(credential : socialData as! AuthCredential)
        
        AppHelper.getInstance().showProgressIndicator(view: self.view)
    }
    
    func loginCancelled() {
    }
    
    func loginToServer(params : [String:String],isLogin:Bool) {
        DataService.ds.postRequest(urlExt: isLogin ? "login/" : "signup/", params: params) { (data, error) in
            DispatchQueue.main.async {
                AppHelper.getInstance().hideProgressIndicator(view: self.view)
            }
            if let data = data {
                print(data)
                let message  = data[AppConstants.ServiceKeys.message] as? String
                if ((data[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                    DispatchQueue.main.async {
                        self.saveLoginData(dict: data)
                        AppHelper.getInstance().setValueForUserDefault(value: params[AppConstants.ServiceKeys.email]!, key: AppConstants.ServiceKeys.email)
                        
                        AppHelper.getInstance().pushView(identifier: "CustomMenuViewController", controllerName: self)
                        //Need to call userstatus api here tonavigate to different dashboard
                    }
                }
                else {
                    DispatchQueue.main.async {
                        AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "\(message!)", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                }
            }
        }
    }    
    func saveLoginData(dict : [String:Any]) {
        AppHelper.getInstance().setValueForUserDefault(value: "1", key: AppConstants.userDefaultConstants.isLoggedIn)
        AppHelper.getInstance().setValueForUserDefault(value: dict[AppConstants.ServiceKeys.username]!, key: AppConstants.userDefaultConstants.userName)
        AppHelper.getInstance().setValueForUserDefault(value: dict[AppConstants.ServiceKeys.userId]!, key:  AppConstants.ServiceKeys.userId)
        AppHelper.getInstance().setValueForUserDefault(value: dict[AppConstants.ServiceKeys.status]!, key: AppConstants.ServiceKeys.dieterStatus)
        
    }

    //MARK: Firebase methods
    
    func authoriseSocialSignIn(credential : AuthCredential) { 
        Auth.auth().currentUser!.link(with: credential) { (authResult, error) in
            if error != nil {
                DispatchQueue.main.async {
                    AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: error?.localizedDescription ?? "", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                    AppHelper.getInstance().hideProgressIndicator(view: self.view)
                }
                return
            }
            let params = [AppConstants.ServiceKeys.username:authResult?.displayName,AppConstants.ServiceKeys.email:authResult?.email,AppConstants.ServiceKeys.uid:authResult?.uid]
            if !self.isFBLogin {
                AppHelper.getInstance().setValueForUserDefault(value: authResult?.photoURL?.absoluteString ?? "", key: AppConstants.ServiceKeys.image)
            }
            self.loginToServer(params: params as! [String : String], isLogin: false)
        }/*
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if error != nil {
                DispatchQueue.main.async {
                    AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: error?.localizedDescription ?? "", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                    AppHelper.getInstance().hideProgressIndicator(view: self.view)
                }
                return
            }
            
            let params = [AppConstants.ServiceKeys.username:authResult?.user.displayName,AppConstants.ServiceKeys.email:authResult?.user.email,AppConstants.ServiceKeys.uid:authResult?.user.uid]
            if !self.isFBLogin {
                AppHelper.getInstance().setValueForUserDefault(value: authResult?.user.photoURL?.absoluteString ?? "", key: AppConstants.ServiceKeys.image)
            }
            self.loginToServer(params: params as! [String : String], isLogin: false)
        }*/
    }
    
    func loginToFirebase() {
        Auth.auth().signIn(withEmail: self.usernameTextField.text!, password: self.passwordTextField.text!) { (user, error) in
            if error == nil {
                let params = [AppConstants.ServiceKeys.email:self.usernameTextField.text!,AppConstants.ServiceKeys.uid:user?.uid]
                self.loginToServer(params: params as! [String : String], isLogin: true)
            }
            else {
                self.createFirebaseAccount()
                
            }
        }
    }
    
    func createFirebaseAccount() {
        Auth.auth().createUser(withEmail: usernameTextField.text!, password: passwordTextField.text!) { (user, error) in
            if error == nil {
                let params = [AppConstants.ServiceKeys.email:self.usernameTextField.text!,AppConstants.ServiceKeys.uid:user?.uid]
                self.loginToServer(params: params as! [String : String], isLogin: true)
            }
            else {
                
                AppHelper.getInstance().hideProgressIndicator(view: self.view)
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: (error?.localizedDescription)!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
    }
}

//MARK:AuthUI Delegate
extension LoginViewController: FUIAuthDelegate {
    
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        switch error {
        case .some(let error as NSError) where UInt(error.code) == FUIAuthErrorCode.userCancelledSignIn.rawValue:
            print("User cancelled sign-in")
        case .some(let error as NSError) where error.userInfo[NSUnderlyingErrorKey] != nil:
            print("Login error: \(error.userInfo[NSUnderlyingErrorKey]!)")
        case .some(let error):
            print("Login error: \(error.localizedDescription)")
        case .none:
            let params = [AppConstants.ServiceKeys.username:authDataResult?.additionalUserInfo?.profile!["name"] ?? "",AppConstants.ServiceKeys.email:authDataResult?.additionalUserInfo?.profile!["email"] ?? "",AppConstants.ServiceKeys.uid:authDataResult?.user.uid ?? ""] as [String : Any]
            if isFBLogin {
                AppHelper.getInstance().setValueForUserDefault(value: (authDataResult?.additionalUserInfo?.profile!["picture"] as! [String:Any])["url"] ?? "", key: AppConstants.ServiceKeys.image)
                self.loginToServer(params: params as! [String : String], isLogin: false)
            }
            else {
                AppHelper.getInstance().setValueForUserDefault(value: authDataResult?.additionalUserInfo?.profile!["picture"] ?? "", key: AppConstants.ServiceKeys.image)
                self.loginToServer(params: params as! [String : String], isLogin: false)
            }
            
        }
        
    }
}

//MARK:GoogleUI Delegate
extension LoginViewController : GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension LoginViewController : AlertTextDelegate {
    func dismissAlertView() {
        
    }
    
    func getInputText(text: String) {
        forgetPassword(email: text)
    }
    
    //MARK: Webservice methods
    func forgetPassword(email : String) {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.passwordCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.forgetPasswordNotify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.email:email], url: AppConstants.Dashboard.forgetPasswordSuffix, notificationKey: AppConstants.Dashboard.forgetPasswordNotify, controllerName: self,requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func passwordCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
//            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
//                
//            }
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: message!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}
