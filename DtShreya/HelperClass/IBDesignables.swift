//
//  IBDesignables.swift
//  E-Commerce
//
//  Created by Sushant Jugran on 15/03/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit

@IBDesignable class CustomView : UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    func createShadow() {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -0.2, height: 0.2)
        layer.shadowRadius = 3
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
    }
    
    func  setGradientBackground() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors =  [UIColor(red: 154/255.0, green: 277/255.0, blue: 137/255.0, alpha: 1.0),UIColor(red: 99/255.0, green: 215/255.0, blue: 177/255.0, alpha: 1.0),UIColor(red: 73/255.0, green: 214/255.0, blue: 214/255.0, alpha: 1.0)].map{$0.cgColor}
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func  setVerticalGradient() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors =  [UIColor(red: 154/255.0, green: 277/255.0, blue: 137/255.0, alpha: 1.0),UIColor(red: 99/255.0, green: 215/255.0, blue: 177/255.0, alpha: 1.0),UIColor(red: 73/255.0, green: 214/255.0, blue: 214/255.0, alpha: 1.0),UIColor(red: 73/255.0, green: 214/255.0, blue: 214/255.0, alpha: 1.0)].map{$0.cgColor}
//        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
//        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func removeShadow() {
        layer.shadowOpacity = 0.0
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
}

@IBDesignable class CustomButton : UIButton {
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
}

@IBDesignable class CustomLabel : UILabel {
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }    
}

@IBDesignable class CustomImage : UIImageView {
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
}

