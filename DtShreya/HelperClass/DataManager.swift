//
//  DataManager.swift
//  Nmami Life
//
//  Created by Sushant Jugran on 04/11/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import Foundation
import FMDB

class DatabaseManager {
    
    static let dbFileName = "countries.db"
    static var database:FMDatabase!
   
    
    func openDatabase() {
        let libraryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last
        let targetPath = "\(libraryPath!)/\(DatabaseManager.dbFileName)"
        
        if !FileManager.default.fileExists(atPath: targetPath) {
            let sourcePath = Bundle.main.path(forResource: "countries", ofType: "db")
            do {
                try FileManager.default.copyItem(atPath: sourcePath!, toPath: targetPath)
            }
            catch {
                print(error.localizedDescription)
            }
        }
       
        let database = FMDatabase(path: targetPath)
        database.open()
        if database.isOpen {
            DatabaseManager.database = database
            
        }
        
    }
    
    func closeDatabase() {
        if (DatabaseManager.database != nil) {
            DatabaseManager.database.close()
        }
    }
    
    func query(queryString:String) {
        if let db = DatabaseManager.database {
            do {
                let q = try db.executeQuery(queryString, values: nil)
                while q.next() {
                    let data = q.string(forColumn: "columnName")
                    // Do whatever here with fetched data, usually add it to an array and return that array
                }
            }
            catch {
                print(error.localizedDescription)
            }
        }
    }
}
