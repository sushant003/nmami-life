//
//  Model Class.swift
//  E-Commerce
//
//  Created by Sushant Jugran on 20/03/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import Foundation

struct NonActiveDashboardModel {
    var title : String = ""
    var imagelink : String = ""
    var bloglink : String = ""
    init(title:String,imagelink:String,bloglink : String) {
        self.title = title
        self.imagelink = imagelink
        self.bloglink = bloglink
    }
}

struct DashboardModel {
    var name : String = ""
    var phone : [String] = []
    var profileImage : String = ""
    var currentPlan : String = ""
    var target : String = ""
    var achive : String = ""
    var steps : String = ""
    var caloriesBurnt = ""
    var trending : [TrendingModel] = [TrendingModel]()
}

struct TrendingModel {
    var title = ""
    var image = ""
    var link = ""
    var description = ""
    var author = ""
}

struct DietitiansModel {
    var id : Int = 0
    var name : String = ""
    var url : String = ""
    init(name:String,url:String,id : Int) {
        self.name = name
        self.url = url
        self.id = id
    }
}

struct MessageModel {
    var time : String = ""
    var message : String = ""
    var attachedImage : String = ""
    var type : Int = 0 //1 : my text, 2 : other text, 3 : my image, 4 : other image
    init(message:String = "",time : String,image : String = "",type : Int) {
        self.message = message
        self.time = time
        self.attachedImage = image
        self.type = type
    }
}

struct MealModel {
    var mealTimeName : String
    var meal : [SubMealModel]
    var collapsed : Bool
    var index : Int
    var missed : Bool
    init(name:String,meal:[SubMealModel],collapsed:Bool = false,mealIndex : Int,missed : Bool) {
        mealTimeName = name
        self.meal = meal
        self.collapsed = collapsed
        index = mealIndex
        self.missed = missed
    }
}

struct SubMealModel {
    var firstFoodName : String
    var secondFoodName : String
    var firstFoodQuantity : String
    var secondFoodQuantity : String
    var option : Bool    
    var mealChanged : Bool = false
}

class Diseases : NSObject {
    var isSelected : Bool = false
    var diseaseName : String = ""
}


struct ProgressModel {
    var achievedWeight : Float!
    var achievedBMI : Float!
    var targetWeight : Float!
    var targetBMI : Float!
    var info : [ProgressInfo] = [ProgressInfo]()
}

struct ProgressInfo {
    var bmi : Float!
    var sugar : Float!
    var weight : Float!
    var date : String!
}

struct DiaryModel {
    var weight : String = ""
    var sleephours : String = ""
    var cheat : String = ""
    var steps : String = ""
    var caloriesBurnt : String = ""
    var date : String = ""
}

class Food : NSObject {
    var isSelected : Bool = false
    var id : Int!
    var isExempted : Bool = false
    var name : String = ""
}

struct ProfileModel {
    var name : String
    var gender : String
    var age : String    
}

struct DiseaseDiaryModel {
    var collapsed : Bool = false
    var diseaseName : String = ""
    var diseaseNotes : [String] = [String]()
}

struct DietStatsModel {
    var date :String
    var taken : Int
    var missed : Int
}

struct ReferListModel {
    var name : String
    var joined : String
    var basicInfo : Bool
    var payment : Bool
    var trial : Bool
}
