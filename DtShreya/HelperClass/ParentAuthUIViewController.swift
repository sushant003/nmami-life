//
//  ParentAuthUIViewController.swift
//  Nmami Life
//
//  Created by kwikard on 11/11/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit
import FirebaseAuthUI
import FirebaseAuth

class ParentAuthUIViewController: UIViewController {
    
    var authUI: FUIAuth! = FUIAuth.defaultAuthUI()
    var auth: Auth = Auth.auth()
    
    func didSignIn(auth: AuthCredential?, error: Error?, callBack: AuthResultCallback?) {
        let callBack: (AuthDataResult?, Error?) -> Void = { [unowned self] result, error in
            callBack?(result?.user, error)
            self.authUI.delegate?.authUI?(self.authUI, didSignInWith: result, error: error)
        }
        if let auth = auth {
            self.auth.signInAndRetrieveData(with: auth, completion: callBack)
        } else if let error = error {
            callBack(nil, error)
        }
    }
    
    func signIn<T: FUIAuthProvider>(type: T.Type, defaultValue: String? = nil) {
        try? self.authUI.signOut() // logout from google etc..
        self.authUI.providers.first(where: { $0 is T })?.signIn(withDefaultValue: defaultValue, presenting: self, completion: self.didSignIn)
    }
}
