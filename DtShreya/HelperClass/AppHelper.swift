//
//  Apphelper.swift
//  E-Commerce
//
//  Created by Sushant Jugran on 15/03/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import GoogleSignIn
import Firebase
//import FacebookCore
//import FacebookLogin
//import FBSDKCoreKit
import DropDown

class AppHelper: NSObject,GIDSignInDelegate {
    static var sharedInstance: AppHelper! = nil
    var userDefaults: UserDefaults! = nil
    var imageCacher: NSCache<NSString,UIImage> = NSCache<NSString,UIImage>()
    var delegate : socialLoginListener!
    var diseaseArray : [String] = [String]()
    
    static func getInstance() -> AppHelper {
        if(sharedInstance == nil) {
            sharedInstance = AppHelper()
        }
        return sharedInstance
    }
    
    func setValueForUserDefault(value: Any, key:  String) {
        if(userDefaults == nil) {
            userDefaults = UserDefaults.init(suiteName: "E-commerce")
            
        }
        userDefaults.set(value, forKey: key)
    }
    
    func getUserDefaultForKey(key: String) -> Any
    {
        if(userDefaults == nil) {
            userDefaults = UserDefaults.init(suiteName: "E-commerce")
        }
        guard  let value = userDefaults.value(forKey: key) else {
            return ""
        }
        if value is String {
            return value as! String
        }
        else if value is Int {
            return (value as! Int).description
        }
        else {
            return value
        }
    }
    
    func removeUserDefaultForKey(key:String)  {
        userDefaults.removeObject(forKey: key)
    }
    
    func getTimeFromEpoch(epochTime : Int64) -> String {
     
        let date = Date(timeIntervalSince1970: TimeInterval(epochTime / 1000))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm aa"
        return dateFormatter.string(from: date as Date)
    }
    
    func getTimeFromDate(date : String) -> String {
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let customDate = formatter1.date(from: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm aa"
        return dateFormatter.string(from: customDate!)
    }
    
    func getEpochFromDate(date : Date) -> Int64
    {
        let epochTime = Int64((date.timeIntervalSince1970 * 1000.0).rounded())
        return epochTime
    }
    
    func pushView(identifier: String,controllerName:UIViewController) {
        let nextViewController: UIViewController = GetViewControllerIntance(id: identifier, controllerName: controllerName)
        (AppDelegate.getDelegate().window?.rootViewController as! UINavigationController).pushViewController(nextViewController, animated: true)
    }
    
    func pushControllerWithData(id: String,controllerName: UIViewController, data:Any) {
        let nextViewController: BaseViewController = GetViewControllerIntance(id: id, controllerName: controllerName) as! BaseViewController
        nextViewController.data = data
        (AppDelegate.getDelegate().window?.rootViewController as! UINavigationController).pushViewController(nextViewController, animated: true)
    }
    
    func GetViewControllerIntance(id: String ,controllerName : UIViewController) -> UIViewController {
        
        var controllersArray: [UIViewController] = (AppDelegate.getDelegate().window?.rootViewController as! UINavigationController).viewControllers
        var targetController: UIViewController!
        for controller in controllersArray {
            if(controller.restorationIdentifier! == id) {
                controllersArray.remove(at: controllersArray.index(of: controller)!)
                break;
            }
        }
        targetController = controllerName.storyboard?.instantiateViewController(withIdentifier: id)
        let modalStyle: UIModalTransitionStyle = UIModalTransitionStyle.coverVertical
        targetController.modalTransitionStyle = modalStyle
        return targetController!
    }
    
    func prepareDropdown(anchor : UIView,dropdownList : [String],controller : CustomDropDownDelegate) {
        let dropDown = DropDown()
        dropDown.dismissMode = .automatic
        dropDown.anchorView = anchor
        dropDown.bottomOffset = CGPoint(x: 0, y:((dropDown.anchorView?.plainView.bounds.height)! + 2))
        dropDown.dataSource = dropdownList
        dropDown.show()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            controller.getSelectedItem(item: item)            
        }
    }
    
    func isNetworkAvailable() -> Bool
    {
        return (NetworkReachabilityManager.init()?.isReachable)!
    }
    
    func isValidEmail(testStr:String) -> Bool {
        
        //      print("validate emilId: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    func googleLoginMethod(controller: UIViewController, listener : socialLoginListener) {
        GIDSignIn.sharedInstance().delegate = self
        delegate = listener
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            if (user) != nil {
                guard let authentication = user.authentication else { return }
                let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                               accessToken: authentication.accessToken)
                delegate.loginSuccess(socialData: credential)
            }
            
            if (GIDSignIn.sharedInstance().currentUser.authentication.idToken != nil) {
                GIDSignIn.sharedInstance().signOut()
            }
        } else {
            print("\(error.localizedDescription)")
        }
    }
    /*
    func faceBookLoginMethod(controller: UIViewController, listener : socialLoginListener) {
        let loginManager = LoginManager()
//        loginManager.logOut()
        
        loginManager.logIn(readPermissions: [.email,.publicProfile], viewController: controller) { loginResult in
            print(loginResult)
            switch loginResult
            {
            case .failed(let error):
                self.showAlert(alertTitle: AppConstants.AppName, message: "Unable to access your profile.", cancelButtonName: "OK", otherButtonName: "", controllerName: listener as! UIViewController, isAlertOnly: true)
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success( _, _, _):
                let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                listener.loginSuccess(socialData: credential)
                
                
                
                let params = ["fields" : "email, name, id,picture.height(300).width(300),gender"]
                let graphRequest = GraphRequest(graphPath: "me", parameters: params)
                graphRequest.start {
                    (urlResponse, requestResult) in
                    switch requestResult {
                    case .failed(let error):
                        print("error in graph request:", error)
                        
                    case .success(let graphResponse):
                        if let responseDictionary = graphResponse.dictionaryValue {
                            if let imgUrl = ((responseDictionary["picture"] as! [String:Any])["data"] as! [String:Any])["url"] {
                                AppHelper.getInstance().setValueForUserDefault(value: imgUrl, key: AppConstants.ServiceKeys.image)
                            }
                        }
                    }
                }
                loginManager.logOut()
                FBSDKAccessToken.setCurrent(nil)
            }
        }
    }
    
    */
    func getMealReminder() {
        if AppHelper.getInstance().isNetworkAvailable() {
            
            Alamofire.request(AppConstants.mainUrl.appending("mealcheck/"), method: HTTPMethod.post, parameters: [AppConstants.ServiceKeys.meal:AppHelper.getInstance().getFoodTime(),AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], encoding: JSONEncoding.default,headers: nil).validate(contentType: ["text/html"]).responseJSON {
                (response) in
                if(response.result.value != nil && Alamofire.JSONSerialization.isValidJSONObject(response.result.value!)) {
                }
                else {
                    print(response.error?.localizedDescription ?? "")
                }
            }
        }
    }
    
    func saveDeviceToken(controller : BaseViewController?) {
        if AppHelper.getInstance().isNetworkAvailable() {            
            Alamofire.request(AppConstants.mainUrl.appending("savetokenios/"), method: HTTPMethod.post, parameters: ["token":AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.userDefaultConstants.user_device_token),AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),"dietitian":"0"], encoding: JSONEncoding.default,headers: nil).validate(contentType: ["text/html"]).responseJSON {
                (response) in
                if(response.result.value != nil && Alamofire.JSONSerialization.isValidJSONObject(response.result.value!)) {
                    if controller != nil {
                    AppHelper.getInstance().logOut()
                        AppHelper.getInstance().pushView(identifier: "LoginViewController", controllerName: controller!)
                    }
                }
                else {
                    print(response.error?.localizedDescription ?? "")
                }
            }
        }
    }
    
    //load image into imageview with desired placeholder
    func loadImageIntoImageView(imageUrl: String,imageView: UIImageView,placeHolder: String)
    {
        if let image = imageCacher.object(forKey: imageUrl as NSString)
        {
            DispatchQueue.main.async(execute: {
                imageView.image = image
            })
        }else if imageUrl.isEmpty
        {
            DispatchQueue.main.async(execute: {
                imageView.image = UIImage(named:placeHolder)
            })
        }
        else {
            Alamofire.request(imageUrl).responseImage { response in
                if let image = response.result.value {
                    self.imageCacher.setObject(image, forKey: imageUrl as NSString)
                    DispatchQueue.main.async(execute: {
                        imageView.image = image
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        imageView.image = UIImage(named:placeHolder)
                    })
                }
            }
        }
    }
    
    func showAlert(alertTitle:String, message:String, cancelButtonName:String, otherButtonName: String, controllerName:UIViewController,isAlertOnly: Bool) {
        _ = NSAttributedString(string: alertTitle, attributes: [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 18), //your font here
            NSAttributedStringKey.foregroundColor : UIColor.black
            ])
        
        _ = NSAttributedString(string: message, attributes: [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 14), //your font here
            NSAttributedStringKey.foregroundColor : UIColor.black
            ])
        
        let alertView = UIAlertController(title: alertTitle, message: message, preferredStyle: UIAlertControllerStyle.actionSheet)
        if(!isAlertOnly) {
            alertView.addAction(UIAlertAction(title: otherButtonName, style: UIAlertActionStyle.default, handler: { action in
                if(controllerName is alertPositiveButtonDelegate)
                {
                    (controllerName as! alertPositiveButtonDelegate).alertPositiveButtonPressed()
                }
            }))
            alertView.addAction(UIAlertAction(title: cancelButtonName, style: UIAlertActionStyle.default, handler: { action in
                if(controllerName is alertPositiveButtonDelegate) {
                    (controllerName as! alertPositiveButtonDelegate).alertNegativeButtonPressed()
                }
            }))
        }
        else {
            alertView.addAction(UIAlertAction(title: cancelButtonName, style: UIAlertActionStyle.default, handler:nil))
        }
        AppDelegate.getDelegate().window?.rootViewController?.present(alertView, animated: true, completion: nil)
    }
    
    func alertWithTextField(title:String,placeholder:String,text:String,controllerName:UIViewController,leftButtonTite : String,isNumericTextField : Bool) {
        let alertView = UIAlertController(title: title, message: "", preferredStyle: UIAlertControllerStyle.alert)
        alertView.addTextField { (textField) in
            textField.placeholder = placeholder
            textField.text = text
            textField.keyboardType = isNumericTextField ? .asciiCapableNumberPad : .asciiCapable
        }
        alertView.addAction(UIAlertAction(title: leftButtonTite, style: UIAlertActionStyle.default, handler: { action in
            if(controllerName is AlertTextDelegate)
            {
                (controllerName as! AlertTextDelegate).getInputText(text:((alertView.textFields?[0])?.text!)!)
            }
        }))
        alertView.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { action in
            if(controllerName is AlertTextDelegate)
            {
                (controllerName as! AlertTextDelegate).dismissAlertView()
            }
        }))
        controllerName.present(alertView, animated: true, completion: nil)
    }
    
    func showProgressIndicator(view:UIView) {
        DispatchQueue.main.async {
            let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
            let backView : UIView = UIView()
            backView.frame = view.bounds
            backView.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.25)
            backView.tag = 700
            actInd.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            actInd.center = backView.center
            actInd.hidesWhenStopped = true
            actInd.activityIndicatorViewStyle =
                UIActivityIndicatorViewStyle.whiteLarge
            backView.addSubview(actInd)
            view.addSubview(backView)
            actInd.startAnimating()
        }
    }
    
    func hideProgressIndicator(view:UIView) {
        DispatchQueue.main.async {
            for subviews in view.subviews {
                if subviews.tag == 700 {
                    if let indicatorView = subviews.subviews[0] as? UIActivityIndicatorView{
                        indicatorView.stopAnimating()
                    }
                    subviews.removeFromSuperview()
                }
            }            
        }
    }
    
    func getFoodTime(mealType: String) -> Int {
        switch mealType {
        case AppConstants.ServiceKeys.firstMeal:
            return 0
        case AppConstants.ServiceKeys.secondMeal:
            return 1
        case AppConstants.ServiceKeys.thirdMeal:
            return 2
        case AppConstants.ServiceKeys.fourthMeal:
            return 3
        case AppConstants.ServiceKeys.fifthMeal:
            return 4
        case AppConstants.ServiceKeys.sixthMeal:
            return 5
        case AppConstants.ServiceKeys.seventhMeal:
            return 6
        case AppConstants.ServiceKeys.supplements:
            return 7
        default:
            return -1
        }
    }
    
    
    func getFoodTime() -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone() as TimeZone?
        dateFormatter.dateFormat = "HH"
        if Int(dateFormatter.string(from: Date()))! < 9 {
            return 1
        }
        else if Int(dateFormatter.string(from: Date()))! < 10 {
            return 2
        }
        else if Int(dateFormatter.string(from: Date()))! < 12 {
            return 3
        }
        else if Int(dateFormatter.string(from: Date()))! < 14 {
            return 4
        }
        else if Int(dateFormatter.string(from: Date()))! < 16 {
            return 5
        }
        else if Int(dateFormatter.string(from: Date()))! < 18 {
            return 6
        }
        else if Int(dateFormatter.string(from: Date()))! < 20 {
            return 7
        }
        else if Int(dateFormatter.string(from: Date()))! < 22 {
            return 8
        }
        else {
            return 0
        }
    }
    
    func getFoodTimeFromNumber(mealNumber : Int) -> String {
        switch mealNumber {
        case 1:
            return "9 AM"
        case 2:
            return "10 AM"
        case 3:
            return "12 PM"
        case 4:
            return "2 PM"
        case 5:
            return "4 PM"
        case 6:
            return "6 PM"
        case 7:
            return "8 PM"
        case 8:
            return "10 PM"
        default:
            return ""
        }
    }
    
    
    func setStatusBar() {
        let statusView : UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusView.backgroundColor = UIColor(red: 56/255.0, green: 164/255.0, blue: 177/255.0, alpha: 1.0)
    }
    
    func logOut() {
        AppHelper.getInstance().setValueForUserDefault(value: "0" , key: AppConstants.userDefaultConstants.isLoggedIn)        
        AppHelper.getInstance().removeUserDefaultForKey(key: AppConstants.ServiceKeys.username)
        AppHelper.getInstance().removeUserDefaultForKey(key: AppConstants.ServiceKeys.userId)
        AppHelper.getInstance().setValueForUserDefault(value: false , key: AppConstants.ServiceKeys.dieterStatus)
        AppHelper.getInstance().removeUserDefaultForKey(key: AppConstants.userDefaultConstants.email)
        AppHelper.getInstance().removeUserDefaultForKey(key: AppConstants.ServiceKeys.image)
        
    }
}
