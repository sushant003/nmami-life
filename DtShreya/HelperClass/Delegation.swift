//
//  Delegation.swift
//  E-Commerce
//
//  Created by Sushant Jugran on 17/03/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit


protocol alertPositiveButtonDelegate {
    func alertPositiveButtonPressed()
    func alertNegativeButtonPressed()
}

protocol socialLoginListener {
    func loginSuccess(socialData : Any)
    func loginCancelled()
    
}

protocol throwData {
    func sendPopedData(data : Any)
}

protocol DatePickerListener {
    func setDateFromPicker(date : Any)
    func dismissDatePicker()
}

protocol AlertTextDelegate {
    func getInputText(text:String)
    func dismissAlertView()
}

protocol CustomDropDownDelegate {
    func getSelectedItem(item:String)
}

protocol HealthKitDelegate {
    func authorizationCompleted()
}
