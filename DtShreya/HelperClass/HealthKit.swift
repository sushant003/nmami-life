//
//  HealthKit.swift
//  Nmami Life
//
//  Created by Sushant Jugran on 13/10/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import Foundation
import Foundation
import HealthKit

class HealthKit
{
    let storage = HKHealthStore()
    var delegate : HealthKitDelegate!
    
    
    func checkAuthorization() -> Bool
    {
        // Default to assuming that we're authorized
        var isEnabled = true
        
        // Do we have access to HealthKit on this device?
        if HKHealthStore.isHealthDataAvailable()
        {
            // We have to request each data type explicitly
            var read = Set<HKObjectType>()
            read.insert(HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!)
            read.insert(HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!)
          
            // Now we can request authorization for step count data
            
            storage.requestAuthorization(toShare: nil, read: read as? Set<HKObjectType>) { (success, error) -> Void in
                isEnabled = success
                if isEnabled == true {
                    self.delegate.authorizationCompleted()
                }
            }
        }
        else
        {
            isEnabled = false
        }
        
        return isEnabled
    }
    
    func recentSteps(completion: @escaping (Double, NSError?) -> ()) {
        // The type of data we are requesting (this is redundant and could probably be an enumeration
        let type = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)
        
        // Our search predicate which will fetch data from now until a day ago
        // (Note, 1.day comes from an extension
        // You'll want to change that to your own NSDate
        
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        let predicate = HKQuery.predicateForSamples(withStart: startOfDay, end: now, options: .strictStartDate)
        
        // The actual HealthKit Query which will fetch all of the steps and sub them up for us.
        let query = HKSampleQuery(sampleType: type!, predicate: predicate, limit: 0, sortDescriptors: nil) { query, results, error in
            var steps: Double = 0
            if (results?.count)! > 0 {
                for result in results as! [HKQuantitySample] {
                    steps += result.quantity.doubleValue(for: HKUnit.count())
                }
            }
            completion(steps, error as NSError?)
        }
        
        storage.execute(query)
    }
    
    func energyBurnt(completion: @escaping (Double, NSError?) -> () )
    {
        // The type of data we are requesting (this is redundant and could probably be an enumeration
        let type = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)
        
        
        // Our search predicate which will fetch data from now until a day ago
        // (Note, 1.day comes from an extension
        // You'll want to change that to your own NSDate
        
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        let predicate = HKQuery.predicateForSamples(withStart: startOfDay, end: now, options: .strictStartDate)
        
        // The actual HealthKit Query which will fetch all of the steps and sub them up for us.
        let query = HKSampleQuery(sampleType: type!, predicate: predicate, limit: 0, sortDescriptors: nil) { query, results, error in
            var steps: Double = 0
            
            if (results?.count)! > 0
            {
                for result in results as! [HKQuantitySample]
                {
                    steps += result.quantity.doubleValue(for: HKUnit.calorie())
                }
            }
            
            completion(steps, error as NSError?)
        }
        
        storage.execute(query)
    }
}
