//
//  UIImageViewExtension.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 17/04/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}

extension UIColor {
    public convenience init?(hexString: String) {
        let r, g, b, a: CGFloat
        
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            let hexColor = String(hexString[start...])
            
            if hexColor.count == 6 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        return nil
    }
}

class halfConcentricCircle : UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.beginPath()
        context.move(to: CGPoint(x: 0, y: 0))
        context.addLine(to: CGPoint(x: 0, y: 100))
        context.addQuadCurve(to: CGPoint(x: self.frame.width, y: 100), control: CGPoint(x: self.frame.width/2, y: self.frame.height))
        context.addLine(to: CGPoint(x: self.frame.width, y: 0))
        context.addCurve(to: CGPoint(x: 0, y: 0), control1: CGPoint(x: self.frame.width - 100, y: self.frame.height - 100), control2: CGPoint(x: 100, y: self.frame.height - 100))
        context.addLine(to: CGPoint(x: 0, y: 0))
        context.closePath()
        context.setFillColor(UIColor(displayP3Red: 189/255.0, green: 234/255.0, blue: 240/255.0, alpha: 1.0).cgColor)
        context.fillPath()
    }
}

class TriangleView : CustomView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
   
    override func draw(_ rect: CGRect) {
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.beginPath()
        context.move(to: CGPoint(x: self.frame.width, y: 0))
        context.addLine(to: CGPoint(x: 0, y: self.frame.height))
        context.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height))
        context.closePath()
        context.setFillColor(UIColor(displayP3Red: 245/255.0, green: 248.0/255.0, blue: 247.0/255.0, alpha: 1.0).cgColor)
        context.fillPath()
    }
}

class ArcView: CustomView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.beginPath()
        context.move(to: CGPoint(x: 0, y: 0))
        context.addLine(to: CGPoint(x: 0, y: self.frame.height - 50))
        context.addQuadCurve(to: CGPoint(x: self.frame.width, y: self.frame.height - 50), control: CGPoint(x: self.frame.width/2, y: self.frame.height))
        context.addLine(to: CGPoint(x: self.frame.width, y: 0))
        context.closePath()
        context.setFillColor(UIColor(displayP3Red: 189/255.0, green: 234/255.0, blue: 240/255.0, alpha: 1.0).cgColor)
        context.fillPath()
    }
}
