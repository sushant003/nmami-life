//
//  AppDelegate.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 07/04/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit
import Firebase
import SideMenuController
import UserNotifications
import BRYXBanner
//import FacebookCore
import GoogleSignIn
import Cloudinary
import FirebaseAuthUI
import SQLite3

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate,UNUserNotificationCenterDelegate {
    var cloudinary : CLDCloudinary!
    var window: UIWindow?
    
    static func getDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        AppHelper.getInstance().setStatusBar()
        Messaging.messaging().delegate = self
//        createCopyOfDatabaseIfNeeded()
        let config = CLDConfiguration(cloudName: AppConstants.Cloudinary_Cloud_Name, apiKey: AppConstants.Cloudinary_Api_Key)
        cloudinary = CLDCloudinary(configuration: config)
        UIApplication.shared.setMinimumBackgroundFetchInterval(1*60*60)
        // Override point for customization after application launch.
//        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        FirebaseApp.configure()
        initMenuController()
        GIDSignIn.sharedInstance().clientID = AppConstants.GOOGLE_API_KEY
        Messaging.messaging().shouldEstablishDirectChannel = true
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        return true
    }
 
    func createCopyOfDatabaseIfNeeded() {
        var success : Bool
        let fileManager = FileManager.default
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentDirectory = paths[0]
        let appDBPath = URL(string: documentDirectory)?.appendingPathComponent("countries.db")
        success = fileManager.fileExists(atPath: "\(appDBPath!)")
        if success {
            return
        }
        let defaultDBPath = Bundle.main.resourceURL!.appendingPathComponent("countries.db")
        do {
            try fileManager.copyItem(at: defaultDBPath, to: appDBPath!)
        }
        catch {
            print(error.localizedDescription)
        }
        
    }
    
    
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        AppHelper.getInstance().getMealReminder()
        
    }
    
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any])
        -> Bool {
            
//
            let sourceApplication = options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String?
            if FUIAuth.defaultAuthUI()?.handleOpen(url, sourceApplication: sourceApplication) ?? false {
                return true
            }
            // other URL handling goes here.
            return false


//            if (SDKApplicationDelegate.shared.application(application, open: url, options: options)) {
//                let handled = SDKApplicationDelegate.shared.application(application, open: url, options: options )
//                return handled
//
//            }
//            if(GIDSignIn.sharedInstance().handle(url,
//                                                 sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
//                                                 annotation: options[UIApplicationOpenURLOptionsKey.annotation])) {
//                return true
//            }
//            return false
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        AppHelper.getInstance().setValueForUserDefault(value: fcmToken, key: AppConstants.userDefaultConstants.user_device_token)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        showNotification(userInfo: userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        showNotification(userInfo: userInfo)
    }
    
    func showNotification(userInfo : [AnyHashable : Any]) {
        if UIApplication.shared.applicationState == .active {
            let banner = Banner(title: ((userInfo["aps"] as! [String:Any])["alert"] as! [String:String])["title"], subtitle: ((userInfo["aps"] as! [String:Any])["alert"] as! [String:String])["body"], backgroundColor: UIColor.white, didTapBlock: nil)
            banner.dismissesOnTap = true
            banner.hasShadows = true
            banner.textColor = UIColor.black
            banner.show(duration: 4.0)
            banner.detailLabel.textColor = UIColor.darkGray
            banner.detailLabel.font = UIFont.systemFont(ofSize: 12.0)
            banner.titleLabel.font = UIFont.systemFont(ofSize: 16.0)
        }
//        else {
//            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5, repeats: false)
//            let center = UNUserNotificationCenter.current()
//            let identifier = "UYLLocalNotification"
//            let content = UNMutableNotificationContent()
//            content.title = userInfo["title"]! as! String
//            content.body = userInfo["data"]! as! String
//            content.sound = UNNotificationSound.default()
//            let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
//            center.add(request, withCompletionHandler: { (error) in
//                if let error = error {
//                    //Something went wrong
//                    print(error.localizedDescription)
//                }
//            })
//        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    }
    
    func initMenuController() {
        SideMenuController.preferences.drawing.sidePanelPosition = .underCenterPanelLeft
        SideMenuController.preferences.drawing.sidePanelWidth = (self.window?.frame.size.width)! * 0.70
        SideMenuController.preferences.drawing.centerPanelShadow = true
        SideMenuController.preferences.animating.statusBarBehaviour = .showUnderlay
    }
}

