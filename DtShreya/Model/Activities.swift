//
//  Activities.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 17/04/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import Foundation

class Activities {
    private var _data: [Activity] = []
    
    var data: [Activity] {
        get {
            return _data
        }
    }
    
    init () {
        _data = [
            Activity(description: "Description1", imageName: "1"),
            Activity(description: "Description2", imageName: "1"),
            Activity(description: "Description3", imageName: "1"),
            Activity(description: "Description4", imageName: "1")
        ]
    }
}
