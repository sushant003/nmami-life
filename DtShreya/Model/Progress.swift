//
//  Progress.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 17/04/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class Progress {
    private var _image: UIImage
    private var _description: String
    
    init(imageName: String, text: String) {
        _image = UIImage(named: imageName) ?? UIImage(named: "1")!
        _description = text
    }
    
    var image: UIImage {
        get {
            return _image
        }
    }
    var text: String {
        get {
            return _description
        }
    }
    
}
