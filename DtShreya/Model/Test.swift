//
//  Appointment.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 28/03/18.
//  Copyright © 2018 Medicians. All rights reserved.
//

import Foundation


class Test {
    private var _testId: Int
    private var _date: String
    private var _dietitianName: String
    private var _testName: String
    private var _testStatus: String
    private var _testNotes: String
    
    var date: String {
        get {
            return _date
        }
    }
    
    var dietitianName: String {
        get {
            return _dietitianName
        }
    }
    
    var testName: String {
        get {
            return _testName
        }
    }
    
    var testNotes: String {
        get {
            return _testNotes
        }
    }
    
    var status: String {
        get {
            return _testStatus
        }
        
    }
    var testId: Int {
        get {
            return _testId
        }
    }
    
    init(data: [String: Any]) {
        
        if let id = data["id"] {
            self._testId = Int(String(describing: id)) ?? 0
        }
        else {
            self._testId = 0
        }
        
        if let status = data["status"] {
            self._testStatus = String(describing: status)
        }
        else {
            self._testStatus = notAvailable
        }
        
        if let testName = data["testname"] {
            self._testName = String(describing: testName)
        }
        else {
            self._testName = notAvailable
        }
        
        if let dietitian = data["dietitian"] {
            self._dietitianName = String(describing: dietitian)
        }
        else {
            self._dietitianName = notAvailable
        }
        
        if let date = data["date"] {
            self._date = String(describing: date)
        }
        else {
            self._date = notAvailable
        }
        
        if let testNotes = data["testnotes"] {
            self._testNotes = String(describing: testNotes)
        }
        else {
            self._testNotes = notAvailable
        }
    }
}
