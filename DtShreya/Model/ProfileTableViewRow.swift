//
//  ProfileTableViewRow.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 28/03/18.
//  Copyright © 2018 Medicians. All rights reserved.
//

import Foundation

class ProfileTableViewRow {
    private var _description: String
    private var _segueIdentifier: String?
    
    var description: String {
        get {
            return _description
        }
    }
    
    var segueIdentifier: String? {
        get {
            return _segueIdentifier
        }
    }
    
    init(description: String, segueIdentifier: String) {
        _description = description
        _segueIdentifier = segueIdentifier
    }
    
    init(description: String) {
        _description = description
    }
    
}
