//
//  Appointment.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 28/03/18.
//  Copyright © 2018 Medicians. All rights reserved.
//

import Foundation


class Appointment {
    private var _appointmentId: Int
    private var _date: String
    private var _dietitianName: String
    private var _appointmentType: String
    private var _appointmentStatus: String
    
    
    var date: String {
        get {
            return _date
        }
    }
    
    var dietitianName: String {
        get {
            return _dietitianName
        }
    }
    
    var type: String {
        get {
            return _appointmentType
        }
    }
    var status: String {
        get {
            return _appointmentStatus
        }
    }
    var appointmentId: Int {
        get {
            return _appointmentId
        }
    }
    
//    init(date: Date, dietitianName: String, appointmentType: Bool, appointmentStatus: Bool) {
//        self._date = date
//        self._dietitianName = dietitianName
//        self._appointmentType = appointmentType
//        self._appointmentStatus = appointmentStatus
//    }
    
    func changeStatus(status : String) {
        _appointmentStatus = status
    }
    
    init(data: [String: Any]) {
        
        if let id = data["appointmentId"] {
            self._appointmentId = Int(String(describing: id)) ?? 0
        }
        else {
            self._appointmentId = 0
        }
        
        if let status = data["status"] {
            self._appointmentStatus = String(describing: status)
        }
        else {
            self._appointmentStatus = notAvailable
        }
        
        if let type = data["type"] {
            self._appointmentType = String(describing: type)
        }
        else {
            self._appointmentType = notAvailable
        }
        
        if let dietitian = data["dietitian"] {
            self._dietitianName = String(describing: dietitian)
        }
        else {
            self._dietitianName = notAvailable
        }
        
        if let date = data["date"] {
            self._date = String(describing: date)
        }
        else {
            self._date = notAvailable
        }
    }
}
