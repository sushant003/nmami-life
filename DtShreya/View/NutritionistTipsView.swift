//
//  NutritionistTipsView.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 19/02/18.
//  Copyright © 2018 Medicians. All rights reserved.
//

import UIKit

class NutritionistTipsView: UIView {
    
    override func awakeFromNib() {
        // corner radius
        self.layer.cornerRadius = 10
        
        // border
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        
        // shadow
//        self.layer.shadowColor = UIColor.black.cgColor
//        self.layer.shadowOffset = CGSize(width: 3, height: 3)
//        self.layer.shadowOpacity = 0.3
//        self.layer.shadowRadius = 5.0
    }
    
    func addLabels(heading: String!, description: String!) {
        let headingLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 20))
        headingLabel.center = CGPoint(x: self.frame.width/2, y: 20)
        headingLabel.textAlignment = .center
        headingLabel.text = heading
        headingLabel.textColor = UIColor.red
        headingLabel.font = UIFont(name: "Avenir Next Bold", size: 17.0)
        self.addSubview(headingLabel)
        
        
        let descriptionLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height - 20))
        descriptionLabel.center = CGPoint(x: self.frame.width/2, y: 50)
        descriptionLabel.text = description
        descriptionLabel.textAlignment = .center
        descriptionLabel.font = UIFont(name: "Avenir Next", size: 15.0)
        self.addSubview(descriptionLabel)
    }
}
