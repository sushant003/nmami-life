//
//  TestTableViewCell.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 20/05/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class TestTableViewCell: UITableViewCell {

    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var dietitianName: UILabel!
    @IBOutlet weak var testName: UILabel!
    @IBOutlet weak var status: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(test: Test) {
        self.date.text = test.date
        self.dietitianName.text = test.dietitianName
        self.testName.text = test.testName
        self.status.text = "Status " + test.status
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
