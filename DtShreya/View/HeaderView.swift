//
//  HeaderView.swift
//  GDI
//
//  Created by Sushant Jugran on 21/06/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit

protocol SeactionHeaderClicked {
    func reloadTable(section : Int,collapsed : Bool)
    func commitAction(section : Int)
}

class HeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var containerView: CustomView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var collapseButton: UIButton!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var dropDownButton: UIButton!
    
    var seactionIndex : Int!
    var isCollapsed : Bool!
    var delegate : SeactionHeaderClicked!

    //MARK: IBAction
    @IBAction func onClickCollapseSection(_ sender: UIButton) {
        delegate.reloadTable(section: seactionIndex, collapsed: isCollapsed ? false : true)
    }
    
    @IBAction func onClickCommitAction(_ sender: Any) {
        delegate.commitAction(section: seactionIndex)
    }
    
}
