//
//  SentTableViewCell.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 12/03/18.
//  Copyright © 2018 Medicians. All rights reserved.
//

import UIKit

class SentTableViewCell: UITableViewCell {

    @IBOutlet weak var messageLabel: MessageLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setText(message: String) {
        self.messageLabel.setupMessageLabel(message: message)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
