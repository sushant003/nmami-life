//
//  HeadingAndTextTableViewCell.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 02/04/18.
//  Copyright © 2018 Medicians. All rights reserved.
//

import UIKit

class HeadingAndTextTableViewCell: UITableViewCell {
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(heading: String, text: String) {
        self.headingLabel.text = heading
        self.label.text = text
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
