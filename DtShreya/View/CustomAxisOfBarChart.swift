//
//  CustomAxisOfBarChart.swift
//  GDI
//
//  Created by Sushant Jugran on 16/08/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit
import Foundation
import Charts

@objc(BarChartFormatter)
public class BarChartFormatter: NSObject, IAxisValueFormatter
{
    static var days: [String]! = [String]()
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return BarChartFormatter.days[Int(value)]
    }
    
    public func setXAxisArray(data : [String]) {
        BarChartFormatter.days = data
    }
}

public class YAxisValueFormatter : NSObject, IAxisValueFormatter {
    let numFormatter: NumberFormatter
    
    override init() {
        numFormatter = NumberFormatter()
        numFormatter.minimumFractionDigits = 1
        numFormatter.maximumFractionDigits = 1
        
        // if number is less than 1 add 0 before decimal
        numFormatter.minimumIntegerDigits = 1 // how many digits do want before decimal
        numFormatter.paddingPosition = .beforePrefix
        numFormatter.paddingCharacter = "0"
    }
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return numFormatter.string(from: NSNumber(floatLiteral: value))!
    }
}
