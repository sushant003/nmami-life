//
//  AppointmentTableViewCell.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 19/03/18.
//  Copyright © 2018 Medicians. All rights reserved.
//

import UIKit

class AppointmentTableViewCell: UITableViewCell {
    @IBOutlet weak var appointmentDateLabel: UILabel!
    @IBOutlet weak var appointmentType: UILabel!
    @IBOutlet weak var dietitianNameLabel: UILabel!    
    @IBOutlet weak var appointmentStatus: UILabel!
    @IBOutlet weak var callIcon: CustomImage!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        callIcon.image = callIcon.image?.withRenderingMode(.alwaysTemplate)
        callIcon.tintColor = UIColor.white
        // Initialization code
    }
    func setData(appointmentDate: String!, appointmentType: String!, dietitianName: String!, appointmentStatus: String!) {
        self.appointmentDateLabel.text = appointmentDate
        self.appointmentType.text = appointmentType
        self.dietitianNameLabel.text = dietitianName
        self.appointmentStatus.text = "Status " + appointmentStatus
//        if appointmentStatus != "Confirmed" {
//
//            rescheduleButton.isHidden = true
//        }
//        else {
//            rescheduleButton.isHidden = false
//            self.appointmentStatus.isHidden = true
//        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
