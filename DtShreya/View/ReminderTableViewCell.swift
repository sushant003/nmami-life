//
//  ReminderTableViewCell.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 28/03/18.
//  Copyright © 2018 Medicians. All rights reserved.
//

import UIKit

class ReminderTableViewCell: UITableViewCell {

    @IBOutlet weak var mealDescription: UILabel!
    @IBOutlet weak var reminderTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setCell(reminder: Reminder) {
        mealDescription.text = reminder.meal
        reminderTimeLabel.text = reminder.reminderTime
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
