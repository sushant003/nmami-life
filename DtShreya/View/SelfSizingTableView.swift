//
//  SelfSizingTableView.swift
//  DtShreya
//
//  Created by Sushant Jugran on 18/09/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class SelfSizingTableView: UITableView {

    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
        self.layoutIfNeeded()
    }
    
    override var intrinsicContentSize: CGSize {
        let height = contentSize.height
        return CGSize(width: contentSize.width, height: height)
    }
}
